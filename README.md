# Readme first

## Files excluded from git:

  keystore.properties
  
  admob.properties
  
  fabric.properties
  
  
###### keystore.properties should look like this:

    storePassword=yourStorePassword
    keyPassword=yourKeyPassword
    keyAlias=yourKeyAllias
    storeFile= full path to jks location

##### Running first time without git init

  please comment out these lines in app build.gradle as shown below
  
    ...
    //apply from: "$project.rootDir/tools/script-git-version.gradle"
    
    ...
    
    productFlavors {
    dev {
      signingConfig signingConfigs.debug
      //versionCode gitVersionCodeTime
      //versionName gitVersionName
      applicationId "eu.oncreate.stockshark.dev"
    }

    prod {
      signingConfig signingConfigs.release
      //versionCode gitVersionCode
      //versionName gitVersionName
      applicationId "eu.oncreate.stockshark"
    }
  }
  

