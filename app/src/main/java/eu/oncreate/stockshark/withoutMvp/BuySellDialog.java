package eu.oncreate.stockshark.withoutMvp;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import eu.oncreate.stockshark.R;

public class BuySellDialog extends DialogFragment {

  public static final String MAX = "max";
  public static final String SELL = "sell";
  @BindView(R.id.dialog_buyOrSellRootView)
  ConstraintLayout root;

  @BindView(R.id.appCompatEditText)
  EditText editTextQuantity;

  @BindView(R.id.textView)
  TextView textView;

  @BindView(R.id.buttonOk)
  Button buttonOk;

  @BindView(R.id.buttonCancel)
  Button buttonCancel;

  private boolean sell;

  private Unbinder unbinder;
  private int maxQuantity = 0;

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    Activity activity = (Activity) context;
    if (!(activity instanceof buyOrSellListener)) {
      throw new ClassCastException(activity.toString() + " must implement YesNoListener");
    }
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {

    View view = inflater.inflate(R.layout.dialog_custom_buy_sale, container);

    unbinder = ButterKnife.bind(this, view);

    if (sell) {
      textView.setText(R.string.enter_amount_sell);
    } else {
      textView.setText(R.string.enter_amount_buy);
    }

    this.setCancelable(true);
    buttonOk.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (verifyTransaction()) {
          ((buyOrSellListener) getActivity()).buyOrSell(
              Integer.parseInt(editTextQuantity.getText().toString()), sell);
        }
      }
    });
    buttonCancel.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        InputMethodManager imm =
            (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        ((buyOrSellListener) getActivity()).cancel();
      }
    });
    return view;
  }

  private boolean verifyTransaction() {
    boolean nameVerified = false;
    if ((!editTextQuantity.getText().toString().equals("")) && (editTextQuantity.getText()
        .toString()
        .length() > 0) && (Integer.parseInt(editTextQuantity.getText().toString()) > 0)) {
      if (sell) {
        if (Integer.parseInt(editTextQuantity.getText().toString()) <= maxQuantity) {
          nameVerified = true;
        } else {
          editTextQuantity.setError(getString(R.string.you_dont_have_enough_stocks));
        }
      } else {
        if (Integer.parseInt(editTextQuantity.getText().toString()) <= maxQuantity) {
          nameVerified = true;
        } else {
          editTextQuantity.setError(getString(R.string.you_dont_have_enough_money));
        }
      }
    } else {
      if (editTextQuantity.getText().toString().equals("")) {
        editTextQuantity.setError(getString(R.string.please_enter_amount));
      } else {
        if (Integer.parseInt(editTextQuantity.getText().toString()) <= 0) {
          editTextQuantity.setError(getString(R.string.please_enter_positive_amount));
        }
      }
    }
    return nameVerified;
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    maxQuantity = getArguments().getInt(MAX);
    sell = getArguments().getBoolean(SELL);
  }

  public interface buyOrSellListener {
    void buyOrSell(int quantity, boolean sell);

    void cancel();

    //void onNo();
  }
}
