package eu.oncreate.stockshark.withoutMvp;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import eu.oncreate.stockshark.R;

/**
 * Created by mantas on 9/29/17.
 */

public class RetireDialog extends DialogFragment {

  @BindView(R.id.dialog_retire)
  ConstraintLayout root;

  @BindView(R.id.btn_no)
  Button buttonNo;

  @BindView(R.id.btn_yes)
  Button buttonYes;

  private Unbinder unbinder;
  private RetireListener callback;

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    Activity activity = (Activity) context;
    try {
      callback = (RetireListener) activity;
    } catch (ClassCastException e) {
      throw new ClassCastException(activity.toString() + " must implement RetireListener");
    }
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {

    View view = inflater.inflate(R.layout.dialog_retire, container);

    unbinder = ButterKnife.bind(this, view);

    this.setCancelable(false);
    buttonYes.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        callback.onRetireClicked();
      }
    });

    buttonNo.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        callback.onCancelRetireClicked();
      }
    });
    return view;
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }

  @Override
  public void onDetach() {
    super.onDetach();
    callback = null;
  }

  public interface RetireListener {
    void onRetireClicked();

    void onCancelRetireClicked();
  }
}
