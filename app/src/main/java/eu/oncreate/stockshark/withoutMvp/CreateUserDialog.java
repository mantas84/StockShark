package eu.oncreate.stockshark.withoutMvp;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import eu.oncreate.stockshark.R;

public class CreateUserDialog extends DialogFragment {

  @BindView(R.id.dialog_create_user_root)
  ConstraintLayout root;

  @BindView(R.id.appCompatEditText)
  EditText editTextName;

  @BindView(R.id.buttonOk)
  Button buttonOk;

  private Unbinder unbinder;

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    Activity activity = (Activity) context;
    if (!(activity instanceof YesNoListener)) {
      throw new ClassCastException(activity.toString() + " must implement YesNoListener");
    }
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {

    View view = inflater.inflate(R.layout.dialog_create_user, container);

    unbinder = ButterKnife.bind(this, view);

    this.setCancelable(false);
    buttonOk.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (verifyName()) {
          ((YesNoListener) getActivity()).onStartNew(editTextName.getText().toString());
        }
      }
    });
    return view;
  }

  private boolean verifyName() {
    boolean nameVerified = false;
    if ((!editTextName.getText().toString().equals("")) && (editTextName.getText()
        .toString()
        .length() > 0)) {
      nameVerified = true;
    } else {
      if (editTextName.getText().toString().equals("")) {
        editTextName.setError("Please enter your name");
      }
    }
    return nameVerified;
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }

  public interface YesNoListener {
    void onStartNew(String userName);

    //void onNo();
  }
}
