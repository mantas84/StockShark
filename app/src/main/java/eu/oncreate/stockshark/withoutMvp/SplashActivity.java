package eu.oncreate.stockshark.withoutMvp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import butterknife.BindView;
import butterknife.ButterKnife;
import eu.oncreate.stockshark.App;
import eu.oncreate.stockshark.R;
import eu.oncreate.stockshark.data.constants.StockConstants;
import eu.oncreate.stockshark.data.database.DatabasePaper;
import eu.oncreate.stockshark.data.models.Stock;
import eu.oncreate.stockshark.data.models.User;
import eu.oncreate.stockshark.view.impl.MainActivity;
import java.util.LinkedList;
import java.util.Random;
import timber.log.Timber;

public class SplashActivity extends AppCompatActivity implements CreateUserDialog.YesNoListener {

  @BindView(R.id.determinateBar)
  ProgressBar progressBar;
  private boolean initInProgres = false;
  private DatabasePaper databasePaper;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_splash);

    ButterKnife.bind(this);

    databasePaper = new DatabasePaper();
    User user = databasePaper.getUser();
    if (user != null) {
      ((App) getApplication()).createUserComponent(databasePaper);
      startMainActivity();
    } else {
      showEditDialog();
    }
  }

  private void startMainActivity() {
    Intent intent = new Intent(this, MainActivity.class);
    startActivity(intent);
    finish();
  }

  @Override
  public void onStartNew(String userName) {
    CreateUserDialog fragment = (CreateUserDialog) getSupportFragmentManager().findFragmentByTag(
        CreateUserDialog.class.getSimpleName());
    if (fragment != null) {
      fragment.dismiss();
    }
    preparePaperDbData(userName);
    startMainActivity();
  }

  private void preparePaperDbData(String userName) {
    progressBar.setVisibility(View.VISIBLE);
    Timber.d("Users name = %s", userName);
    User user = new User(userName);
    user.setMoney(10000.0d);

    databasePaper.insertUser(user);

    for (String stockName : StockConstants.STOCKS_NAMES) {
      double DELTA = (new Random().nextDouble()) * 2 - 1;
      double base_Price = StockConstants.getStockBasePrice(stockName);

      Stock stock = new Stock(stockName, base_Price, base_Price);
      Stock stock1 = new Stock(stockName, base_Price + DELTA, base_Price);

      LinkedList<Stock> stocks = new LinkedList<>();
      stocks.add(stock);
      stocks.add(stock1);

      databasePaper.insertStocks(stocks);
    }
    progressBar.setVisibility(View.GONE);
    ((App) getApplication()).createUserComponent(databasePaper);
  }

  private void showEditDialog() {
    FragmentManager fm = getSupportFragmentManager();
    if (fm.findFragmentByTag(CreateUserDialog.class.getSimpleName()) == null) {
      CreateUserDialog createUserDialog = new CreateUserDialog();
      createUserDialog.show(fm, createUserDialog.getClass().getSimpleName());
    }
  }
}
