package eu.oncreate.stockshark.withoutMvp;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import eu.oncreate.stockshark.R;
import timber.log.Timber;

/**
 * Created by mantas on 9/27/17.
 */

public class HireReleaseStaffDialog extends DialogFragment {

  public static final String MONEY = "money";
  public static final String STAFF_COUNT = "staff_count";
  public static final Float DEFAULT_TAX = 0.1f;
  public static final double DEFAULT_STAFF_COST = 2000.0d;
  public static final String STAFF_COUNT1 = "staffCount";
  @BindView(R.id.btn_increase)
  FloatingActionButton buttonIncrease;
  @BindView(R.id.btn_decrease)
  FloatingActionButton buttonDecrease;
  @BindView(R.id.btn_OK)
  Button buttonOk;
  @BindView(R.id.txt_staffCount)
  TextView textViewStaffCount;
  @BindView(R.id.txt_estimatedTax)
  TextView textViewTax;
  @BindView(R.id.txt_staffHireCost)
  TextView textViewStaffHireCost;
  private Unbinder unbinder;
  private double money;
  private int staffCountReal;
  private int staffCountField;
  private HireReleaseStaffListener callback;

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    Activity activity = (Activity) context;
    try {
      callback = (HireReleaseStaffListener) activity;
    } catch (ClassCastException e) {
      throw new ClassCastException(activity.toString() + " must implement SimulationStartAndGo");
    }
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.dialog_staff, container);
    unbinder = ButterKnife.bind(this, view);

    if (savedInstanceState != null) {
      staffCountField = savedInstanceState.getInt(STAFF_COUNT1);
    } else {
      staffCountField = staffCountReal;
    }
    textViewStaffHireCost.setText(String.format("%.0f", staffCountField * DEFAULT_STAFF_COST));
    textViewStaffCount.setText(String.valueOf(staffCountField));
    textViewTax.setText(getTax(staffCountField));

    buttonDecrease.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (staffCountField > 0) {
          staffCountField -= 1;
          textViewStaffCount.setText(String.valueOf(staffCountField));
          textViewTax.setText(getTax(staffCountField));
          textViewStaffHireCost.setText(
              String.format("%.0f", staffCountField * DEFAULT_STAFF_COST));
        }
      }
    });

    buttonIncrease.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        staffCountField += 1;
        textViewStaffCount.setText(String.valueOf(staffCountField));
        textViewTax.setText(getTax(staffCountField));
        textViewStaffHireCost.setText(String.format("%.0f", staffCountField * DEFAULT_STAFF_COST));
      }
    });

    buttonOk.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (money > staffCountField * DEFAULT_STAFF_COST) {
          callback.staffChange(staffCountField, DEFAULT_STAFF_COST);
        } else {
          Toast.makeText(getActivity(), getActivity().getString(R.string.not_enough_money),
              Toast.LENGTH_SHORT).show();
        }
      }
    });

    this.setCancelable(true);

    return view;
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    money = getArguments().getDouble(MONEY);
    staffCountReal = getArguments().getInt(STAFF_COUNT);
    //maxQuantity = getArguments().getInt(MAX);
    //sell = getArguments().getBoolean(SELL);
  }

  @Override
  public void onDetach() {
    super.onDetach();
    callback = null;
  }

  private String getTax(int StaffCount) {
    float tax = (float) (DEFAULT_TAX * Math.pow(0.95f, StaffCount));
    String taxString = String.format("%.2f", tax * 100);
    taxString = taxString + "%";
    return taxString;
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    Timber.d("saved staff %s", staffCountField);
    outState.putInt(STAFF_COUNT1, staffCountField);
  }

  public interface HireReleaseStaffListener {
    void staffChange(int newStaffCount, double staffCost);

    void cancel();
  }
}
