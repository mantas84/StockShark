package eu.oncreate.stockshark.withoutMvp;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.graphics.Palette;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import eu.oncreate.stockshark.R;
import eu.oncreate.stockshark.helpers.GetVectorsHelper;
import eu.oncreate.stockshark.helpers.StringToResStringHelper;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import java.util.concurrent.TimeUnit;
import org.threeten.bp.Instant;
import timber.log.Timber;

public class AssetItemFragment extends Fragment {
  public static final String LAST_GAINED = "last_gained";
  private static final String ARG_ASSET_NAME = "assetName";
  private static final String ARG_ASSET_PROFIT = "asset_profit";
  private static final String ARG_ASSET_PRICE = "asset_price";
  private static final String ARG_ASSET_LIMIT = "asset_limit";
  private static final String ARG_ASSET_OWNED = "asset_owned";
  private static final String ARG_ASSET_LAST_GAINED = "asset_last_gained";
  private static final String ARG_ASSET_INTERVAL = "asset_interval";
  final int PIECES_COUNT = 1000;
  @BindView(R.id.txt_assetName)
  TextView textViewName;
  @BindView(R.id.txt_profit)
  TextView textViewProfit;
  @BindView(R.id.txt_price)
  TextView textViewPrice;
  @BindView(R.id.txt_limit)
  TextView textViewLimit;
  @BindView(R.id.txt_owned)
  TextView textViewOwned;
  @BindView(R.id.img_asset)
  ImageView imageViewAsset;
  @BindView(R.id.progressBar_asset)
  ProgressBar progressBar;
  @BindView(R.id.btn_buy)
  Button buttonBuy;
  @BindView(R.id.constraintLayout_asset_item)
  View rootView;
  private double profit, price;
  private int limit, owned;
  private long lastGained, interval;
  private long onePieceLengthMillis;
  private int currentPiecesCount;
  private Observable<Long> observable;
  private DisposableObserver<Long> disposableObserver;
  private Unbinder unbinder;
  private String name;
  private OnFragmentInteractionListener listener;

  public AssetItemFragment() {
    // Required empty public constructor
  }

  public static AssetItemFragment newInstance(String name, double profit, double price, int limit,
      int owned, long lastGained, long interval) {
    AssetItemFragment fragment = new AssetItemFragment();
    Bundle args = new Bundle();
    args.putString(ARG_ASSET_NAME, name);
    args.putDouble(ARG_ASSET_PROFIT, profit);
    args.putDouble(ARG_ASSET_PRICE, price);
    args.putInt(ARG_ASSET_LIMIT, limit);
    args.putInt(ARG_ASSET_OWNED, owned);
    args.putLong(ARG_ASSET_LAST_GAINED, lastGained);
    args.putLong(ARG_ASSET_INTERVAL, interval);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    if (getArguments() != null) {
      name = getArguments().getString(ARG_ASSET_NAME);
      profit = getArguments().getDouble(ARG_ASSET_PROFIT);
      price = getArguments().getDouble(ARG_ASSET_PRICE);
      limit = getArguments().getInt(ARG_ASSET_LIMIT);
      owned = getArguments().getInt(ARG_ASSET_OWNED);
      lastGained = getArguments().getLong(ARG_ASSET_LAST_GAINED);
      interval = getArguments().getLong(ARG_ASSET_INTERVAL);
      onePieceLengthMillis = (interval * 1000) / PIECES_COUNT;
      currentPiecesCount =
          (int) (((Instant.now().getEpochSecond() - lastGained) * 1000) / onePieceLengthMillis);
    }

    if (savedInstanceState != null) {
      lastGained = savedInstanceState.getLong(LAST_GAINED);
    }

    currentPiecesCount =
        (int) (((Instant.now().getEpochSecond() - lastGained) * 1000) / onePieceLengthMillis);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_asset_item, container, false);
    unbinder = ButterKnife.bind(this, view);

    imageViewAsset.setImageDrawable(GetVectorsHelper.getDrawable(getActivity(), name));

    setBackgroundColorAndProgressBar();

    textViewName.setText(StringToResStringHelper.getAssetString(getActivity(), name));
    textViewProfit.setText(String.format("%.2f", profit));
    textViewPrice.setText(String.format("%.2f", price));
    textViewOwned.setText(String.valueOf(owned));
    textViewLimit.setText(String.valueOf(limit));
    if (owned == 0) {
      progressBar.setVisibility(View.INVISIBLE);
    } else {
      progressBar.setVisibility(View.VISIBLE);
    }
    progressBar.setMax(PIECES_COUNT);
    return view;
  }

  private void setBackgroundColorAndProgressBar() {
    Drawable drawable =
        ContextCompat.getDrawable(getActivity(), GetVectorsHelper.getDrawableInt(name));

    Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(),
        Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(bitmap);
    drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
    drawable.draw(canvas);

    Palette palette = Palette.from(bitmap).maximumColorCount(8).generate();
    Timber.d("Swatch count %s", palette.getSwatches().size());
    if (palette.getMutedSwatch() != null) {
      Timber.d("getMutedSwatch used");
      rootView.setBackgroundColor(palette.getMutedSwatch().getRgb());
    } else {
      if (palette.getDarkMutedSwatch() != null) {
        Timber.d("getDarkMutedSwatch used");
        rootView.setBackgroundColor(palette.getDarkMutedSwatch().getRgb());
      } else {
        if (palette.getLightMutedSwatch() != null) {
          Timber.d("getLightMutedSwatch used");
          rootView.setBackgroundColor(palette.getLightMutedSwatch().getRgb());
        }
      }
    }

    if (palette.getLightVibrantSwatch() != null) {
      progressBar.setProgressTintList(
          ColorStateList.valueOf(palette.getLightVibrantSwatch().getRgb()));
    }
  }

  //// TODO: Rename method, update argument and hook method into UI event
  //public void onButtonPressed(Uri uri) {
  //  if (listener != null) {
  //    listener.onFragmentBuyClicked(uri);
  //  }
  //}

  @Override
  public void onResume() {
    super.onResume();
    Timber.d("onResume %s, owned: %s", name, owned);
    if (owned > 0) startObservingProgress();
  }

  private void startObservingProgress() {

    Timber.d("onResume lastGained %s", lastGained);

    long now = Instant.now().getEpochSecond();
    if (lastGained == 0) {
      lastGained = now;
    }
    currentPiecesCount = (int) (((now - lastGained) * 1000) / onePieceLengthMillis);
    //Timber.d("%s name, currentPiecesCount %s", name, currentPiecesCount);
    //Timber.d("%s startObserving, %s onePieceLengthMillis, %s lastGained, %s now, %s dif", name,
    //    onePieceLengthMillis, lastGained, now * 1000, (now * 1000 - lastGained));

    observable = Observable.interval(onePieceLengthMillis, TimeUnit.MILLISECONDS)
        .subscribeOn(Schedulers.computation())
        .observeOn(AndroidSchedulers.mainThread());

    disposableObserver = new DisposableObserver<Long>() {
      @Override
      public void onNext(@NonNull Long aLong) {
        //Timber.d("currentPiecesCount %s", currentPiecesCount);
        currentPiecesCount += 1;
        updateProgressBar(currentPiecesCount);
        if (currentPiecesCount >= PIECES_COUNT) {
          Timber.d("Reached max");
          currentPiecesCount = 0;
          lastGained = Instant.now().getEpochSecond();
          listener.onFragmentMoneyGainFromAsset(name);
        }
      }

      @Override
      public void onError(@NonNull Throwable e) {
        Timber.d(e.toString());
      }

      @Override
      public void onComplete() {
        Timber.d("onComplete");
      }
    };

    observable.subscribe(disposableObserver);
  }

  private void updateProgressBar(int currentPieces) {
    progressBar.setProgress(currentPieces);
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnFragmentInteractionListener) {
      listener = (OnFragmentInteractionListener) context;
    } else {
      throw new RuntimeException(
          context.toString() + " must implement OnFragmentInteractionListener");
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    listener = null;
  }

  @Override
  public void onPause() {
    super.onPause();
    if (disposableObserver != null) disposableObserver.dispose();
    Timber.d("onPause lastGained %s", lastGained);
  }

  @OnClick(R.id.btn_buy)
  void buy() {
    listener.onFragmentBuyClicked(name);
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putLong(LAST_GAINED, lastGained);
  }

  public interface OnFragmentInteractionListener {
    void onFragmentBuyClicked(String name);

    void onFragmentMoneyGainFromAsset(String name);
  }
}
