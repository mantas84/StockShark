package eu.oncreate.stockshark.presenter;

import eu.oncreate.stockshark.view.DetailsView;

public interface DetailsPresenter extends BasePresenter<DetailsView> {

  void startSimulation();

  void stopSimulation();

  void buyStocks(String quantity);

  void sellStocks(String quantity);
}