package eu.oncreate.stockshark.presenter.impl;

import android.support.annotation.NonNull;
import eu.oncreate.stockshark.data.models.Asset;
import eu.oncreate.stockshark.interactor.AssetsInteractor;
import eu.oncreate.stockshark.presenter.AssetsPresenter;
import eu.oncreate.stockshark.view.AssetsView;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import java.util.List;
import javax.inject.Inject;
import timber.log.Timber;

public final class AssetsPresenterImpl extends BasePresenterImpl<AssetsView>
    implements AssetsPresenter {
  /**
   * The interactor
   */
  @NonNull
  private final AssetsInteractor mInteractor;

  private Single<List<Asset>> single;
  private DisposableSingleObserver<List<Asset>> observer;

  // The view is available using the mView variable

  @Inject
  public AssetsPresenterImpl(@NonNull AssetsInteractor interactor) {
    mInteractor = interactor;
  }

  @Override
  public void onStart(boolean viewCreated) {
    super.onStart(viewCreated);

    getDataFromDb();
  }

  private void getDataFromDb() {
    single = mInteractor.getData();
    observer = new DisposableSingleObserver<List<Asset>>() {
      @Override
      public void onSuccess(@io.reactivex.annotations.NonNull List<Asset> assets) {
        if (mView != null) {
          mView.gotData(assets);
        }
      }

      @Override
      public void onError(@io.reactivex.annotations.NonNull Throwable e) {
        if (mView != null) {
          mView.onGetDataError(e.toString());
        }
      }
    };
    single.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    single.subscribe(observer);
  }

  @Override
  public void onStop() {
    // Your code here, mView will be null after this method until next onStart()

    super.onStop();
  }

  @Override
  public void onPresenterDestroyed() {
        /*
         * Your code here. After this method, your presenter (and view) will be completely destroyed
         * so make sure to cancel any HTTP call or database connection
         */

    super.onPresenterDestroyed();
  }

  @Override
  public void reloadData() {
    getDataFromDb();
  }

  @Override
  public void tryToBuy(String name, long epochSeconds) {
    if (mInteractor.canUserBuyIt(name)) {
      Single<List<Asset>> completable = mInteractor.buyAsset(name, epochSeconds);
      completable.subscribeOn(Schedulers.io())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe(new DisposableSingleObserver<List<Asset>>() {
            @Override
            public void onSuccess(@io.reactivex.annotations.NonNull List<Asset> assets) {
              Timber.d("onSuccess called");
              if (mView != null) {
                mView.buySuccessful(assets);
              }
            }

            @Override
            public void onError(@io.reactivex.annotations.NonNull Throwable e) {
              if (mView != null) {
                Timber.d("onError called");
                mView.onGetBuyError(e.toString());
              }
            }
          });
    } else {
      Single<String> single = mInteractor.whyCantBuyIt(name);
      single.observeOn(AndroidSchedulers.mainThread())
          .subscribeOn(Schedulers.io())
          .subscribe(new DisposableSingleObserver<String>() {
            @Override
            public void onSuccess(@io.reactivex.annotations.NonNull String s) {
              if (mView != null) {
                mView.cantBuyIt(s);
              }
            }

            @Override
            public void onError(@io.reactivex.annotations.NonNull Throwable e) {
              Timber.d(e.toString());
            }
          });
    }
  }

  @Override
  public void MoneyGained(String name, long timeNow) {
    Completable completable = mInteractor.assetMoneyGained(name, timeNow);
    completable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new DisposableCompletableObserver() {
          @Override
          public void onComplete() {
            Timber.d("Completed");
          }

          @Override
          public void onError(@io.reactivex.annotations.NonNull Throwable e) {
            Timber.d("-------------------Not saved -----------------------");
            Timber.d(e.toString());
          }
        });
  }
}