package eu.oncreate.stockshark.presenter.impl;

import android.support.annotation.NonNull;
import eu.oncreate.stockshark.data.models.Achievements;
import eu.oncreate.stockshark.interactor.AchievementsInteractor;
import eu.oncreate.stockshark.presenter.AchievementsPresenter;
import eu.oncreate.stockshark.view.AchievementsView;
import io.reactivex.Single;
import io.reactivex.observers.DisposableSingleObserver;
import java.util.List;
import javax.inject.Inject;

public final class AchievementsPresenterImpl extends BasePresenterImpl<AchievementsView>
    implements AchievementsPresenter {
  /**
   * The interactor
   */
  @NonNull
  private final AchievementsInteractor mInteractor;

  private Single<List<Achievements>> achievement;
  private DisposableSingleObserver<List<Achievements>> observer;
  // The view is available using the mView variable

  @Inject
  public AchievementsPresenterImpl(@NonNull AchievementsInteractor interactor) {
    mInteractor = interactor;
  }

  @Override
  public void onStart(boolean viewCreated) {
    super.onStart(viewCreated);
    getDataFromDB();
    // Your code here. Your view is available using mView and will not be null until next onStop()
  }

  private void getDataFromDB() {
    achievement = mInteractor.getData();
    observer = new DisposableSingleObserver<List<Achievements>>() {
      @Override
      public void onSuccess(@io.reactivex.annotations.NonNull List<Achievements> achievements1) {
        if (mView != null) {
          mView.provideData(achievements1);
        }
      }

      @Override
      public void onError(@io.reactivex.annotations.NonNull Throwable e) {
        if (mView != null) {
          mView.onError("Something went wrong");
        }
      }
    };
    achievement.subscribe(observer);
  }

  @Override
  public void onStop() {
    // Your code here, mView will be null after this method until next onStart()

    super.onStop();
  }

  @Override
  public void onPresenterDestroyed() {
        /*
         * Your code here. After this method, your presenter (and view) will be completely destroyed
         * so make sure to cancel any HTTP call or database connection
         */

    super.onPresenterDestroyed();
  }

  @Override
  public void getData() {
    getDataFromDB();
  }
}