package eu.oncreate.stockshark.presenter.impl;

import android.support.annotation.NonNull;
import eu.oncreate.stockshark.data.models.OwnedStockWithCurrentPrice;
import eu.oncreate.stockshark.data.models.User;
import eu.oncreate.stockshark.interactor.UserInfoInteractor;
import eu.oncreate.stockshark.presenter.UserInfoPresenter;
import eu.oncreate.stockshark.view.UserInfoView;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import timber.log.Timber;

public final class UserInfoPresenterImpl extends BasePresenterImpl<UserInfoView>
    implements UserInfoPresenter {
  /**
   * The interactor
   */
  @NonNull
  private final UserInfoInteractor mInteractor;

  private double stockWorth;
  private long stocksCount;
  private boolean groupStocks = false;
  private List<OwnedStockWithCurrentPrice> ownedStockWithCurrentPricesList;
  private List<OwnedStockWithCurrentPrice> ownedStockWithCurrentPricesListGrouped;

  // The view is available using the mView variable

  @Inject
  public UserInfoPresenterImpl(@NonNull UserInfoInteractor interactor) {
    mInteractor = interactor;
  }

  @Override
  public void onStart(boolean viewCreated) {
    super.onStart(viewCreated);

    getStocksData();

    getUserData();

    // Your code here. Your view is available using mView and will not be null until next onStop()
  }

  private void getStocksData() {
    Single<List<OwnedStockWithCurrentPrice>> ownedStocksList = mInteractor.getStocksData();
    ownedStocksList.observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe(new DisposableSingleObserver<List<OwnedStockWithCurrentPrice>>() {
          @Override
          public void onSuccess(@io.reactivex.annotations.NonNull
              List<OwnedStockWithCurrentPrice> ownedStockWithCurrentPrices) {
            ownedStockWithCurrentPricesList = ownedStockWithCurrentPrices;
            setGroupStocks(ownedStockWithCurrentPrices);
            stockWorth = getStocksWorth(ownedStockWithCurrentPrices);
            stocksCount = getStocksCount(ownedStockWithCurrentPrices);
            if (mView != null) {
              mView.stockAreReady();
            }
          }

          private long getStocksCount(
              List<OwnedStockWithCurrentPrice> ownedStockWithCurrentPrices) {
            long total = 0;
            for (OwnedStockWithCurrentPrice ownedStock : ownedStockWithCurrentPrices) {
              total += ownedStock.getStocksCount();
            }
            return total;
          }

          @Override
          public void onError(@io.reactivex.annotations.NonNull Throwable e) {
            Timber.d("onError %s", e.toString());
          }
        });
  }

  private double getStocksWorth(List<OwnedStockWithCurrentPrice> ownedStockWithCurrentPrices) {
    double worth = 0.0;
    if (ownedStockWithCurrentPrices.size() > 0) {
      for (OwnedStockWithCurrentPrice o : ownedStockWithCurrentPrices) {
        worth = worth + (o.getCurrentPrice() + o.getStocksCount());
      }
    }
    return worth;
  }

  private void setGroupStocks(List<OwnedStockWithCurrentPrice> ownedStockWithCurrentPrices) {
    Map<String, OwnedStockWithCurrentPrice> groupedMap = new HashMap();

    if (ownedStockWithCurrentPrices != null && ownedStockWithCurrentPrices.size() > 0) {
      for (OwnedStockWithCurrentPrice ownedStockWithCurrentPrice : ownedStockWithCurrentPrices) {
        if (!groupedMap.containsKey(ownedStockWithCurrentPrice.getName())) {
          groupedMap.put(ownedStockWithCurrentPrice.getName(), ownedStockWithCurrentPrice);
        } else {
          OwnedStockWithCurrentPrice old = groupedMap.get(ownedStockWithCurrentPrice.getName());
          double totalValue = old.getCurrentPrice() * old.getStocksCount();
          double bougthValue = old.getPrice() * old.getStocksCount();
          int newCount = old.getStocksCount() + ownedStockWithCurrentPrice.getStocksCount();
          double newBoughtPrice = (bougthValue + (ownedStockWithCurrentPrice.getPrice()
              * ownedStockWithCurrentPrice.getStocksCount())) / newCount;
          double newTotalPrice = (totalValue + (ownedStockWithCurrentPrice.getCurrentPrice()
              * ownedStockWithCurrentPrice.getStocksCount())) / newCount;

          OwnedStockWithCurrentPrice newStock =
              new OwnedStockWithCurrentPrice(old.getName(), newCount, newBoughtPrice,
                  newTotalPrice);
          groupedMap.put(newStock.getName(), newStock);
        }
      }
    }
    ownedStockWithCurrentPricesListGrouped = new ArrayList<>(groupedMap.values());
  }

  private void getUserData() {
    Single<User> userSingle = mInteractor.getUser();
    userSingle.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new DisposableSingleObserver<User>() {
          @Override
          public void onSuccess(@io.reactivex.annotations.NonNull User user) {
            if (mView != null) {
              String money = String.format("%.2f", user.getMoney());
              mView.setData(user.getName(), money, user.getOwnedStocksList().size(),
                  user.getStaffCount());
            }
          }

          @Override
          public void onError(@io.reactivex.annotations.NonNull Throwable e) {
            Timber.d("onError %s", e.toString());
          }
        });
  }

  @Override
  public void onStop() {
    // Your code here, mView will be null after this method until next onStart()

    super.onStop();
  }

  @Override
  public void onPresenterDestroyed() {
        /*
         * Your code here. After this method, your presenter (and view) will be completely destroyed
         * so make sure to cancel any HTTP call or database connection
         */

    super.onPresenterDestroyed();
  }

  @Override
  public String getStockWorth() {

    return String.format("%.2f", stockWorth);
  }

  @Override
  public void getData(boolean group) {
    groupStocks = group;
    if (group) {
      if (mView != null) {
        mView.setStocksData(ownedStockWithCurrentPricesListGrouped);
      }
    } else {
      if (mView != null) {
        mView.setStocksData(ownedStockWithCurrentPricesList);
      }
    }
  }

  @Override
  public String getStocksCount() {
    return String.valueOf(stocksCount);
  }

  @Override
  public void staffChanged(int newStaffCount, double staffCostPerUnit) {
    mInteractor.staffChanged(newStaffCount, staffCostPerUnit)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new DisposableCompletableObserver() {
          @Override
          public void onComplete() {
            Timber.d("onComplete");
            getUserData();
          }

          @Override
          public void onError(@io.reactivex.annotations.NonNull Throwable e) {
            Timber.d("onError %s", e.toString());
          }
        });
  }
}