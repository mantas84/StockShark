package eu.oncreate.stockshark.presenter.impl;

import android.support.annotation.NonNull;
import eu.oncreate.stockshark.interactor.RetireInteractor;
import eu.oncreate.stockshark.presenter.RetirePresenter;
import eu.oncreate.stockshark.view.RetireView;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;
import javax.inject.Inject;

public final class RetirePresenterImpl extends BasePresenterImpl<RetireView>
    implements RetirePresenter {
  /**
   * The interactor
   */
  @NonNull
  private final RetireInteractor mInteractor;
  private boolean retired;

  // The view is available using the mView variable

  @Inject
  public RetirePresenterImpl(@NonNull RetireInteractor interactor) {
    mInteractor = interactor;
  }

  @Override
  public void onStart(boolean viewCreated) {
    super.onStart(viewCreated);

    Completable completable = mInteractor.retire();
    completable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new DisposableCompletableObserver() {
          @Override
          public void onComplete() {
            retired = true;
            if (mView != null) {
              mView.retired();
            }
          }

          @Override
          public void onError(@io.reactivex.annotations.NonNull Throwable e) {

          }
        });
    // Your code here. Your view is available using mView and will not be null until next onStop()
  }

  @Override
  public void onStop() {
    // Your code here, mView will be null after this method until next onStart()

    super.onStop();
  }

  @Override
  public void onPresenterDestroyed() {
        /*
         * Your code here. After this method, your presenter (and view) will be completely destroyed
         * so make sure to cancel any HTTP call or database connection
         */

    super.onPresenterDestroyed();
  }

  @Override
  public boolean checkForRetired() {
    return retired;
  }
}