package eu.oncreate.stockshark.presenter;

import eu.oncreate.stockshark.view.StockListView;

public interface StockListPresenter extends BasePresenter<StockListView> {

}