package eu.oncreate.stockshark.presenter;

import eu.oncreate.stockshark.view.AchievementsView;

public interface AchievementsPresenter extends BasePresenter<AchievementsView> {

  void getData();
}