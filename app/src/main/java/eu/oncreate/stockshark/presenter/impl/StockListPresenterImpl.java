package eu.oncreate.stockshark.presenter.impl;

import android.support.annotation.NonNull;
import eu.oncreate.stockshark.data.models.Stock;
import eu.oncreate.stockshark.interactor.StockListInteractor;
import eu.oncreate.stockshark.presenter.StockListPresenter;
import eu.oncreate.stockshark.view.StockListView;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import java.util.LinkedList;
import java.util.List;
import javax.inject.Inject;
import timber.log.Timber;

public final class StockListPresenterImpl extends BasePresenterImpl<StockListView>
    implements StockListPresenter {
  /**
   * The interactor
   */
  @NonNull
  private final StockListInteractor mInteractor;

  private Observable<List<LinkedList<Stock>>> observable;
  private DisposableObserver<List<LinkedList<Stock>>> disposableObserver;

  // The view is available using the mView variable

  @Inject
  public StockListPresenterImpl(@NonNull StockListInteractor interactor) {
    mInteractor = interactor;
  }

  @Override
  public void onStart(boolean viewCreated) {
    super.onStart(viewCreated);

    // Your code here. Your view is available using mView and will not be null until next onStop()
    Single<List<LinkedList<Stock>>> data = null;
    if (mView != null) {
      data = mInteractor.getInitData(mView.getStocksNames());
    }
    data.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new DisposableSingleObserver<List<LinkedList<Stock>>>() {
          @Override
          public void onSuccess(
              @io.reactivex.annotations.NonNull List<LinkedList<Stock>> linkedLists) {
            if (mView != null) mView.showStocksFirstTime(linkedLists);
          }

          @Override
          public void onError(@io.reactivex.annotations.NonNull Throwable e) {
            Timber.d("onError: %s", e.toString());
          }
        });

    observable = mInteractor.getDataUpdates(mView.getStocksNames());
    disposableObserver = new DisposableObserver<List<LinkedList<Stock>>>() {
      @Override
      public void onNext(@io.reactivex.annotations.NonNull List<LinkedList<Stock>> linkedLists) {
        if (mView != null) mView.getStocksUpdates(linkedLists);
      }

      @Override
      public void onError(@io.reactivex.annotations.NonNull Throwable e) {
        Timber.d("onError %s", e.toString());
      }

      @Override
      public void onComplete() {
        Timber.d("onComplete");
      }
    };
    observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    observable.subscribe(disposableObserver);
  }

  @Override
  public void onStop() {
    // Your code here, mView will be null after this method until next onStart()

    super.onStop();
  }

  @Override
  public void onPresenterDestroyed() {
        /*
         * Your code here. After this method, your presenter (and view) will be completely destroyed
         * so make sure to cancel any HTTP call or database connection
         */
    if (disposableObserver != null) {
      if (!disposableObserver.isDisposed()) disposableObserver.dispose();
    }
    super.onPresenterDestroyed();
  }
}