package eu.oncreate.stockshark.presenter;

import eu.oncreate.stockshark.view.MainView;

public interface MainPresenter extends BasePresenter<MainView> {

  void startSimulation();

  void stopSimulation();

  void rewardVideoWatched(long timeNow);

  boolean getRewardButtonState();

  void getRetiringScore();
}