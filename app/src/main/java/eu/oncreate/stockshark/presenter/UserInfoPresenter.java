package eu.oncreate.stockshark.presenter;

import eu.oncreate.stockshark.view.UserInfoView;

public interface UserInfoPresenter extends BasePresenter<UserInfoView> {

  String getStockWorth();

  void getData(boolean group);

  String getStocksCount();

  void staffChanged(int newStaffCount, double staffCostPerUnit);
}