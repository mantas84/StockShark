package eu.oncreate.stockshark.presenter.loader;

import android.support.annotation.NonNull;
import eu.oncreate.stockshark.presenter.BasePresenter;

/**
 * Factory to implement to create a presenter
 */
public interface PresenterFactory<T extends BasePresenter> {
  @NonNull
  T create();
}
