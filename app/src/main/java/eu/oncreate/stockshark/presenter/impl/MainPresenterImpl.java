package eu.oncreate.stockshark.presenter.impl;

import android.support.annotation.NonNull;
import eu.oncreate.stockshark.data.constants.StockConstants;
import eu.oncreate.stockshark.data.models.Stock;
import eu.oncreate.stockshark.interactor.MainInteractor;
import eu.oncreate.stockshark.presenter.MainPresenter;
import eu.oncreate.stockshark.view.MainView;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import java.util.LinkedList;
import java.util.List;
import javax.inject.Inject;
import timber.log.Timber;

public final class MainPresenterImpl extends BasePresenterImpl<MainView> implements MainPresenter {
  /**
   * The interactor
   */
  @NonNull
  private final MainInteractor mInteractor;

  private String[] currentStockGroup;
  private boolean enableRewardVideo;
  private Observable<List<LinkedList<Stock>>> observableStocks;
  private DisposableObserver disposableObserverSave;
  private DisposableObserver<List<LinkedList<Stock>>> disposableObserver;
  private Observable<Integer> observableEnableRewardVideo;
  private DisposableObserver<Integer> disposableObserverEnableRewardVideo;
  private Integer countdownTimer;

  // The view is available using the mView variable

  @Inject
  public MainPresenterImpl(@NonNull MainInteractor interactor) {
    mInteractor = interactor;
  }

  @Override
  public void onStart(boolean viewCreated) {
    super.onStart(viewCreated);

    // Your code here. Your view is available using mView and will not be null until next onStop()
    if (currentStockGroup == null) {
      currentStockGroup = StockConstants.STOCKS_NAMES_IT;
    }
  }

  @Override
  public void onStop() {
    // Your code here, mView will be null after this method until next onStart()

    super.onStop();
  }

  @Override
  public void onPresenterDestroyed() {
        /*
         * Your code here. After this method, your presenter (and view) will be completely destroyed
         * so make sure to cancel any HTTP call or database connection
         */

    currentStockGroup = null;
    if (!disposableObserver.isDisposed()) disposableObserver.dispose();
    if (!disposableObserverSave.isDisposed()) disposableObserverSave.dispose();
    super.onPresenterDestroyed();
  }

  @Override
  public void startSimulation() {
    if ((disposableObserver != null) && (!disposableObserver.isDisposed())) {
      disposableObserver.dispose();
    }

    if (countdownTimer == null || countdownTimer == 0) {
      setUpObservables();
    } else {
      resumeRewardVideoWatched();
    }
  }

  private void setUpObservables() {
    observableStocks = mInteractor.startSimulation(currentStockGroup)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io());
    disposableObserver = new DisposableObserver<List<LinkedList<Stock>>>() {
      @Override
      public void onNext(@io.reactivex.annotations.NonNull List<LinkedList<Stock>> linkedLists) {
        if (mView != null) {
          //does nothing for now
          //mView.updateStocks(linkedLists);
          mView.getAchievements(mInteractor.getNewUpdateAchievements());
        }
      }

      @Override
      public void onError(@io.reactivex.annotations.NonNull Throwable e) {
        Timber.d("Error %s", e.toString());
      }

      @Override
      public void onComplete() {
        Timber.d("disposableObserver onComplete");
      }
    };

    if ((disposableObserverSave != null) && (!disposableObserverSave.isDisposed())) {
      disposableObserverSave.dispose();
    }
    disposableObserverSave = new DisposableObserver() {
      @Override
      public void onNext(@io.reactivex.annotations.NonNull Object o) {
        mInteractor.saveStocks();
      }

      @Override
      public void onError(@io.reactivex.annotations.NonNull Throwable e) {
        Timber.d("onError %s", e.toString());
      }

      @Override
      public void onComplete() {

      }
    };

    observableStocks.subscribe(disposableObserver);
    observableStocks.buffer(4)
        .subscribeOn(Schedulers.io())
        .observeOn(Schedulers.io())
        .subscribe(disposableObserverSave);
  }

  @Override
  public void stopSimulation() {
    if (disposableObserverEnableRewardVideo != null) disposableObserverEnableRewardVideo.dispose();
    disposableObserver.dispose();
    disposableObserverSave.dispose();
  }

  @Override
  public void rewardVideoWatched(long timeNow) {
    stopSimulation();
    rewardCountdown(timeNow);
    //startSimulation();
  }

  @Override
  public boolean getRewardButtonState() {
    return (countdownTimer == null || countdownTimer < 1);
  }

  @Override
  public void getRetiringScore() {
    Single<Double> getScore = mInteractor.getScore();
    getScore.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new DisposableSingleObserver<Double>() {
          @Override
          public void onSuccess(@io.reactivex.annotations.NonNull Double aDouble) {
            if (mView != null) {
              mView.gotScore(aDouble);
            }
          }

          @Override
          public void onError(@io.reactivex.annotations.NonNull Throwable e) {

          }
        });
  }

  private void resumeRewardVideoWatched() {
    stopSimulation();
    rewardCountdown(0);
    setUpObservables();
    observableEnableRewardVideo.subscribe(disposableObserverEnableRewardVideo);
  }

  private void rewardCountdown(long timeNow) {
    if (countdownTimer == null || countdownTimer == 0) countdownTimer = 60;
    observableEnableRewardVideo = mInteractor.rewardVideoWatched(timeNow, countdownTimer)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io());
    disposableObserverEnableRewardVideo = new DisposableObserver<Integer>() {
      @Override
      public void onNext(@io.reactivex.annotations.NonNull Integer integer) {
        countdownTimer = integer;
        if (integer > 0) {
          enableRewardVideo = false;
        } else {
          enableRewardVideo = true;
          onComplete();
        }
        if (mView != null) {
          mView.updateCountdown(integer);
        }
      }

      @Override
      public void onError(@io.reactivex.annotations.NonNull Throwable e) {
        Timber.d("onError %s", e.toString());
      }

      @Override
      public void onComplete() {
        Timber.d("onComplete");
        mInteractor.resetSimTime();
        stopSimulation();
        startSimulation();
        if (mView != null) mView.rewardVideoEnable(enableRewardVideo);
      }
    };

    //observableEnableRewardVideo.subscribe(disposableObserverEnableRewardVideo);
  }
}