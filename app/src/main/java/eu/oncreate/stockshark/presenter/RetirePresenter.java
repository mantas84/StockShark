package eu.oncreate.stockshark.presenter;

import eu.oncreate.stockshark.view.RetireView;

public interface RetirePresenter extends BasePresenter<RetireView> {

  boolean checkForRetired();
}