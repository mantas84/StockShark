package eu.oncreate.stockshark.presenter.impl;

import android.support.annotation.NonNull;
import eu.oncreate.stockshark.data.models.OwnedStocks;
import eu.oncreate.stockshark.data.models.Stock;
import eu.oncreate.stockshark.data.models.User;
import eu.oncreate.stockshark.interactor.DetailsInteractor;
import eu.oncreate.stockshark.presenter.DetailsPresenter;
import eu.oncreate.stockshark.view.DetailsView;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.LinkedList;
import javax.inject.Inject;
import timber.log.Timber;

public final class DetailsPresenterImpl extends BasePresenterImpl<DetailsView>
    implements DetailsPresenter {
  /**
   * The interactor
   */
  @NonNull
  private final DetailsInteractor mInteractor;

  private Stock previousStock;
  private Stock currentStock;
  //double TAXES = 0.1d;
  private User user;
  private String stockName;
  private Observable<LinkedList<Stock>> observable;
  private DisposableObserver<LinkedList<Stock>> disposableObserver;
  private int counter = 0;

  // The view is available using the mView variable

  @Inject
  public DetailsPresenterImpl(@NonNull DetailsInteractor interactor) {
    mInteractor = interactor;
  }

  @Override
  public void onStart(boolean viewCreated) {
    super.onStart(viewCreated);

    // Your code here. Your view is available using mView and will not be null until next onStop()
    if (stockName == null) {
      if (mView != null) {
        stockName = mView.getStockName();
      }
    }

    getInitialStockData();

    getUser();
  }

  private void getUser() {
    Single<User> userSingle = mInteractor.getUserData();
    userSingle.observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe(new DisposableSingleObserver<User>() {
          @Override
          public void onSuccess(@io.reactivex.annotations.NonNull User user1) {
            user = user1;
            provideInitDataToUi();
          }

          @Override
          public void onError(@io.reactivex.annotations.NonNull Throwable e) {
            Timber.d("onError %s", e.toString());
          }
        });
  }

  private void getInitialStockData() {
    Single<LinkedList<Stock>> stockData = mInteractor.getStockData(stockName);
    stockData.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new DisposableSingleObserver<LinkedList<Stock>>() {
          @Override
          public void onSuccess(
              @io.reactivex.annotations.NonNull LinkedList<Stock> stockLinkedList) {
            if (mView != null) {
              previousStock = stockLinkedList.get(stockLinkedList.size() - 2);
              currentStock = stockLinkedList.getLast();
              counter = stockLinkedList.size() - 1;
              provideInitDataToUi();
              mView.getInitialStockData(stockLinkedList);
            }
          }

          @Override
          public void onError(@io.reactivex.annotations.NonNull Throwable e) {
            if (mView != null) {
              mView.onError(e);
            }
          }
        });
  }

  @Override
  public void onStop() {
    // Your code here, mView will be null after this method until next onStart()

    super.onStop();
  }

  @Override
  public void onPresenterDestroyed() {
        /*
         * Your code here. After this method, your presenter (and view) will be completely destroyed
         * so make sure to cancel any HTTP call or database connection
         */
    counter = 0;
    stockName = null;
    if (!disposableObserver.isDisposed()) disposableObserver.dispose();
    super.onPresenterDestroyed();
  }

  @Override
  public void startSimulation() {
    Timber.d("stopSimulation");
    observable = mInteractor.startSimulation(stockName)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io());
    disposableObserver = new DisposableObserver<LinkedList<Stock>>() {
      @Override
      public void onNext(@io.reactivex.annotations.NonNull LinkedList<Stock> stockLinkedList) {
        if (mView != null) {
          previousStock.setName(currentStock.getName());
          previousStock.setPrice(currentStock.getPrice());
          previousStock.setBasePrice(currentStock.getBasePrice());
          currentStock = stockLinkedList.getLast();
          //// TODO: 8/15/17 changeSellPrice
          provideDataToUi(user, currentStock, previousStock);
        }
      }

      @Override
      public void onError(@io.reactivex.annotations.NonNull Throwable e) {
        Timber.d("Error %s", e.toString());
      }

      @Override
      public void onComplete() {
        Timber.d("disposableObserver onComplete");
      }
    };
    observable.subscribe(disposableObserver);
  }

  @Override
  public void stopSimulation() {
    if (disposableObserver != null) {
      if (!disposableObserver.isDisposed()) {
        disposableObserver.dispose();
        Timber.d("stopSimulation");
      }
    }
  }

  @Override
  public void buyStocks(String quantity) {
    int quantityInt = Integer.parseInt(quantity);
    Completable completable = mInteractor.buyStocks(currentStock, quantityInt);
    completable.andThen(mInteractor.getUserData())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe(new DisposableSingleObserver<User>() {
          @Override
          public void onSuccess(@io.reactivex.annotations.NonNull User user) {
            //provideDataToUi(user, currentStock, previousStock);
            updateUserUi(user, false);
          }

          @Override
          public void onError(@io.reactivex.annotations.NonNull Throwable e) {
            Timber.d("onError %s", e.toString());
          }
        });
  }

  @Override
  public void sellStocks(String quantity) {
    int quantityInt = Integer.parseInt(quantity);
    Completable completable = mInteractor.sellStocks(currentStock, quantityInt);
    completable.andThen(mInteractor.getUserData())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe(new DisposableSingleObserver<User>() {
          @Override
          public void onSuccess(@io.reactivex.annotations.NonNull User user) {
            //provideDataToUi(user, currentStock, previousStock);
            updateUserUi(user, true);
          }

          @Override
          public void onError(@io.reactivex.annotations.NonNull Throwable e) {
            Timber.d("onError %s", e.toString());
          }
        });
  }

  private synchronized void provideInitDataToUi() {
    if (mView != null && user != null && currentStock != null) {
      provideDataToUi(user, currentStock, previousStock);
    }
  }

  /**
   * @param sell if selling is true; if buying false
   */
  private void updateUserUi(User user, boolean sell) {
    this.user = user;

    int sellCountInt = 0;
    double totalWorth = 0;
    ArrayList<OwnedStocks> stocksReadyToSell = new ArrayList<>();
    if (user.getOwnedStocksList() != null) {
      for (OwnedStocks ownedStocks : user.getOwnedStocksList()) {
        if (ownedStocks.getName().equals(currentStock.getName())) {
          stocksReadyToSell.add(ownedStocks);
          sellCountInt = sellCountInt + ownedStocks.getStocksCount();
          totalWorth = totalWorth + ownedStocks.getPrice() * ownedStocks.getStocksCount();
        }
      }
    }
    int buyCountInt = (int) (user.getMoney() / currentStock.getPrice());
    String money = String.format("%.0f", user.getMoney());
    double profitDouble =
        (1 - user.getTaxesValues()) * currentStock.getPrice() - (totalWorth / sellCountInt);
    String profit = String.format("%.3f", profitDouble);

    if (sell) {
      if (mView != null) {
        mView.sellSuccessful(money, profit, sellCountInt, buyCountInt);
        mView.getAchievements(mInteractor.getNewSellAchievements());
      }
    } else {
      if (mView != null) {
        mView.buySuccessful(money, profit, sellCountInt, buyCountInt);
        mView.getAchievements(mInteractor.getNewBuyAchievements());
      }
    }
  }

  private void provideDataToUi(User user, Stock currentStock, Stock previousStock) {
    if (mView != null) {
      //// TODO: 8/15/17 taxes
      String sellPrice = String.format("%.3f", currentStock.getPrice());
      String price = String.format("%.3f", currentStock.getPrice());
      int sellCountInt = 0;
      double totalWorth = 0;
      ArrayList<OwnedStocks> stocksReadyToSell = new ArrayList<>();
      if (user.getOwnedStocksList() != null) {
        for (OwnedStocks ownedStocks : user.getOwnedStocksList()) {
          if (ownedStocks.getName().equals(currentStock.getName())) {
            stocksReadyToSell.add(ownedStocks);
            sellCountInt = sellCountInt + ownedStocks.getStocksCount();
            totalWorth = totalWorth + ownedStocks.getPrice() * ownedStocks.getStocksCount();
          }
        }
      }
      String sellCount = String.valueOf(sellCountInt);
      int buyCountInt = (int) (user.getMoney() / currentStock.getPrice());
      String buyCount = String.valueOf(buyCountInt);
      double profitDouble =
          (1 - user.getTaxesValues()) * currentStock.getPrice() - (totalWorth / sellCountInt);
      String profit = String.format("%.3f", profitDouble);
      String money = String.format("%.0f", user.getMoney());
      double priceChangeDouble = (currentStock.getPrice() / previousStock.getPrice() - 1) * 100;
      String priceChange = String.format("%.2f", priceChangeDouble);
      mView.provideData(currentStock.getName(), price, priceChangeDouble, money, profit,
          sellCountInt, user.getTaxesValues(), buyCountInt, counter);
      counter += 1;
      mView.getAchievements(mInteractor.getNewUpdateAchievements());
      //mView.provideData(price, sellPrice, buyCount, sellCount, totalWorth, money);
    }
  }
}