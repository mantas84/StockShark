package eu.oncreate.stockshark.presenter;

import eu.oncreate.stockshark.view.AssetsView;

public interface AssetsPresenter extends BasePresenter<AssetsView> {

  void reloadData();

  void tryToBuy(String name, long epochSeconds);

  void MoneyGained(String name, long epochSecond);
}