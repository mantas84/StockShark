package eu.oncreate.stockshark;

import android.app.Application;
import android.content.Context;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.google.android.gms.ads.MobileAds;
import com.jakewharton.threetenabp.AndroidThreeTen;
import com.squareup.leakcanary.LeakCanary;
import eu.oncreate.stockshark.data.database.DatabasePaper;
import eu.oncreate.stockshark.injection.AppComponent;
import eu.oncreate.stockshark.injection.AppModule;
import eu.oncreate.stockshark.injection.DaggerAppComponent;
import eu.oncreate.stockshark.injection.UserComponent;
import eu.oncreate.stockshark.injection.UserModule;
import io.fabric.sdk.android.Fabric;
import io.paperdb.Paper;
import timber.log.Timber;

public final class App extends Application {
  private AppComponent mAppComponent;
  private UserComponent userComponent;

  public static App get(Context context) {
    return (App) context.getApplicationContext();
  }

  @Override
  public void onCreate() {
    super.onCreate();

    if (BuildConfig.DEBUG) {
      if (LeakCanary.isInAnalyzerProcess(this)) {
        return;
      }
      LeakCanary.install(this);
      StrictMode.setThreadPolicy(
          new StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build());
      StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().penaltyLog().build());

      Timber.plant(new Timber.DebugTree() {
        @Override
        protected String createStackElementTag(StackTraceElement element) {
          return super.createStackElementTag(element) + ":" + element.getLineNumber();
        }
      });
    } else {
      Fabric.with(this, new Crashlytics());
      Fabric.with(this, new Answers());
    }

    Paper.init(this);
    AndroidThreeTen.init(this);
    MobileAds.initialize(this, BuildConfig.adMobId);

    mAppComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
  }

  public UserComponent createUserComponent(DatabasePaper databasePaper) {
    userComponent = mAppComponent.plus(new UserModule(databasePaper));
    return userComponent;
  }

  public void releaseUserComponent() {
    userComponent = null;
  }

  @NonNull
  public UserComponent getUserComponent() {
    return userComponent;
  }

  @NonNull
  public AppComponent getAppComponent() {
    return mAppComponent;
  }
}