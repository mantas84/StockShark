package eu.oncreate.stockshark.injection;

import dagger.Subcomponent;
import eu.oncreate.stockshark.view.impl.MainActivity;

@ActivityScope
@Subcomponent(
    //dependencies = AppComponent.class,
    modules = MainViewModule.class)
public interface MainViewComponent {
  //void inject(MainActivity activity);
  MainActivity inject(MainActivity MainActivity);
}