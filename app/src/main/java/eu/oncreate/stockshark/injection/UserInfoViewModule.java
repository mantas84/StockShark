package eu.oncreate.stockshark.injection;

import android.support.annotation.NonNull;
import dagger.Module;
import dagger.Provides;
import eu.oncreate.stockshark.data.DataSource;
import eu.oncreate.stockshark.interactor.UserInfoInteractor;
import eu.oncreate.stockshark.interactor.impl.UserInfoInteractorImpl;
import eu.oncreate.stockshark.presenter.UserInfoPresenter;
import eu.oncreate.stockshark.presenter.impl.UserInfoPresenterImpl;
import eu.oncreate.stockshark.presenter.loader.PresenterFactory;

@Module
public final class UserInfoViewModule {
  @Provides
  public UserInfoInteractor provideInteractor(DataSource dataSource) {
    return new UserInfoInteractorImpl(dataSource);
  }

  @Provides
  public PresenterFactory<UserInfoPresenter> providePresenterFactory(
      @NonNull final UserInfoInteractor interactor) {
    return new PresenterFactory<UserInfoPresenter>() {
      @NonNull
      @Override
      public UserInfoPresenter create() {
        return new UserInfoPresenterImpl(interactor);
      }
    };
  }
}
