package eu.oncreate.stockshark.injection;

import android.support.annotation.NonNull;
import dagger.Module;
import dagger.Provides;
import eu.oncreate.stockshark.data.DataSource;
import eu.oncreate.stockshark.interactor.MainInteractor;
import eu.oncreate.stockshark.interactor.impl.MainInteractorImpl;
import eu.oncreate.stockshark.presenter.MainPresenter;
import eu.oncreate.stockshark.presenter.impl.MainPresenterImpl;
import eu.oncreate.stockshark.presenter.loader.PresenterFactory;

@Module
public final class MainViewModule {
  @Provides
  public MainInteractor provideInteractor(DataSource dataSource) {
    return new MainInteractorImpl(dataSource);
  }

  @Provides
  public PresenterFactory<MainPresenter> providePresenterFactory(
      @NonNull final MainInteractor interactor) {
    return new PresenterFactory<MainPresenter>() {
      @NonNull
      @Override
      public MainPresenter create() {
        return new MainPresenterImpl(interactor);
      }
    };
  }
}
