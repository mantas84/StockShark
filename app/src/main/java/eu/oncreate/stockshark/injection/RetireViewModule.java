package eu.oncreate.stockshark.injection;

import android.support.annotation.NonNull;
import dagger.Module;
import dagger.Provides;
import eu.oncreate.stockshark.data.DataSource;
import eu.oncreate.stockshark.interactor.RetireInteractor;
import eu.oncreate.stockshark.interactor.impl.RetireInteractorImpl;
import eu.oncreate.stockshark.presenter.RetirePresenter;
import eu.oncreate.stockshark.presenter.impl.RetirePresenterImpl;
import eu.oncreate.stockshark.presenter.loader.PresenterFactory;

@Module
public final class RetireViewModule {
  @Provides
  public RetireInteractor provideInteractor(DataSource dataSource) {
    return new RetireInteractorImpl(dataSource);
  }

  @Provides
  public PresenterFactory<RetirePresenter> providePresenterFactory(
      @NonNull final RetireInteractor interactor) {
    return new PresenterFactory<RetirePresenter>() {
      @NonNull
      @Override
      public RetirePresenter create() {
        return new RetirePresenterImpl(interactor);
      }
    };
  }
}
