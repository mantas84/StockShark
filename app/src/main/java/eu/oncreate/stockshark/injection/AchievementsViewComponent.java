package eu.oncreate.stockshark.injection;

import dagger.Subcomponent;
import eu.oncreate.stockshark.view.impl.AchievementsActivity;

@ActivityScope
@Subcomponent(
    //dependencies = AppComponent.class,
    modules = AchievementsViewModule.class)
public interface AchievementsViewComponent {
  //void inject(AchievementsActivity activity);
  AchievementsActivity inject(AchievementsActivity achievementsActivity);
}