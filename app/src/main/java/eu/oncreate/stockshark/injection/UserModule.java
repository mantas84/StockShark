package eu.oncreate.stockshark.injection;

import dagger.Module;
import dagger.Provides;
import eu.oncreate.stockshark.data.DataSource;
import eu.oncreate.stockshark.data.StockDao;
import eu.oncreate.stockshark.data.UserDao;
import eu.oncreate.stockshark.data.constants.StockConstants;
import eu.oncreate.stockshark.data.database.DatabasePaper;
import eu.oncreate.stockshark.data.models.User;

@Module
public class UserModule {

  private DatabasePaper databasePaper;

  public UserModule(DatabasePaper databasePaper) {
    this.databasePaper = databasePaper;
  }

  @Provides
  @UserScope
  DatabasePaper provideDatabasePaper() {
    return databasePaper;
  }

  @Provides
  @UserScope
  DataSource provideDataSource(StockDao stockDao, UserDao userDao, DatabasePaper databasePaper) {
    return new DataSource(stockDao, userDao, databasePaper);
  }

  @Provides
  @UserScope
  UserDao provideUserDao(DatabasePaper databasePaper) {
    User user = databasePaper.getUser();
    return new UserDao(user);
  }

  @Provides
  @UserScope
  StockDao provideStockDao(DatabasePaper databasePaper) {
    return new StockDao(databasePaper.getStocks(StockConstants.STOCK_ORANGE),
        databasePaper.getStocks(StockConstants.STOCK_BOOGLE),
        databasePaper.getStocks(StockConstants.STOCK_FACEMAG),
        databasePaper.getStocks(StockConstants.STOCK_MACROSOFT),
        databasePaper.getStocks(StockConstants.STOCK_ABN),

        databasePaper.getStocks(StockConstants.STOCK_SUNNY),
        databasePaper.getStocks(StockConstants.STOCK_SUNSONG),
        databasePaper.getStocks(StockConstants.STOCK_BANASONIK),
        databasePaper.getStocks(StockConstants.STOCK_LUAWEI),
        databasePaper.getStocks(StockConstants.STOCK_LOSHIPA),

        databasePaper.getStocks(StockConstants.STOCK_POYOPA),
        databasePaper.getStocks(StockConstants.STOCK_ZOLKSZAGEN),
        databasePaper.getStocks(StockConstants.STOCK_MAIN_MOTORING),
        databasePaper.getStocks(StockConstants.STOCK_CIA),
        databasePaper.getStocks(StockConstants.STOCK_CORD),

        databasePaper.getStocks(StockConstants.STOCK_DAM_MART),
        databasePaper.getStocks(StockConstants.STOCK_FLOSTCO),
        databasePaper.getStocks(StockConstants.STOCK_PROGER),
        databasePaper.getStocks(StockConstants.STOCK_HOME_DESPOT),
        databasePaper.getStocks(StockConstants.STOCK_BEST_ACQUISITION),

        databasePaper.getStocks(StockConstants.STOCK_JASON_FOODS),
        databasePaper.getStocks(StockConstants.STOCK_CLEPSICO),
        databasePaper.getStocks(StockConstants.STOCK_TESTLE),
        databasePaper.getStocks(StockConstants.STOCK_POPA_COLA),
        databasePaper.getStocks(StockConstants.STOCK_VRAFT_REINZ_CO),

        databasePaper.getStocks(StockConstants.STOCK_AP_P),
        databasePaper.getStocks(StockConstants.STOCK_HORIZON_COMMUNICATIONS),
        databasePaper.getStocks(StockConstants.STOCK_ZOID),
        databasePaper.getStocks(StockConstants.STOCK_ZODAFONE),
        databasePaper.getStocks(StockConstants.STOCK_BLORANGE)

    );
  }
}
