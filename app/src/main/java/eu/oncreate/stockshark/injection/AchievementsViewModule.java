package eu.oncreate.stockshark.injection;

import android.support.annotation.NonNull;
import dagger.Module;
import dagger.Provides;
import eu.oncreate.stockshark.data.DataSource;
import eu.oncreate.stockshark.interactor.AchievementsInteractor;
import eu.oncreate.stockshark.interactor.impl.AchievementsInteractorImpl;
import eu.oncreate.stockshark.presenter.AchievementsPresenter;
import eu.oncreate.stockshark.presenter.impl.AchievementsPresenterImpl;
import eu.oncreate.stockshark.presenter.loader.PresenterFactory;

@Module
public final class AchievementsViewModule {
  @Provides
  public AchievementsInteractor provideInteractor(DataSource dataSource) {
    return new AchievementsInteractorImpl(dataSource);
  }

  @Provides
  public PresenterFactory<AchievementsPresenter> providePresenterFactory(
      @NonNull final AchievementsInteractor interactor) {
    return new PresenterFactory<AchievementsPresenter>() {
      @NonNull
      @Override
      public AchievementsPresenter create() {
        return new AchievementsPresenterImpl(interactor);
      }
    };
  }
}
