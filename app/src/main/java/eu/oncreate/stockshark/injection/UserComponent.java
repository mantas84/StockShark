package eu.oncreate.stockshark.injection;

import dagger.Subcomponent;

@UserScope
@Subcomponent(modules = {
    UserModule.class
})
public interface UserComponent {
  MainViewComponent plus(MainViewModule mainViewModule);

  StockListViewComponent plus(StockListViewModule stockListViewModule);

  DetailsViewComponent plus(DetailsViewModule detailsViewModule);

  UserInfoViewComponent plus(UserInfoViewModule userInfoViewModule);

  AchievementsViewComponent plus(AchievementsViewModule achievementsViewModule);

  AssetsViewComponent plus(AssetsViewModule assetsViewModule);

  RetireViewComponent plus(RetireViewModule RetireViewModule);
}
