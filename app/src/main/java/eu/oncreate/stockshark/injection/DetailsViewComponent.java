package eu.oncreate.stockshark.injection;

import dagger.Subcomponent;
import eu.oncreate.stockshark.view.impl.DetailsActivity;

@ActivityScope
@Subcomponent(
    //dependencies = AppComponent.class,
    modules = DetailsViewModule.class)
public interface DetailsViewComponent {
  //void inject(DetailsActivity activity);
  DetailsActivity inject(DetailsActivity detailsActivity);
}