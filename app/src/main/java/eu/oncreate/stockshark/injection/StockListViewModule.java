package eu.oncreate.stockshark.injection;

import android.support.annotation.NonNull;
import dagger.Module;
import dagger.Provides;
import eu.oncreate.stockshark.data.DataSource;
import eu.oncreate.stockshark.interactor.StockListInteractor;
import eu.oncreate.stockshark.interactor.impl.StockListInteractorImpl;
import eu.oncreate.stockshark.presenter.StockListPresenter;
import eu.oncreate.stockshark.presenter.impl.StockListPresenterImpl;
import eu.oncreate.stockshark.presenter.loader.PresenterFactory;

@Module
public final class StockListViewModule {
  @Provides
  public StockListInteractor provideInteractor(DataSource dataSource) {
    return new StockListInteractorImpl(dataSource);
  }

  @Provides
  public PresenterFactory<StockListPresenter> providePresenterFactory(
      @NonNull final StockListInteractor interactor) {
    return new PresenterFactory<StockListPresenter>() {
      @NonNull
      @Override
      public StockListPresenter create() {
        return new StockListPresenterImpl(interactor);
      }
    };
  }
}
