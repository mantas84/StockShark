package eu.oncreate.stockshark.injection;

import android.support.annotation.NonNull;
import dagger.Module;
import dagger.Provides;
import eu.oncreate.stockshark.data.DataSource;
import eu.oncreate.stockshark.interactor.DetailsInteractor;
import eu.oncreate.stockshark.interactor.impl.DetailsInteractorImpl;
import eu.oncreate.stockshark.presenter.DetailsPresenter;
import eu.oncreate.stockshark.presenter.impl.DetailsPresenterImpl;
import eu.oncreate.stockshark.presenter.loader.PresenterFactory;

@Module
public final class DetailsViewModule {
  @Provides
  public DetailsInteractor provideInteractor(DataSource dataSource) {
    return new DetailsInteractorImpl(dataSource);
  }

  @Provides
  public PresenterFactory<DetailsPresenter> providePresenterFactory(
      @NonNull final DetailsInteractor interactor) {
    return new PresenterFactory<DetailsPresenter>() {
      @NonNull
      @Override
      public DetailsPresenter create() {
        return new DetailsPresenterImpl(interactor);
      }
    };
  }
}
