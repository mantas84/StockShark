package eu.oncreate.stockshark.injection;

import dagger.Subcomponent;
import eu.oncreate.stockshark.view.impl.RetireActivity;

@ActivityScope
@Subcomponent(modules = RetireViewModule.class)
public interface RetireViewComponent {
  //void inject(RetireActivity activity);
  RetireActivity inject(RetireActivity MainActivity);
}