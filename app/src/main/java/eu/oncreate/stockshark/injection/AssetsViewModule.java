package eu.oncreate.stockshark.injection;

import android.support.annotation.NonNull;
import dagger.Module;
import dagger.Provides;
import eu.oncreate.stockshark.data.DataSource;
import eu.oncreate.stockshark.interactor.AssetsInteractor;
import eu.oncreate.stockshark.interactor.impl.AssetsInteractorImpl;
import eu.oncreate.stockshark.presenter.AssetsPresenter;
import eu.oncreate.stockshark.presenter.impl.AssetsPresenterImpl;
import eu.oncreate.stockshark.presenter.loader.PresenterFactory;

@Module
public final class AssetsViewModule {
  @Provides
  public AssetsInteractor provideInteractor(DataSource dataSource) {
    return new AssetsInteractorImpl(dataSource);
  }

  @Provides
  public PresenterFactory<AssetsPresenter> providePresenterFactory(
      @NonNull final AssetsInteractor interactor) {
    return new PresenterFactory<AssetsPresenter>() {
      @NonNull
      @Override
      public AssetsPresenter create() {
        return new AssetsPresenterImpl(interactor);
      }
    };
  }
}
