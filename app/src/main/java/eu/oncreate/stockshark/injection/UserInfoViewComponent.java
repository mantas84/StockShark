package eu.oncreate.stockshark.injection;

import dagger.Subcomponent;
import eu.oncreate.stockshark.view.impl.UserInfoActivity;

@ActivityScope
@Subcomponent(modules = UserInfoViewModule.class)
public interface UserInfoViewComponent {
  UserInfoActivity inject(UserInfoActivity activity);
}