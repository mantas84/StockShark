package eu.oncreate.stockshark.injection;

import dagger.Subcomponent;
import eu.oncreate.stockshark.view.impl.AssetsActivity;

@ActivityScope
@Subcomponent(modules = AssetsViewModule.class)
public interface AssetsViewComponent {
  //void inject(AssetsActivity activity);
  AssetsActivity inject(AssetsActivity activity);
}