package eu.oncreate.stockshark.injection;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import dagger.Module;
import dagger.Provides;
import eu.oncreate.stockshark.App;

import static android.content.Context.MODE_PRIVATE;

@Module
public final class AppModule {
  @NonNull
  private final App mApp;

  public AppModule(@NonNull App app) {
    mApp = app;
  }

  @Provides
  public Context provideAppContext() {
    return mApp;
  }

  @Provides
  public App provideApp() {
    return mApp;
  }

  @Provides
  public SharedPreferences provideSharedPreferences() {
    SharedPreferences myPrefs = mApp.getSharedPreferences("myPrefs", MODE_PRIVATE);
    return myPrefs;
  }
}
