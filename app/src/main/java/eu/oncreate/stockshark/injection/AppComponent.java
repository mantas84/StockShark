package eu.oncreate.stockshark.injection;

import android.content.Context;
import dagger.Component;
import eu.oncreate.stockshark.App;
import javax.inject.Singleton;

@Singleton
@Component(modules = { AppModule.class })
public interface AppComponent {
  Context getAppContext();

  App getApp();

  UserComponent plus(UserModule userModule);
}