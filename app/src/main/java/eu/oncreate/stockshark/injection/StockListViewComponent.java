package eu.oncreate.stockshark.injection;

import dagger.Subcomponent;
import eu.oncreate.stockshark.view.impl.StockListFragment;

@FragmentScope
@Subcomponent(
    //dependencies = AppComponent.class,
    modules = StockListViewModule.class)
public interface StockListViewComponent {
  //void inject(StockListFragment fragment);
  StockListFragment inject(StockListFragment fragment);
}