package eu.oncreate.stockshark.view.impl;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.view.View;
import butterknife.BindView;
import butterknife.ButterKnife;
import eu.oncreate.stockshark.App;
import eu.oncreate.stockshark.R;
import eu.oncreate.stockshark.adapter.AssetViewPagerAdapter;
import eu.oncreate.stockshark.data.models.Asset;
import eu.oncreate.stockshark.helpers.StringToResStringHelper;
import eu.oncreate.stockshark.injection.AppComponent;
import eu.oncreate.stockshark.injection.AssetsViewModule;
import eu.oncreate.stockshark.presenter.AssetsPresenter;
import eu.oncreate.stockshark.presenter.loader.PresenterFactory;
import eu.oncreate.stockshark.view.AssetsView;
import eu.oncreate.stockshark.withoutMvp.AssetItemFragment;
import java.util.List;
import javax.inject.Inject;
import org.threeten.bp.Instant;
import timber.log.Timber;

public final class AssetsActivity extends BaseActivity<AssetsPresenter, AssetsView>
    implements AssetsView, AssetItemFragment.OnFragmentInteractionListener {

  @Inject
  PresenterFactory<AssetsPresenter> mPresenterFactory;

  @BindView(R.id.rootView_assets)
  View rootView;
  @BindView(R.id.viewPager_assets)
  ViewPager viewPager;
  private AssetViewPagerAdapter assetViewPagerAdapter;
  // Your presenter is available using the mPresenter variable

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_assets);

    ButterKnife.bind(this);
    // Your code here
    // Do not call mPresenter from here, it will be null! Wait for onStart or onPostCreate.
  }

  @Override
  protected void setupComponent(@NonNull AppComponent parentComponent) {
    ((App) getApplication()).getUserComponent().plus(new AssetsViewModule()).inject(this);
  }

  @NonNull
  @Override
  protected PresenterFactory<AssetsPresenter> getPresenterFactory() {
    return mPresenterFactory;
  }

  @Override
  public void onFragmentBuyClicked(String name) {
    //// TODO: 9/23/17

    //Toast.makeText(this, "Buy should happen", Toast.LENGTH_SHORT).show();
    Timber.d("onFragmentBuyClicked called");
    mPresenter.tryToBuy(name, Instant.now().getEpochSecond());
  }

  @Override
  public void onFragmentMoneyGainFromAsset(String name) {
    mPresenter.MoneyGained(name, Instant.now().getEpochSecond());
  }

  @Override
  public void gotData(List<Asset> assets) {
    assetViewPagerAdapter = new AssetViewPagerAdapter(getSupportFragmentManager(), assets);
    viewPager.setAdapter(assetViewPagerAdapter);
  }

  @Override
  public void onGetDataError(String error) {
    Snackbar.make(rootView, R.string.something_went_wrong, Snackbar.LENGTH_LONG)
        .setAction(R.string.retry, new View.OnClickListener() {
          public void onClick(View v) {
            mPresenter.reloadData();
          }
        })
        .show();
  }

  @Override
  public void buySuccessful(List<Asset> assetList) {
    assetViewPagerAdapter.reloadData(assetList);
  }

  @Override
  public void onGetBuyError(String s) {
    Snackbar snackbar = Snackbar.make(rootView, "Something went wrong", Snackbar.LENGTH_SHORT);
    snackbar.show();
  }

  @Override
  public void cantBuyIt(String s) {
    String reason = StringToResStringHelper.getAssetCantBuyReason(this, s);
    Snackbar snackbar = Snackbar.make(rootView, reason, Snackbar.LENGTH_SHORT);
    snackbar.show();
  }
}
