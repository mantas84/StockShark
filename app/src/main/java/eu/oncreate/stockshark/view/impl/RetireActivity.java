package eu.oncreate.stockshark.view.impl;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import eu.oncreate.stockshark.App;
import eu.oncreate.stockshark.R;
import eu.oncreate.stockshark.injection.AppComponent;
import eu.oncreate.stockshark.injection.RetireViewModule;
import eu.oncreate.stockshark.presenter.RetirePresenter;
import eu.oncreate.stockshark.presenter.loader.PresenterFactory;
import eu.oncreate.stockshark.view.RetireView;
import eu.oncreate.stockshark.withoutMvp.SplashActivity;
import javax.inject.Inject;

public final class RetireActivity extends BaseActivity<RetirePresenter, RetireView>
    implements RetireView {
  @Inject
  PresenterFactory<RetirePresenter> mPresenterFactory;

  @BindView(R.id.txt_score)
  TextView textViewScore;
  @BindView(R.id.btn_again)
  Button buttonAgain;

  // Your presenter is available using the mPresenter variable

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_retire);

    ButterKnife.bind(this);

    buttonAgain.setEnabled(false);

    String score = String.format("%.0f", getIntent().getDoubleExtra("Score", 0));
    textViewScore.setText(score);

    // Your code here
    // Do not call mPresenter from here, it will be null! Wait for onStart or onPostCreate.
  }

  @Override
  protected void onResume() {
    super.onResume();
    buttonAgain.setEnabled(mPresenter.checkForRetired());
  }

  @Override
  protected void setupComponent(@NonNull AppComponent parentComponent) {
    //DaggerRetireViewComponent.builder()
    //    .appComponent(parentComponent)
    //    .retireViewModule(new RetireViewModule())
    //    .build()
    //    .inject(this);
    ((App) getApplication()).getUserComponent().plus(new RetireViewModule()).inject(this);
  }

  @NonNull
  @Override
  protected PresenterFactory<RetirePresenter> getPresenterFactory() {
    return mPresenterFactory;
  }

  @OnClick(R.id.btn_again)
  public void tryAgain() {
    Intent intent = new Intent(this, SplashActivity.class);
    startActivity(intent);
    App.get(this).releaseUserComponent();
    finish();
  }

  @Override
  public void retired() {
    buttonAgain.setEnabled(true);
  }
}
