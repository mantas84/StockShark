package eu.oncreate.stockshark.view.impl;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.github.mikephil.charting.jobs.MoveViewJob;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import eu.oncreate.stockshark.App;
import eu.oncreate.stockshark.BuildConfig;
import eu.oncreate.stockshark.R;
import eu.oncreate.stockshark.adapter.StockViewPagerAdapter;
import eu.oncreate.stockshark.data.models.Achievements;
import eu.oncreate.stockshark.helpers.StringToResStringHelper;
import eu.oncreate.stockshark.injection.AppComponent;
import eu.oncreate.stockshark.injection.MainViewModule;
import eu.oncreate.stockshark.presenter.MainPresenter;
import eu.oncreate.stockshark.presenter.loader.PresenterFactory;
import eu.oncreate.stockshark.view.MainView;
import java.util.List;
import javax.inject.Inject;
import org.threeten.bp.Instant;
import timber.log.Timber;

public final class MainActivity extends BaseActivity<MainPresenter, MainView>
    implements MainView, StockListFragment.SimulationStartAndGo, RewardedVideoAdListener {
  private static final String VIEWPAGER_STATE = "viewpager_state";
  @Inject
  PresenterFactory<MainPresenter> mPresenterFactory;

  @Inject
  SharedPreferences myPrefs;
  @BindView(R.id.pager)
  ViewPager viewPager;

  // Your presenter is available using the mPresenter variable
  @BindView(R.id.btn_userInfo)
  ImageButton userInfo;
  @BindView(R.id.btn_speedUpReward)
  ImageButton buttonSpeedReward;
  @BindView(R.id.adView)
  AdView tempAdView;
  @BindView(R.id.view_main_root)
  ViewGroup rootView;
  @BindView(R.id.toolbar)
  Toolbar toolbar;
  private RewardedVideoAd rewardVideoAd;
  private AdView adView;

  private StockViewPagerAdapter stockViewPagerAdapter;
  private int recyclerPosition;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.view_main);

    ButterKnife.bind(this);

    setSupportActionBar(toolbar);

    rewardVideoAd = MobileAds.getRewardedVideoAdInstance(this);
    rewardVideoAd.loadAd(BuildConfig.unitIdRewardVideo, new AdRequest.Builder().build());

    if (stockViewPagerAdapter == null) {
      stockViewPagerAdapter = new StockViewPagerAdapter(getSupportFragmentManager());
      viewPager.setAdapter(stockViewPagerAdapter);

      userInfo.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          startUserInfoActivity();
        }
      });

      buttonSpeedReward.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          if (rewardVideoAd.isLoaded()) {
            rewardVideoAd.show();
          }
        }
      });
    }

    setUpBannerAd();

    rewardVideoAd.setRewardedVideoAdListener(this);

    // Your code here
    // Do not call mPresenter from here, it will be null! Wait for onStart or onPostCreate.
  }

  private void startUserInfoActivity() {
    Intent intent = new Intent(MainActivity.this, UserInfoActivity.class);
    View view = userInfo;
    ActivityOptionsCompat options = ActivityOptionsCompat.
        makeSceneTransitionAnimation(this, view, "userImageTransition");
    startActivity(intent, options.toBundle());
  }

  private void setUpBannerAd() {
    adView = new AdView(this);
    AdRequest adRequest = new AdRequest.Builder().build();
    adView.setAdSize(AdSize.SMART_BANNER);
    adView.setAdUnitId(BuildConfig.unitIdBanner);
    adView.setLayoutParams(this.tempAdView.getLayoutParams());
    adView.setVisibility(View.VISIBLE);
    rootView.addView(adView);
    adView.loadAd(adRequest);
  }

  @Override
  protected void setupComponent(@NonNull AppComponent parentComponent) {
    ((App) getApplication()).getUserComponent().plus(new MainViewModule()).inject(this);
  }

  @NonNull
  @Override
  protected PresenterFactory<MainPresenter> getPresenterFactory() {
    return mPresenterFactory;
  }

  @Override
  protected void onDestroy() {
    //// FIXME: 8/20/17 https://github.com/PhilJay/MPAndroidChart/issues/2238
    MoveViewJob.getInstance(null, 0f, 0f, null, null);
    //// FIXME: 9/28/17 leak, workaround -> using appContext instead of context
    //https://github.com/googleads/googleads-mobile-android-examples/issues/83
    rewardVideoAd.destroy(this);
    if (adView != null) adView.destroy();
    super.onDestroy();
  }

  @Override
  public void onResume() {
    super.onResume();
    Timber.d("onResume");
    Boolean exit = myPrefs.getBoolean("EXIT", false);
    if (exit) {
      SharedPreferences.Editor prefsEditor;
      prefsEditor = myPrefs.edit();
      prefsEditor.putBoolean("EXIT", false);
      prefsEditor.commit();
      mPresenter.getRetiringScore();
    } else {
      rewardVideoAd.resume(this);
      if (adView != null) adView.resume();
      boolean state = mPresenter.getRewardButtonState();
      buttonSpeedReward.setEnabled(state);
      if (state) {
        buttonSpeedReward.setAlpha(1.0f);
      } else {
        buttonSpeedReward.setAlpha(.5f);
      }
      mPresenter.startSimulation();
    }
  }

  @Override
  public void onStop() {
    super.onStop();
    Timber.d("onStop");
    //mPresenter.stopSimulation();
  }

  @Override
  protected void onPause() {
    Timber.d("onPause");
    mPresenter.stopSimulation();
    rewardVideoAd.pause(this);
    if (adView != null) adView.pause();
    super.onPause();
  }

  @Override
  public void startDetailsActivity(String stockName, View graph, int themeInt) {
    Intent intent = new Intent(this, DetailsActivity.class);
    intent.putExtra("stockName", stockName);
    intent.putExtra("theme", themeInt);

    recyclerPosition = themeInt;

    ActivityOptionsCompat options = ActivityOptionsCompat.
        makeSceneTransitionAnimation(this, graph, "stockGraphTransition");
    startActivity(intent, options.toBundle());
  }

  @Override
  public int getRecyclerPosition() {
    return recyclerPosition;
  }

  @Override
  public void onRewardedVideoAdLoaded() {

  }

  @Override
  public void onRewardedVideoAdOpened() {

  }

  @Override
  public void onRewardedVideoStarted() {

  }

  @Override
  public void onRewardedVideoAdClosed() {
    if (!rewardVideoAd.isLoaded()) {
      rewardVideoAd.loadAd(BuildConfig.unitIdRewardVideo, new AdRequest.Builder().build());
    }
  }

  @Override
  public void onRewarded(RewardItem rewardItem) {
    Timber.d("onRewarded called");
    long timeNow = Instant.now().getEpochSecond();
    buttonSpeedReward.setEnabled(false);
    buttonSpeedReward.setAlpha(.5f);
    mPresenter.rewardVideoWatched(timeNow);
  }

  @Override
  public void onRewardedVideoAdLeftApplication() {

  }

  @Override
  public void onRewardedVideoAdFailedToLoad(int i) {

  }

  @Override
  public void updateCountdown(Integer integer) {
    Timber.d("countdown %s", integer);
  }

  @Override
  public void rewardVideoEnable(boolean enableRewardVideo) {
    buttonSpeedReward.setEnabled(enableRewardVideo);
    if (enableRewardVideo) {
      buttonSpeedReward.setAlpha(1.0f);
    } else {
      buttonSpeedReward.setAlpha(.5f);
    }
  }

  @Override
  public void getAchievements(List<Achievements> newUpdateAchievements) {
    Timber.d("newUpdateAchievements==null %s", newUpdateAchievements == null);
    if (newUpdateAchievements != null) {
      Timber.d("newUpdateAchievements size %s", newUpdateAchievements.size());
    }
    if ((newUpdateAchievements != null) && (newUpdateAchievements.size() > 0)) {
      //showNewAchievements(newUpdateAchievements);
      for (Achievements achievement : newUpdateAchievements) {
        achievementToast(
            StringToResStringHelper.getAchievementString(this, achievement.getTypeName(),
                achievement.getLevel()), Toast.LENGTH_SHORT);
        //delay
      }
    }
  }

  @Override
  public void gotScore(Double aDouble) {
    //// TODO: 9/29/17
    Intent intent = new Intent(this, RetireActivity.class);
    intent.putExtra("Score", aDouble);
    startActivity(intent);
    finish();
  }

  void achievementToast(String achievementText, int length) {
    LayoutInflater inflater = getLayoutInflater();
    View layout = inflater.inflate(R.layout.toast_achievement,
        (ViewGroup) findViewById(R.id.custom_toast_container));

    TextView text = layout.findViewById(R.id.text);
    text.setText(achievementText);
    ImageView imageView = layout.findViewById(R.id.img_toastImage);
    //// TODO: 9/21/17 change image
    imageView.setImageDrawable(
        getResources().getDrawable(R.drawable.ic_action_achievement_black, getTheme()));

    Toast toast = new Toast(getApplicationContext());
    //toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
    toast.setDuration(length);
    toast.setView(layout);
    toast.show();
  }
}
