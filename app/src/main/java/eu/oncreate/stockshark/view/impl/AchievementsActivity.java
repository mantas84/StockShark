package eu.oncreate.stockshark.view.impl;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.BindView;
import butterknife.ButterKnife;
import eu.oncreate.stockshark.App;
import eu.oncreate.stockshark.R;
import eu.oncreate.stockshark.adapter.AchievementsAdapter;
import eu.oncreate.stockshark.data.models.Achievements;
import eu.oncreate.stockshark.injection.AchievementsViewModule;
import eu.oncreate.stockshark.injection.AppComponent;
import eu.oncreate.stockshark.presenter.AchievementsPresenter;
import eu.oncreate.stockshark.presenter.loader.PresenterFactory;
import eu.oncreate.stockshark.view.AchievementsView;
import java.util.List;
import javax.inject.Inject;

public final class AchievementsActivity
    extends BaseActivity<AchievementsPresenter, AchievementsView> implements AchievementsView {
  @Inject
  PresenterFactory<AchievementsPresenter> mPresenterFactory;

  // Your presenter is available using the mPresenter variable

  @BindView(R.id.recycleView_Achievements)
  RecyclerView recyclerView;
  @BindView(R.id.rootView_achievements)
  View rootView;

  private AchievementsAdapter adapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_achievements);

    ButterKnife.bind(this);

    //if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);

  }

  @Override
  protected void setupComponent(@NonNull AppComponent parentComponent) {
    //DaggerAchievementsViewComponent.builder()
    //    .appComponent(parentComponent)
    //    .achievementsViewModule(new AchievementsViewModule())
    //    .build()
    //    .inject(this);
    ((App) getApplication()).getUserComponent().plus(new AchievementsViewModule()).inject(this);
  }

  @NonNull
  @Override
  protected PresenterFactory<AchievementsPresenter> getPresenterFactory() {
    return mPresenterFactory;
  }

  @Override
  public void provideData(List<Achievements> achievements) {
    adapter = new AchievementsAdapter(this, achievements);

    recyclerView.setHasFixedSize(true);
    //land =2, portrait=1
    int orientation = getResources().getConfiguration().orientation;
    GridLayoutManager gridLayoutManager = new GridLayoutManager(this, orientation);
    recyclerView.setLayoutManager(gridLayoutManager);
    recyclerView.setAdapter(adapter);
  }

  @Override
  public void onError(String errorString) {
    Snackbar.make(rootView, errorString, Snackbar.LENGTH_SHORT);
    Snackbar.make(rootView, errorString, Snackbar.LENGTH_SHORT)
        .setAction("Retry", new View.OnClickListener() {
          public void onClick(View v) {
            mPresenter.getData();
          }
        })
        .show();
  }
}
