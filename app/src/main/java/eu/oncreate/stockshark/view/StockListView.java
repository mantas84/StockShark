package eu.oncreate.stockshark.view;

import android.support.annotation.UiThread;
import eu.oncreate.stockshark.data.models.Stock;
import java.util.LinkedList;
import java.util.List;

@UiThread
public interface StockListView {

  String[] getStocksNames();

  void showStocksFirstTime(List<LinkedList<Stock>> linkedLists);

  void getStocksUpdates(List<LinkedList<Stock>> linkedLists);
}