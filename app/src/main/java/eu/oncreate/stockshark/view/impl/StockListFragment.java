package eu.oncreate.stockshark.view.impl;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import eu.oncreate.stockshark.App;
import eu.oncreate.stockshark.R;
import eu.oncreate.stockshark.adapter.StockListRecycleViewAdapter;
import eu.oncreate.stockshark.data.models.Stock;
import eu.oncreate.stockshark.injection.AppComponent;
import eu.oncreate.stockshark.injection.StockListViewModule;
import eu.oncreate.stockshark.presenter.StockListPresenter;
import eu.oncreate.stockshark.presenter.loader.PresenterFactory;
import eu.oncreate.stockshark.view.StockListView;
import java.util.LinkedList;
import java.util.List;
import javax.inject.Inject;
import timber.log.Timber;

public final class StockListFragment extends BaseFragment<StockListPresenter, StockListView>
    implements StockListView, StockListRecycleViewAdapter.OnItemClickListener {
  public static final String STOCK_NAMES = "stockNames";
  private static final String SAVED_LAYOUT_MANAGER = "saved_layout_manager";
  Parcelable layoutManagerState;
  @Inject
  PresenterFactory<StockListPresenter> mPresenterFactory;
  @BindView(R.id.recycleView_Stocks)
  RecyclerView recyclerView;
  private View root;
  private StockListRecycleViewAdapter adapter;

  private Unbinder unbinder;

  private SimulationStartAndGo callback;

  // Your presenter is available using the mPresenter variable

  public StockListFragment() {
    // Required empty public constructor
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    root = inflater.inflate(R.layout.view_stocks_list, container, false);
    unbinder = ButterKnife.bind(this, root);
    recyclerView = root.findViewById(R.id.recycleView_Stocks);
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    // Your code here
    // Do not call mPresenter from here, it will be null! Wait for onStart
  }

  @Override
  protected void setupComponent(@NonNull AppComponent parentComponent) {
    ((App) getActivity().getApplication()).getUserComponent()
        .plus(new StockListViewModule())
        .inject(this);
  }

  @NonNull
  @Override
  protected PresenterFactory<StockListPresenter> getPresenterFactory() {
    return mPresenterFactory;
  }

  @Override
  public String[] getStocksNames() {
    return getArguments().getStringArray(STOCK_NAMES);
  }

  @Override
  public void showStocksFirstTime(List<LinkedList<Stock>> linkedLists) {
    Timber.d("adapter null %s", adapter == null);
    if (adapter == null) {
      adapter = new StockListRecycleViewAdapter(this, getActivity());
      //land =2, portrait=1
      int orientation = getResources().getConfiguration().orientation;
      GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), orientation);
      recyclerView.setLayoutManager(gridLayoutManager);
      recyclerView.setItemAnimator(new DefaultItemAnimator());
      adapter.swapData(linkedLists);
      recyclerView.setAdapter(adapter);
      Timber.d("layoutManagerState %s", layoutManagerState == null);
      if (layoutManagerState != null) {
        recyclerView.getLayoutManager().onRestoreInstanceState(layoutManagerState);
      }
      Timber.d("Position %s", callback.getRecyclerPosition());
      //recyclerView.scrollToPosition(callback.getRecyclerPosition());
    }
  }

  @Override
  public void getStocksUpdates(List<LinkedList<Stock>> linkedLists) {
    adapter.swapData(linkedLists);
  }

  @Override
  public void onItemClick(String stockName, View graph, int themeInt) {
    callback.startDetailsActivity(stockName, graph, themeInt);
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    Activity activity = (Activity) context;
    try {
      callback = (SimulationStartAndGo) activity;
    } catch (ClassCastException e) {
      throw new ClassCastException(activity.toString() + " must implement SimulationStartAndGo");
    }
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    if (savedInstanceState != null) {
      //Restore the fragment's state here
      if (recyclerView != null
          && recyclerView.getLayoutManager() != null
          && savedInstanceState.getParcelableArray(SAVED_LAYOUT_MANAGER) != null) {
        recyclerView.getLayoutManager()
            .onRestoreInstanceState(savedInstanceState.getParcelable(SAVED_LAYOUT_MANAGER));
      }
    }
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    layoutManagerState = recyclerView.getLayoutManager().onSaveInstanceState();
    outState.putParcelable(SAVED_LAYOUT_MANAGER, layoutManagerState);
    //Save the fragment's state here
  }

  public interface SimulationStartAndGo {
    void startDetailsActivity(String stockName, View graph, int themeInt);

    int getRecyclerPosition();
  }
}
