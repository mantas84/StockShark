package eu.oncreate.stockshark.view;

import android.support.annotation.UiThread;
import eu.oncreate.stockshark.data.models.OwnedStockWithCurrentPrice;
import java.util.List;

@UiThread
public interface UserInfoView {

  void setStocksData(List<OwnedStockWithCurrentPrice> ownedStockWithCurrentPrices);

  void setData(String name, String money, int size, int staffCount);

  void stockAreReady();
}