package eu.oncreate.stockshark.view.impl;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.jobs.MoveViewJob;
import eu.oncreate.stockshark.App;
import eu.oncreate.stockshark.R;
import eu.oncreate.stockshark.data.models.Achievements;
import eu.oncreate.stockshark.data.models.Stock;
import eu.oncreate.stockshark.helpers.StringToResStringHelper;
import eu.oncreate.stockshark.injection.AppComponent;
import eu.oncreate.stockshark.injection.DetailsViewModule;
import eu.oncreate.stockshark.presenter.DetailsPresenter;
import eu.oncreate.stockshark.presenter.loader.PresenterFactory;
import eu.oncreate.stockshark.view.DetailsView;
import eu.oncreate.stockshark.withoutMvp.BuySellDialog;
import java.util.LinkedList;
import java.util.List;
import javax.inject.Inject;
import timber.log.Timber;

import static eu.oncreate.stockshark.helpers.MaterialColorsHelper.getColors;

public final class DetailsActivity extends BaseActivity<DetailsPresenter, DetailsView>
    implements DetailsView, BuySellDialog.buyOrSellListener {
  @Inject
  PresenterFactory<DetailsPresenter> mPresenterFactory;

  @BindView(R.id.view_stock_details_root)
  View rootView;
  @BindView(R.id.txt_price)
  TextView currentPrice;
  @BindView(R.id.txt_priceChange)
  TextView priceChange;
  @BindView(R.id.txt_money)
  TextView money;
  @BindView(R.id.txt_profit)
  TextView sellProfit;
  @BindView(R.id.txt_ownedStocks)
  TextView ownedStocksCount;
  @BindView(R.id.txt_tax)
  TextView taxes;
  @BindView(R.id.imageView)
  LineChart chart;
  @BindView(R.id.img_priceChangeIndicator)
  ImageView imageView;

  private int buyQuantityMax;
  private int sellQuantityMax;
  private String stockName;

  private Toast toast;
  private int[] graphColors;

  // Your presenter is available using the mPresenter variable

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    int themeInt = getIntent().getIntExtra("theme", 0);
    graphColors = getColors(this, themeInt);

    super.onCreate(savedInstanceState);
    setContentView(R.layout.view_stock_details_v2);

    ButterKnife.bind(this);
    if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    rootView.setBackgroundColor(graphColors[1]);
    // Your code here
    // Do not call mPresenter from here, it will be null! Wait for onStart or onPostCreate.
  }

  private boolean validSell(int sellCount) {
    if (sellQuantityMax < sellCount || sellQuantityMax == 0) {
      Snackbar.make(rootView, R.string.not_enough_stocks_to_sell, Snackbar.LENGTH_LONG);
      return false;
    }
    return true;
  }

  private boolean validBuy(int buyCount) {
    Timber.d("buyQuantityMax %s, buyCount %s", buyQuantityMax, buyCount);
    if (buyQuantityMax < buyCount || buyQuantityMax == 0) {
      Snackbar.make(rootView, R.string.not_enough_money, Snackbar.LENGTH_LONG);
      return false;
    }
    return true;
  }

  @Override
  protected void setupComponent(@NonNull AppComponent parentComponent) {
    ((App) getApplication()).getUserComponent().plus(new DetailsViewModule()).inject(this);
  }

  @NonNull
  @Override
  protected PresenterFactory<DetailsPresenter> getPresenterFactory() {
    return mPresenterFactory;
  }

  @Override
  public String getStockName() {
    return getIntent().getStringExtra("stockName");
  }

  @Override
  public void onError(Throwable e) {
    Timber.d("onError %s", e.toString());
  }

  @Override
  public void getInitialStockData(LinkedList<Stock> stockLinkedList) {

    chart.setMaxVisibleValueCount(10);
    chart.setBackgroundColor(Color.TRANSPARENT);
    LineData data1 = new LineData();
    ILineDataSet set1 = createSet();
    data1.addDataSet(set1);
    chart.setData(data1);

    LineData data = chart.getData();
    for (Stock stock : stockLinkedList) {
      ILineDataSet set = data.getDataSetByIndex(0);
      float priceF = (float) stock.getPrice();
      data.addEntry(new Entry(set.getEntryCount(), priceF), 0);
      data.notifyDataChanged();
    }
    chart.notifyDataSetChanged();
    chart.setVisibleXRangeMaximum(10);
    chart.setMaxVisibleValueCount(10);
    chart.moveViewToX(data.getEntryCount());

    data.setValueTextColor(graphColors[0]);
    chart.setHighlightPerDragEnabled(false);
    chart.setHighlightPerTapEnabled(false);
    XAxis xAxis = chart.getXAxis();
    YAxis yAxisLeft = chart.getAxisLeft();
    YAxis yAxisRight = chart.getAxisRight();
    xAxis.setEnabled(false);
    yAxisLeft.setEnabled(false);
    yAxisRight.setEnabled(false);
    Legend legend = chart.getLegend();
    legend.setEnabled(false);
    chart.getDescription().setEnabled(false);
    chart.setData(data);
    chart.setBackgroundColor(graphColors[0]);
    //chart.setViewPortOffsets(0f, 0f, 0f, 0f);
    chart.invalidate();
  }

  private LineDataSet createSet() {

    LineDataSet set = new LineDataSet(null, null);
    set.setAxisDependency(YAxis.AxisDependency.LEFT);
    set.setColor(graphColors[3]);
    set.setCircleColor(graphColors[2]);
    //set.setCircleColor(getResources().getColor(R.color.colorPrimary));
    set.setLineWidth(8f);
    set.setCircleRadius(8f);
    set.setFillAlpha(65);
    //set.setFillColor(ColorTemplate.getHoloBlue());
    //set.setHighLightColor(Color.rgb(244, 117, 117));
    //set.setValueTextColor(Color.WHITE);
    //set.setValueTextSize(9f);
    set.setDrawValues(false);
    return set;
  }

  @Override
  public void buySuccessful(String money1, String profit1, int sellCountInt1, int buyCountInt1) {
    money.setText(money1);
    sellProfit.setText(profit1);
    sellQuantityMax = sellCountInt1;
    ownedStocksCount.setText(String.valueOf(sellCountInt1));
    buyQuantityMax = buyCountInt1;

    if (toast != null) {
      toast.setText(R.string.buy_successful);
      toast.show();
    } else {
      toast = Toast.makeText(this, R.string.buy_successful, Toast.LENGTH_SHORT);
      toast.show();
    }
  }

  @Override
  public void sellSuccessful(String money1, String profit1, int sellCountInt1, int buyCountInt1) {
    money.setText(money1);
    sellProfit.setText(profit1);
    sellQuantityMax = sellCountInt1;
    ownedStocksCount.setText(String.valueOf(sellCountInt1));
    buyQuantityMax = buyCountInt1;

    if (toast != null) {
      toast.setText(R.string.sell_successful);
      toast.show();
    } else {
      toast = Toast.makeText(this, R.string.sell_successful, Toast.LENGTH_SHORT);
      toast.show();
    }
  }

  @Override
  public void provideData(String currentStockName, String price, double priceChange1,
      String userMoney, String profit, int userOwnedStocksCount, double taxes1, int userCanBuyMax,
      int graphCounter) {
    if (stockName == null) {
      stockName = currentStockName;
      if (getSupportActionBar() != null) getSupportActionBar().setTitle(stockName);
    }

    if (chart.getData() != null) {
      LineData data = chart.getData();
      ILineDataSet set = data.getDataSetByIndex(0);
      float priceF = Float.parseFloat(price);
      if (set.getEntryCount() >= 99) {
        Entry e = set.getEntryForXValue(graphCounter - 99, Float.NaN);
        data.removeEntry(e, 0);
      }
      data.addEntry(new Entry(graphCounter, priceF), 0);
      chart.notifyDataSetChanged();
      chart.setVisibleXRangeMaximum(10);
      chart.setMaxVisibleValueCount(10);
      chart.moveViewToX(data.getEntryCount());
      chart.setData(data);
    }

    String priceChange2 = String.format("%.2f", priceChange1);
    if (priceChange1 >= 0) {
      imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_trending_up, getTheme()));
    } else {
      imageView.setImageDrawable(
          getResources().getDrawable(R.drawable.ic_trending_down, getTheme()));
    }
    //imageView.setColorFilter(ContextCompat.getColor(this, R.color.material_light_white),
    //    PorterDuff.Mode.SRC_IN);

    currentPrice.setText(price);
    priceChange.setText(priceChange2);
    money.setText(userMoney);
    sellProfit.setText(profit);
    buyQuantityMax = userCanBuyMax;
    sellQuantityMax = userOwnedStocksCount;
    ownedStocksCount.setText(String.valueOf(userOwnedStocksCount));
    String taxesString = String.format("%.2f", taxes1 * 100);
    taxes.setText(taxesString + "%");
  }

  @Override
  public void getAchievements(List<Achievements> newAchievements) {
    Timber.d("newUpdateAchievements==null %s", newAchievements == null);
    if (newAchievements != null) {
      Timber.d("newUpdateAchievements size %s", newAchievements.size());
    }
    if ((newAchievements != null) && (newAchievements.size() > 0)) {
      //showNewAchievements(newUpdateAchievements);
      for (Achievements achievement : newAchievements) {
        achievementToast(
            StringToResStringHelper.getAchievementString(this, achievement.getTypeName(),
                achievement.getLevel()), Toast.LENGTH_SHORT);
        //delay
      }
    }
  }

  void achievementToast(String achievementText, int length) {
    LayoutInflater inflater = getLayoutInflater();
    View layout = inflater.inflate(R.layout.toast_achievement,
        (ViewGroup) findViewById(R.id.custom_toast_container));

    TextView text = layout.findViewById(R.id.text);
    text.setText(achievementText);
    ImageView imageView = layout.findViewById(R.id.img_toastImage);
    //// TODO: 9/21/17 change image
    imageView.setImageDrawable(
        getResources().getDrawable(R.drawable.ic_action_achievement_black, getTheme()));

    Toast toast = new Toast(getApplicationContext());
    //toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
    toast.setDuration(length);
    toast.setView(layout);
    toast.show();
  }

  @Override
  public void onResume() {
    super.onResume();
    mPresenter.startSimulation();
  }

  @Override
  public void onPause() {
    super.onPause();
    Timber.d("onPause");
    mPresenter.stopSimulation();
    //// FIXME: 8/20/17 https://github.com/PhilJay/MPAndroidChart/issues/2238
    MoveViewJob.getInstance(null, 0f, 0f, null, null);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    //// FIXME: 8/20/17 https://github.com/PhilJay/MPAndroidChart/issues/2238
    MoveViewJob.getInstance(null, 0f, 0f, null, null);
  }

  @OnClick({ R.id.button_buy1, R.id.button_buy100, R.id.button_buy10000, R.id.button_buyCustom })
  public void buyStocks(View view) {
    if (view.getId() == R.id.button_buy1) {
      if (validBuy(1)) {
        mPresenter.buyStocks(String.valueOf(1));
      }
    }

    if (view.getId() == R.id.button_buy100) {
      if (validBuy(100)) {
        mPresenter.buyStocks(String.valueOf(100));
      }
    }

    if (view.getId() == R.id.button_buy10000) {
      if (validBuy(10000)) {
        mPresenter.buyStocks(String.valueOf(10000));
      }
    }

    if (view.getId() == R.id.button_buyCustom) {
      showEditDialog(false, buyQuantityMax);
    }
  }

  @OnClick({
      R.id.button_sell1, R.id.button_sell100, R.id.button_sell10000, R.id.button_sellCustom
  })
  public void sellStocks(View view) {
    if (view.getId() == R.id.button_sell1) {
      if (validSell(1)) {
        mPresenter.sellStocks(String.valueOf(1));
      }
    }

    if (view.getId() == R.id.button_sell100) {
      if (validSell(100)) {
        mPresenter.sellStocks(String.valueOf(100));
      }
    }

    if (view.getId() == R.id.button_sell10000) {
      if (validSell(10000)) {
        mPresenter.sellStocks(String.valueOf(100000));
      }
    }

    if (view.getId() == R.id.button_sellCustom) {
      showEditDialog(true, sellQuantityMax);
    }
  }

  private void showEditDialog(boolean sell, int maxQuantity) {
    FragmentManager fm = getSupportFragmentManager();
    if (fm.findFragmentByTag(BuySellDialog.class.getSimpleName()) == null) {
      BuySellDialog buySellDialog = new BuySellDialog();
      Bundle args = new Bundle();
      args.putBoolean("sell", sell);
      args.putInt("max", maxQuantity);
      buySellDialog.setArguments(args);
      buySellDialog.show(fm, buySellDialog.getClass().getSimpleName());
    }
  }

  @Override
  public void buyOrSell(int quantity, boolean sell) {
    cancel();
    if (sell) {
      mPresenter.sellStocks(String.valueOf(quantity));
    } else {
      mPresenter.buyStocks(String.valueOf(quantity));
    }
  }

  @Override
  public void cancel() {
    BuySellDialog fragment = (BuySellDialog) getSupportFragmentManager().findFragmentByTag(
        BuySellDialog.class.getSimpleName());
    if (fragment != null) {
      fragment.dismiss();
    }
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {

    if (item.getItemId() == android.R.id.home) {
      //finish();
      this.supportFinishAfterTransition();
      return true;
    }
    return false;
  }
}
