package eu.oncreate.stockshark.view.impl;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import eu.oncreate.stockshark.App;
import eu.oncreate.stockshark.R;
import eu.oncreate.stockshark.adapter.StockUserInfoAdapter;
import eu.oncreate.stockshark.data.models.OwnedStockWithCurrentPrice;
import eu.oncreate.stockshark.injection.AppComponent;
import eu.oncreate.stockshark.injection.UserInfoViewModule;
import eu.oncreate.stockshark.presenter.UserInfoPresenter;
import eu.oncreate.stockshark.presenter.loader.PresenterFactory;
import eu.oncreate.stockshark.view.UserInfoView;
import eu.oncreate.stockshark.withoutMvp.HireReleaseStaffDialog;
import eu.oncreate.stockshark.withoutMvp.RetireDialog;
import java.util.List;
import javax.inject.Inject;
import timber.log.Timber;

public final class UserInfoActivity extends BaseActivity<UserInfoPresenter, UserInfoView>
    implements UserInfoView, StockUserInfoAdapter.OnItemClickListener,
    HireReleaseStaffDialog.HireReleaseStaffListener, RetireDialog.RetireListener {
  private final String SWITCH1_STATE = "switchState";
  @Inject
  PresenterFactory<UserInfoPresenter> mPresenterFactory;
  @Inject
  SharedPreferences myPrefs;

  @BindView(R.id.app_bar_layout)
  AppBarLayout appBarLayout;
  @BindView(R.id.collapsing_toolbar)
  CollapsingToolbarLayout collapsingToolbar;
  @BindView(R.id.img_userProfile)
  ImageView imageViewUserProfile;
  @BindView(R.id.txt_userMoney)
  TextView textViewUserMoney;
  @BindView(R.id.txt_stocksWorth)
  TextView textViewStocksWorth;
  @BindView(R.id.txt_stocksCount)
  TextView textViewStocksCount;
  @BindView(R.id.txt_staffCount)
  TextView textViewStaffCount;
  @BindView(R.id.toolbar_user)
  Toolbar toolbar;
  @BindView(R.id.recycleView_userOwnedStocks1)
  RecyclerView recyclerView;
  @BindView(R.id.fab)
  FloatingActionButton fab;
  @BindView(R.id.btn_achievements)
  Button buttonAchievements;
  @BindView(R.id.btn_staff)
  Button buttonStaff;
  @BindView(R.id.btn_retire)
  Button buttonRetire;
  private Switch switch1;

  // Your presenter is available using the mPresenter variable

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.view_user);

    ButterKnife.bind(this);
    toolbar.setTitle("");
    setSupportActionBar(toolbar);
    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    // Your code here
    // Do not call mPresenter from here, it will be null! Wait for onStart or onPostCreate.
  }

  @Override
  protected void setupComponent(@NonNull AppComponent parentComponent) {
    ((App) getApplication()).getUserComponent().plus(new UserInfoViewModule()).inject(this);
  }

  @NonNull
  @Override
  protected PresenterFactory<UserInfoPresenter> getPresenterFactory() {
    return mPresenterFactory;
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu_user_info, menu);
    switch1 = menu.findItem(R.id.action_switch_group_stocks)
        .getActionView()
        .findViewById(R.id.action_switch_grouping);
    switch1.setChecked(getPreferences(Context.MODE_PRIVATE).getBoolean(SWITCH1_STATE, false));
    switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        SharedPreferences.Editor editor = myPrefs.edit();
        editor.putBoolean(SWITCH1_STATE, b);
        editor.commit();
        groupStocksInList(b);
      }
    });
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle item selection

    if (item.getItemId() == android.R.id.home) {
      fab.setVisibility(View.INVISIBLE);
      this.supportFinishAfterTransition();
      return true;
    }
    switch (item.getItemId()) {
      //case R.id.action_switch_group_stocks:
      //  groupStocksInList(item.isChecked());
      //  return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  private void groupStocksInList(boolean checked) {
    String checkString = "Not Checked";
    if (checked) {
      checkString = "checked";
    }
    mPresenter.getData(checked);
    Timber.d("Is %s", checkString);
  }

  @Override
  public void setStocksData(List<OwnedStockWithCurrentPrice> ownedStockWithCurrentPrices) {
    recyclerView.setHasFixedSize(true);
    //land =2, portrait=1
    int orientation = getResources().getConfiguration().orientation;
    GridLayoutManager gridLayoutManager = new GridLayoutManager(this, orientation);
    recyclerView.setLayoutManager(gridLayoutManager);
    StockUserInfoAdapter adapter = new StockUserInfoAdapter(this, this);
    adapter.swapData(ownedStockWithCurrentPrices);
    recyclerView.setAdapter(adapter);
  }

  @Override
  public void setData(String name, String money, int size, int staffCount1) {
    Timber.d("user name should be %s", name);
    if (toolbar != null) {
      toolbar.setTitle(name);
      Timber.d("toolbars title is %s", toolbar.getTitle());
    } else {
      Timber.d("action bar is null ---------------------");
    }
    textViewUserMoney.setText(money);
    textViewStaffCount.setText(String.valueOf(staffCount1));
  }

  @Override
  public void stockAreReady() {
    textViewStocksWorth.setText(mPresenter.getStockWorth());
    textViewStocksCount.setText(mPresenter.getStocksCount());
    mPresenter.getData(getPreferences(Context.MODE_PRIVATE).getBoolean(SWITCH1_STATE, false));
  }

  @OnClick(R.id.btn_achievements)
  public void submit(View view) {
    Intent intent = new Intent(this, AchievementsActivity.class);
    startActivity(intent);
  }

  @OnClick(R.id.btn_staff)
  public void showStaff(View view) {
    showStaffDialog();
  }

  @OnClick(R.id.fab)
  public void showAssetsActivity(View view) {
    Intent intent = new Intent(this, AssetsActivity.class);
    startActivity(intent);
  }

  @OnClick(R.id.btn_retire)
  public void showRetireDialog() {
    FragmentManager fm = getSupportFragmentManager();
    if (fm.findFragmentByTag(RetireDialog.class.getSimpleName()) == null) {
      RetireDialog retireDialog = new RetireDialog();
      retireDialog.show(fm, retireDialog.getClass().getSimpleName());
    }
  }

  @Override
  public void onItemClick(String stockName) {
    //// TODO: add listener

  }

  @Override
  public void staffChange(int newStaffCount, double staffCost) {
    cancel();
    mPresenter.staffChanged(newStaffCount, staffCost);
  }

  @Override
  public void cancel() {
    HireReleaseStaffDialog fragment =
        (HireReleaseStaffDialog) getSupportFragmentManager().findFragmentByTag(
            HireReleaseStaffDialog.class.getSimpleName());
    if (fragment != null) {
      fragment.dismiss();
    }
  }

  private void showStaffDialog() {
    FragmentManager fm = getSupportFragmentManager();
    if (fm.findFragmentByTag(HireReleaseStaffDialog.class.getSimpleName()) == null) {
      HireReleaseStaffDialog hireReleaseStaff = new HireReleaseStaffDialog();
      Bundle args = new Bundle();
      args.putDouble(HireReleaseStaffDialog.MONEY,
          Double.parseDouble(textViewUserMoney.getText().toString()));
      args.putInt(HireReleaseStaffDialog.STAFF_COUNT,
          Integer.parseInt(textViewStaffCount.getText().toString()));
      hireReleaseStaff.setArguments(args);
      hireReleaseStaff.show(fm, hireReleaseStaff.getClass().getSimpleName());
    }
  }

  @Override
  public void onRetireClicked() {
    onCancelRetireClicked();
    //mPresenter.retire();
    SharedPreferences.Editor prefsEditor;
    prefsEditor = myPrefs.edit();
    prefsEditor.putBoolean("EXIT", true);
    prefsEditor.commit();
    //startActivity(intent);
    this.supportFinishAfterTransition();
  }

  @Override
  public void onCancelRetireClicked() {
    RetireDialog fragment = (RetireDialog) getSupportFragmentManager().findFragmentByTag(
        RetireDialog.class.getSimpleName());
    if (fragment != null) {
      fragment.dismiss();
    }
  }

  @Override
  public void onBackPressed() {
    //// TODO: 10/2/17 weird behaviour
    fab.setVisibility(View.INVISIBLE);
    super.onBackPressed();
  }

  @Override
  protected void onResume() {
    super.onResume();
    fab.setVisibility(View.VISIBLE);
  }
}
