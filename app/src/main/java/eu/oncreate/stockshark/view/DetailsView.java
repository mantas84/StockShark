package eu.oncreate.stockshark.view;

import android.support.annotation.UiThread;
import eu.oncreate.stockshark.data.models.Achievements;
import eu.oncreate.stockshark.data.models.Stock;
import java.util.LinkedList;
import java.util.List;

@UiThread
public interface DetailsView {

  String getStockName();

  void onError(Throwable e);

  void getInitialStockData(LinkedList<Stock> stockLinkedList);

  void buySuccessful(String money, String profit, int sellCountInt, int buyCountInt);

  void sellSuccessful(String money, String profit, int sellCountInt, int buyCountInt);

  void provideData(String currentStockName, String price, double priceChange, String userMoney,
      String profit, int userOwnedStocksCount, double taxes, int userCanBuyMax, int graphIndex);

  void getAchievements(List<Achievements> newAchievements);
}