package eu.oncreate.stockshark.view;

import android.support.annotation.UiThread;
import eu.oncreate.stockshark.data.models.Asset;
import java.util.List;

@UiThread
public interface AssetsView {

  void gotData(List<Asset> assets);

  void onGetDataError(String error);

  void buySuccessful(List<Asset> assetList);

  void onGetBuyError(String s);

  void cantBuyIt(String s);
}