package eu.oncreate.stockshark.view;

import android.support.annotation.UiThread;
import eu.oncreate.stockshark.data.models.Achievements;
import java.util.List;

@UiThread
public interface AchievementsView {

  void provideData(List<Achievements> achievements);

  void onError(String errorString);
}