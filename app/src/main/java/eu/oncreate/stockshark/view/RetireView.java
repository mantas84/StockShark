package eu.oncreate.stockshark.view;

import android.support.annotation.UiThread;

@UiThread
public interface RetireView {

  void retired();
}