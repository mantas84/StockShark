package eu.oncreate.stockshark.view;

import android.support.annotation.UiThread;
import eu.oncreate.stockshark.data.models.Achievements;
import java.util.List;

@UiThread
public interface MainView {
  void updateCountdown(Integer integer);

  void rewardVideoEnable(boolean enableRewardVideo);

  void getAchievements(List<Achievements> newUpdateAchievements);

  void gotScore(Double aDouble);

  //not needed
  //void updateStocks(List<LinkedList<Stock>> linkedLists);
}