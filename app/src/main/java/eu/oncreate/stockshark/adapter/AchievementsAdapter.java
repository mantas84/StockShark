package eu.oncreate.stockshark.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import eu.oncreate.stockshark.R;
import eu.oncreate.stockshark.data.models.Achievements;
import eu.oncreate.stockshark.helpers.StringToResStringHelper;
import java.util.List;

import static eu.oncreate.stockshark.helpers.MaterialColorsHelper.getColors;

/**
 * Created by mantas on 9/23/17.
 */

public class AchievementsAdapter extends RecyclerView.Adapter<AchievementsAdapter.ViewHolder> {

  private List<Achievements> data;
  private Context context;

  public AchievementsAdapter(Context context, List<Achievements> data) {
    this.data = data;
    this.context = context;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View itemView =
        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_achievement, parent, false);
    return new ViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, int position) {
    final int finalPosition = position;
    data.get(position);
    int[] colors = getColors(context, position);
    holder.rootView.setBackgroundColor(colors[0]);
    holder.icon.setColorFilter(colors[3]);
    holder.name.setTextColor(context.getResources().getColor(R.color.material_light_white));
    holder.name.setText(
        StringToResStringHelper.getAchievementString(context, data.get(finalPosition).getTypeName(),
            data.get(finalPosition).getLevel()));
    if (data.get(finalPosition).getValue()) {
      holder.name.setAlpha(1.0f);
    } else {
      holder.name.setAlpha(0.6f);
    }
    holder.icon.setImageDrawable(context.getDrawable(R.drawable.ic_action_achievement_white));
  }

  @Override
  public int getItemCount() {
    if (data != null) {
      return data.size();
    }
    return 0;
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    public TextView name;
    ImageView icon;
    View rootView;

    public ViewHolder(View view) {
      super(view);
      name = view.findViewById(R.id.txt_achievement);
      icon = view.findViewById(R.id.img_achievements_icon);
      rootView = view;
    }
  }
}
