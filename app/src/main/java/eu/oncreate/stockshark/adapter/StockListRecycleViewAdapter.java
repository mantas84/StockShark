package eu.oncreate.stockshark.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.jobs.MoveViewJob;
import eu.oncreate.stockshark.R;
import eu.oncreate.stockshark.data.models.Stock;
import eu.oncreate.stockshark.helpers.MpandroidGraphHelper;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import timber.log.Timber;

import static eu.oncreate.stockshark.helpers.MaterialColorsHelper.getColors;

public class StockListRecycleViewAdapter
    extends RecyclerView.Adapter<StockListRecycleViewAdapter.ViewHolder> {

  private List<LinkedList<Stock>> stockList = new ArrayList<>();
  private OnItemClickListener listener;
  private Context context;

  public StockListRecycleViewAdapter(OnItemClickListener listener, Context context) {
    this.listener = listener;
    this.context = context;
  }

  public void swapData(List<LinkedList<Stock>> stockList1) {
    if (stockList == null) {
      Timber.d("Null here");
    }
    stockList.clear();
    stockList.addAll(stockList1);
    notifyDataSetChanged();
  }

  @Override
  public StockListRecycleViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

    View itemView =
        LayoutInflater.from(parent.getContext()).inflate(R.layout.card_stock_list, parent, false);

    return new ViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(final StockListRecycleViewAdapter.ViewHolder holder, int position) {
    final int finalPosition = position;
    String priceNow = String.format("%.3f", stockList.get(position).getLast().getPrice());
    double priceChangeDouble =
        (1 - (stockList.get(position).get(stockList.get(position).size() - 2).getPrice() / stockList
            .get(position)
            .get(stockList.get(position).size() - 1)
            .getPrice())) * 100;
    String priceChangeString = String.format("%.3f", priceChangeDouble) + "%";

    int[] colors = getColors(context, position);

    MpandroidGraphHelper.setUpGraph(holder.chart, stockList.get(position), 10, colors[1], colors[3],
        colors[2]);

    holder.rootView.setBackgroundColor(colors[1]);

    holder.chart.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        listener.onItemClick(stockList.get(finalPosition).getLast().getName(), holder.chart,
            finalPosition);
      }
    });

    holder.name.setText(stockList.get(position).getLast().getName());
    holder.price.setText(priceNow);
    if (priceChangeDouble >= 0) {
      holder.imageView.setImageDrawable(
          context.getResources().getDrawable(R.drawable.ic_trending_up, context.getTheme()));
    } else {
      holder.imageView.setImageDrawable(
          context.getResources().getDrawable(R.drawable.ic_trending_down, context.getTheme()));
    }
    holder.imageView.setColorFilter(ContextCompat.getColor(context, R.color.material_light_white),
        PorterDuff.Mode.SRC_IN);
    holder.change.setText(priceChangeString);
    holder.rootView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        listener.onItemClick(stockList.get(finalPosition).getLast().getName(), holder.chart,
            finalPosition);
      }
    });
  }

  @Override
  public int getItemCount() {
    if (stockList == null) {
      return 0;
    } else {
      return stockList.size();
    }
  }

  @Override
  public void onViewDetachedFromWindow(ViewHolder holder) {
    super.onViewDetachedFromWindow(holder);
    //// FIXME: 8/20/17 https://github.com/PhilJay/MPAndroidChart/issues/2238
    MoveViewJob.getInstance(null, 0f, 0f, null, null);
  }

  @Override
  public void onViewRecycled(ViewHolder holder) {
    super.onViewRecycled(holder);
    //// FIXME: 8/20/17 https://github.com/PhilJay/MPAndroidChart/issues/2238
    MoveViewJob.getInstance(null, 0f, 0f, null, null);
  }

  public interface OnItemClickListener {
    void onItemClick(String stockName, View graph, int themeInt);
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    public TextView name, price, change;
    ImageView imageView;
    LineChart chart;
    View rootView;

    public ViewHolder(View view) {
      super(view);
      imageView = view.findViewById(R.id.imageView_iconPriceChange);
      chart = view.findViewById(R.id.imageView);
      name = view.findViewById(R.id.textView_name);
      change = view.findViewById(R.id.textView_change);
      price = view.findViewById(R.id.textView_price);
      rootView = view;
    }
  }
}
