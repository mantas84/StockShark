package eu.oncreate.stockshark.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import eu.oncreate.stockshark.data.models.Asset;
import eu.oncreate.stockshark.withoutMvp.AssetItemFragment;
import java.util.List;
import timber.log.Timber;

/**
 * Created by mantas on 9/23/17.
 */

public class AssetViewPagerAdapter extends FragmentStatePagerAdapter {

  private List<Asset> data;
  private int currentPos;

  public AssetViewPagerAdapter(FragmentManager fm, List<Asset> data) {
    super(fm);
    this.data = data;
  }

  @Override
  public Fragment getItem(int position) {
    Timber.d("positron %s", position);
    currentPos = position;
    Fragment fragment =
        AssetItemFragment.newInstance(data.get(position).getName(), data.get(position).getGain(),
            data.get(position).getPrice(), data.get(position).getCountLimit(),
            data.get(position).getCount(), data.get(position).getLastGained(),
            data.get(position).getInterval());
    return fragment;
  }

  @Override
  public int getCount() {
    if (data != null) return data.size();
    return 0;
  }

  @Override
  public int getItemPosition(Object object) {
    // POSITION_NONE makes it possible to reload the PagerAdapter
    return POSITION_NONE;
  }

  public void reloadData(List<Asset> assetList) {
    data = assetList;
    Timber.d("currentPos=%s", currentPos);
    notifyDataSetChanged();
  }
}