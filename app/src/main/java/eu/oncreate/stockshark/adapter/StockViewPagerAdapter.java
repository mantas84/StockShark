package eu.oncreate.stockshark.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import eu.oncreate.stockshark.data.constants.StockConstants;
import eu.oncreate.stockshark.view.impl.StockListFragment;

public class StockViewPagerAdapter extends FragmentStatePagerAdapter {
  //public class StockViewPagerAdapter extends FragmentPagerAdapter {

  private final int PAGES_COUNT = 6;
  private final String[] PAGE_ONE = StockConstants.STOCKS_NAMES_IT;
  private final String[] PAGE_TWO = StockConstants.STOCKS_NAMES_MANUFACTURERS;
  private final String[] PAGE_THREE = StockConstants.STOCKS_NAMES_CARS;
  private final String[] PAGE_FOUR = StockConstants.STOCKS_NAMES_SHOPS;
  private final String[] PAGE_FIVE = StockConstants.STOCKS_NAMES_FOODS;
  private final String[] PAGE_SIX = StockConstants.STOCKS_NAMES_COMMUNICATION;

  private final String[] TAB_NAME = { "IT", "MAN", "CARS", "SHOP", "FOOD", "COM" };

  public StockViewPagerAdapter(FragmentManager fm) {
    super(fm);
  }

  @Override
  public Fragment getItem(int position) {
    Fragment fragment = new StockListFragment();
    String[] pageStocks = getPageStocksNames(position);
    Bundle args = new Bundle();
    args.putStringArray(StockListFragment.STOCK_NAMES, pageStocks);
    fragment.setArguments(args);
    return fragment;
  }

  private String[] getPageStocksNames(int position) {
    String[] pageStocksNames;
    switch (position) {
      case 0:
        pageStocksNames = PAGE_ONE;
        break;
      case 1:
        pageStocksNames = PAGE_TWO;
        break;
      case 2:
        pageStocksNames = PAGE_THREE;
        break;
      case 3:
        pageStocksNames = PAGE_FOUR;
        break;
      case 4:
        pageStocksNames = PAGE_FIVE;
        break;
      case 5:
        pageStocksNames = PAGE_SIX;
        break;
      default:
        pageStocksNames = PAGE_ONE;
        break;
    }
    return pageStocksNames;
  }

  @Override
  public int getCount() {
    return PAGES_COUNT;
  }

  @Override
  public CharSequence getPageTitle(int position) {
    return TAB_NAME[position];
  }
}
