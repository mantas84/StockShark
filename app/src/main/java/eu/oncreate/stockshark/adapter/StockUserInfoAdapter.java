package eu.oncreate.stockshark.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.github.mikephil.charting.jobs.MoveViewJob;
import eu.oncreate.stockshark.R;
import eu.oncreate.stockshark.data.models.OwnedStockWithCurrentPrice;
import java.util.ArrayList;
import java.util.List;
import timber.log.Timber;

import static eu.oncreate.stockshark.helpers.MaterialColorsHelper.getColors;

public class StockUserInfoAdapter extends RecyclerView.Adapter<StockUserInfoAdapter.ViewHolder> {

  private List<OwnedStockWithCurrentPrice> stockList = new ArrayList<>();
  private OnItemClickListener listener;
  private Context context;

  public StockUserInfoAdapter(OnItemClickListener listener, Context context) {
    this.listener = listener;
    this.context = context;
  }

  public void swapData(List<OwnedStockWithCurrentPrice> stockList1) {
    if (stockList == null) {
      Timber.d("Null here");
    }
    stockList.clear();
    stockList.addAll(stockList1);
    notifyDataSetChanged();
  }

  @Override
  public StockUserInfoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

    View itemView = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.card_user_info_stock_list, parent, false);

    return new ViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(StockUserInfoAdapter.ViewHolder holder, int position) {
    final int finalPosition = position;
    String buyPrice = String.format("%.2f", stockList.get(position).getPrice());
    String nowPrice = String.format("%.2f", stockList.get(position).getCurrentPrice());
    String stockCount = String.valueOf(stockList.get(position).getStocksCount());

    int[] colors = getColors(context, position);

    holder.rootView.setBackgroundColor(colors[1]);

    holder.name.setText(stockList.get(position).getName());
    holder.priceBought.setText(buyPrice);
    holder.priceNow.setText(nowPrice);
    holder.stockCount.setText(stockCount);
    holder.rootView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        listener.onItemClick(stockList.get(finalPosition).getName());
      }
    });
  }

  @Override
  public int getItemCount() {
    if (stockList == null) {
      return 0;
    } else {
      return stockList.size();
    }
  }

  @Override
  public void onViewDetachedFromWindow(ViewHolder holder) {
    super.onViewDetachedFromWindow(holder);
    //// FIXME: 8/20/17 https://github.com/PhilJay/MPAndroidChart/issues/2238
    MoveViewJob.getInstance(null, 0f, 0f, null, null);
  }

  @Override
  public void onViewRecycled(ViewHolder holder) {
    super.onViewRecycled(holder);
    //// FIXME: 8/20/17 https://github.com/PhilJay/MPAndroidChart/issues/2238
    MoveViewJob.getInstance(null, 0f, 0f, null, null);
  }

  public interface OnItemClickListener {
    void onItemClick(String stockName);
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    public TextView name, priceBought, priceNow, stockCount;
    View rootView;

    public ViewHolder(View view) {
      super(view);
      name = view.findViewById(R.id.textView_name);
      priceBought = view.findViewById(R.id.textView_price_bought);
      priceNow = view.findViewById(R.id.textView_price_current);
      stockCount = view.findViewById(R.id.textView_stockCount);
      rootView = view;
    }
  }
}


