package eu.oncreate.stockshark.interactor;

import eu.oncreate.stockshark.data.models.Achievements;
import eu.oncreate.stockshark.data.models.Stock;
import io.reactivex.Observable;
import io.reactivex.Single;
import java.util.LinkedList;
import java.util.List;

public interface MainInteractor extends BaseInteractor {

  Observable<List<LinkedList<Stock>>> startSimulation(String[] currentStockGroup);

  void saveStocks();

  Observable<Integer> rewardVideoWatched(long timeNow, int countdown);

  void resetSimTime();

  List<Achievements> getNewUpdateAchievements();

  Single<Double> getScore();
}