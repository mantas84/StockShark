package eu.oncreate.stockshark.interactor.impl;

import eu.oncreate.stockshark.data.DataSource;
import eu.oncreate.stockshark.data.constants.AssetConstants;
import eu.oncreate.stockshark.data.models.Asset;
import eu.oncreate.stockshark.interactor.AssetsInteractor;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import java.util.List;
import java.util.concurrent.Callable;
import javax.inject.Inject;
import timber.log.Timber;

public final class AssetsInteractorImpl implements AssetsInteractor {

  private DataSource dataSource;

  @Inject
  public AssetsInteractorImpl(DataSource dataSource) {
    this.dataSource = dataSource;
  }

  @Override
  public Single<List<Asset>> getData() {
    return Single.fromCallable(new Callable<List<Asset>>() {
      @Override
      public List<Asset> call() throws Exception {
        //return dataSource.getUser().getAssetList();
        return dataSource.getUserAssetList();
      }
    });
    //return Single.just(dataSource.getUser().getAssetList());
  }

  @Override
  public boolean canUserBuyIt(String name) {
    boolean canUserBuyIt = false;
    double price = Double.MAX_VALUE;
    int count = 0;
    int countLimit = 0;
    List<Asset> assetList = dataSource.getUserAssetList();
    for (Asset asset : assetList) {
      if (asset.getName().equals(name)) {
        price = asset.getPrice();
        count = asset.getCount();
        countLimit = asset.getCountLimit();
        break;
      }
    }
    if ((dataSource.getUserMoney() > price) && (count < countLimit)) {
      canUserBuyIt = true;
    }
    Timber.d("price %s, count %s, countLimit %s, canUserBuyIt %s", price, count, countLimit,
        canUserBuyIt);
    return canUserBuyIt;
  }

  @Override
  public Single<String> whyCantBuyIt(final String name) {
    return Single.fromCallable(new Callable<String>() {
      @Override
      public String call() throws Exception {
        return whyCant(name);
      }
    });
  }

  @Override
  public Single<List<Asset>> buyAsset(final String name, final long epochSeconds) {
    return Single.fromCallable(new Callable<List<Asset>>() {
      @Override
      public List<Asset> call() throws Exception {
        return buyAssetFromDb(name, epochSeconds);
      }
    });
    //return Single.just(buyAssetFromDb(name, epochSeconds));
    //return Completable.(buyAssetFromDb(name));
    //return Completable;
  }

  @Override
  public Completable assetMoneyGained(final String name, final long timeNow) {
    return Completable.fromCallable(new Callable<Boolean>() {
      @Override
      public Boolean call() throws Exception {
        return onMoneyGainedModifyDb(name, timeNow);
      }
    });
  }

  private boolean onMoneyGainedModifyDb(String name, long timeNow) {
    List<Asset> assetList = dataSource.getUserAssetList();
    double moneyEarned = 0;
    int i = 0;
    boolean modified = false;
    for (Asset asset : assetList) {
      if (asset.getName().equals(name)) {
        i = assetList.indexOf(asset);
        double timeBetween = timeNow - asset.getLastGained();
        int timesPassed = (int) (timeBetween / asset.getInterval());
        int count = asset.getCount();
        moneyEarned = timesPassed * count * asset.getGain();
        modified = true;
        break;
      }
    }
    assetList.get(i).setLastGained(timeNow);
    dataSource.setUserAssetList(assetList);
    dataSource.userMoneyChange(moneyEarned);
    dataSource.storeUserData();
    return modified;
  }

  private String whyCant(String name) {
    String reason = "";
    double price = Double.MAX_VALUE;
    int count = 0;
    int countLimit = 0;
    List<Asset> assetList = dataSource.getUserAssetList();
    for (Asset asset : assetList) {
      if (asset.getName().equals(name)) {
        price = asset.getPrice();
        count = asset.getCount();
        countLimit = asset.getCountLimit();
        break;
      }
    }
    if (dataSource.getUserMoney() < price) {
      reason = AssetConstants.ASSET_COULD_NOT_AFFORD_IT;
    } else {
      reason = AssetConstants.ASSET_LIMIT_REACHED;
    }
    return reason;
  }

  private List<Asset> buyAssetFromDb(String name, long epochSeconds) {
    List<Asset> assetList = dataSource.getUserAssetList();
    double cost = 0;
    for (Asset asset : assetList) {
      if (asset.getName().equals(name)) {
        int i = assetList.indexOf(asset);
        if (assetList.get(i).getCount() == 0) {
          assetList.get(i).setLastGained(epochSeconds);
        }
        assetList.get(i).setCount(asset.getCount() + 1);
        cost = asset.getPrice();
        break;
      }
    }
    dataSource.setUserAssetList(assetList);
    dataSource.userMoneyChange(-cost);
    dataSource.storeUserData();
    return assetList;
  }
}