package eu.oncreate.stockshark.interactor;

import io.reactivex.Completable;

public interface RetireInteractor extends BaseInteractor {

  Completable retire();
}