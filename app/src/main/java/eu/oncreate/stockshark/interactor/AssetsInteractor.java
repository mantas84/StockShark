package eu.oncreate.stockshark.interactor;

import eu.oncreate.stockshark.data.models.Asset;
import io.reactivex.Completable;
import io.reactivex.Single;
import java.util.List;

public interface AssetsInteractor extends BaseInteractor {

  Single<List<Asset>> getData();

  boolean canUserBuyIt(String name);

  Single<String> whyCantBuyIt(String name);

  Single<List<Asset>> buyAsset(String name, long epochSeconds);

  Completable assetMoneyGained(String name, long timeNow);
}