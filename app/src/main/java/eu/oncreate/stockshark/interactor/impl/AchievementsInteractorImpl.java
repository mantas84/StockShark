package eu.oncreate.stockshark.interactor.impl;

import eu.oncreate.stockshark.data.DataSource;
import eu.oncreate.stockshark.data.models.Achievements;
import eu.oncreate.stockshark.interactor.AchievementsInteractor;
import io.reactivex.Single;
import java.util.List;
import java.util.concurrent.Callable;
import javax.inject.Inject;

public final class AchievementsInteractorImpl implements AchievementsInteractor {

  private DataSource dataSource;

  @Inject
  public AchievementsInteractorImpl(DataSource dataSource) {
    this.dataSource = dataSource;
  }

  @Override
  public Single<List<Achievements>> getData() {
    Single<List<Achievements>> single = Single.fromCallable(new Callable<List<Achievements>>() {
      @Override
      public List<Achievements> call() throws Exception {
        return dataSource.getUserAchievements();
      }
    });
    return single;
  }
}