package eu.oncreate.stockshark.interactor;

import eu.oncreate.stockshark.data.models.Achievements;
import io.reactivex.Single;
import java.util.List;

public interface AchievementsInteractor extends BaseInteractor {

  Single<List<Achievements>> getData();
}