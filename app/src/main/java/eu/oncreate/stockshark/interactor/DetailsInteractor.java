package eu.oncreate.stockshark.interactor;

import eu.oncreate.stockshark.data.models.Achievements;
import eu.oncreate.stockshark.data.models.Stock;
import eu.oncreate.stockshark.data.models.User;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import java.util.LinkedList;
import java.util.List;

public interface DetailsInteractor extends BaseInteractor {

  Single<LinkedList<Stock>> getStockData(String stockName);

  Observable<LinkedList<Stock>> startSimulation(String stockName);

  Single<User> getUserData();

  Completable buyStocks(Stock currentStock, int quantityInt);

  Completable sellStocks(Stock currentStock, int quantityInt);

  List<Achievements> getNewBuyAchievements();

  List<Achievements> getNewSellAchievements();

  List<Achievements> getNewUpdateAchievements();
}