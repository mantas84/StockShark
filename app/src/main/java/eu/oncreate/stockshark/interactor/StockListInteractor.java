package eu.oncreate.stockshark.interactor;

import eu.oncreate.stockshark.data.models.Stock;
import io.reactivex.Observable;
import io.reactivex.Single;
import java.util.LinkedList;
import java.util.List;

public interface StockListInteractor extends BaseInteractor {

  Single<List<LinkedList<Stock>>> getInitData(String[] stockNames);

  Observable<List<LinkedList<Stock>>> getDataUpdates(String[] stocksNames);
}