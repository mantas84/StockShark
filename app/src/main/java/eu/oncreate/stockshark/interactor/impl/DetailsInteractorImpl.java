package eu.oncreate.stockshark.interactor.impl;

import eu.oncreate.stockshark.data.DataSource;
import eu.oncreate.stockshark.data.models.Achievements;
import eu.oncreate.stockshark.data.models.Stock;
import eu.oncreate.stockshark.data.models.User;
import eu.oncreate.stockshark.interactor.DetailsInteractor;
import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableOnSubscribe;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;

public final class DetailsInteractorImpl implements DetailsInteractor {

  private DataSource dataSource;

  @Inject
  public DetailsInteractorImpl(DataSource dataSource) {
    this.dataSource = dataSource;
  }

  @Override
  public Single<LinkedList<Stock>> getStockData(final String stockName) {
    return Single.create(new SingleOnSubscribe<LinkedList<Stock>>() {
      @Override
      public void subscribe(@NonNull SingleEmitter<LinkedList<Stock>> e) throws Exception {
        LinkedList<Stock> stocks;
        stocks = dataSource.getStocks(stockName);
        if (stocks != null) {
          e.onSuccess(stocks);
        } else {
          e.onError(new Throwable("No such stock"));
        }
      }
    });
  }

  @Override
  public Observable<LinkedList<Stock>> startSimulation(final String stockName) {
    return Observable.interval(5, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
        .map(new Function<Long, Boolean>() {
          @Override
          public Boolean apply(@NonNull Long aLong) throws Exception {
            dataSource.updateStockPrices();
            return true;
          }
        })
        .map(new Function<Boolean, LinkedList<Stock>>() {
          @Override
          public LinkedList<Stock> apply(@NonNull Boolean aBoolean) throws Exception {
            return dataSource.getStocks(stockName);
          }
        });
  }

  @Override
  public Single<User> getUserData() {
    return Single.fromCallable(new Callable<User>() {
      @Override
      public User call() throws Exception {
        return dataSource.getUser();
      }
    });
  }

  @Override
  public Completable buyStocks(final Stock currentStock, final int quantityInt) {
    return Completable.create(new CompletableOnSubscribe() {
      @Override
      public void subscribe(@NonNull CompletableEmitter e) throws Exception {
        try {
          dataSource.userBuyStocks(currentStock.getName(), quantityInt);
        } catch (Exception e1) {
          e1.printStackTrace();
          e.onError(e1);
        }
        e.onComplete();
      }
    });
  }

  @Override
  public Completable sellStocks(final Stock currentStock, final int quantityInt) {
    return Completable.create(new CompletableOnSubscribe() {
      @Override
      public void subscribe(@NonNull CompletableEmitter e) throws Exception {
        try {
          dataSource.userSellStocks(currentStock.getName(), quantityInt);
        } catch (Exception e1) {
          e1.printStackTrace();
          e.onError(e1);
        }
        e.onComplete();
      }
    });
  }

  @Override
  public List<Achievements> getNewBuyAchievements() {
    return dataSource.checkUserBuyAchievements();
  }

  @Override
  public List<Achievements> getNewSellAchievements() {
    return dataSource.checkUserSellAchievements();
  }

  @Override
  public List<Achievements> getNewUpdateAchievements() {
    return dataSource.checkStockWorthAchievements();
  }
}