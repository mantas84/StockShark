package eu.oncreate.stockshark.interactor.impl;

import eu.oncreate.stockshark.data.DataSource;
import eu.oncreate.stockshark.data.models.OwnedStockWithCurrentPrice;
import eu.oncreate.stockshark.data.models.OwnedStocks;
import eu.oncreate.stockshark.data.models.User;
import eu.oncreate.stockshark.interactor.UserInfoInteractor;
import io.reactivex.Completable;
import io.reactivex.Single;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import javax.inject.Inject;

public final class UserInfoInteractorImpl implements UserInfoInteractor {

  private DataSource dataSource;

  @Inject
  public UserInfoInteractorImpl(DataSource dataSource) {
    this.dataSource = dataSource;
  }

  @Override
  public Single<User> getUser() {
    return Single.fromCallable(new Callable<User>() {
      @Override
      public User call() throws Exception {
        return dataSource.getUser();
      }
    });
  }

  @Override
  public Single<List<OwnedStockWithCurrentPrice>> getStocksData() {
    return Single.fromCallable(new Callable<List<OwnedStockWithCurrentPrice>>() {
      @Override
      public List<OwnedStockWithCurrentPrice> call() throws Exception {
        return getStocks();
      }
    });
  }

  @Override
  public Completable staffChanged(final int newStaffCount, final double staffCostPerUnit) {
    return Completable.fromCallable(new Callable<Boolean>() {
      @Override
      public Boolean call() throws Exception {
        return dataSource.staffChanged(newStaffCount, staffCostPerUnit);
      }
    });
  }

  private List<OwnedStockWithCurrentPrice> getStocks() {
    List<OwnedStockWithCurrentPrice> list = new ArrayList<>();
    List<OwnedStocks> ownedList = dataSource.getUserOwnedStocksList();
    if (ownedList != null && ownedList.size() > 0) {
      for (OwnedStocks ownedStocks : ownedList) {
        double currentPrice = dataSource.getStocks(ownedStocks.getName()).getLast().getPrice();
        list.add(new OwnedStockWithCurrentPrice(ownedStocks.getName(), ownedStocks.getStocksCount(),
            ownedStocks.getPrice(), currentPrice));
      }
    }
    return list;
  }
}