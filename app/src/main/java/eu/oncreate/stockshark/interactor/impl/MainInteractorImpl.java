package eu.oncreate.stockshark.interactor.impl;

import eu.oncreate.stockshark.data.DataSource;
import eu.oncreate.stockshark.data.models.Achievements;
import eu.oncreate.stockshark.data.models.Stock;
import eu.oncreate.stockshark.interactor.MainInteractor;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;

public final class MainInteractorImpl implements MainInteractor {

  private final int DEFAULT_SIM_TIME = 5;
  private final int AWARDED_SIM_TIME = 1;
  private DataSource dataSource;
  private int timerUnit;
  private int awardTimerLeft;

  @Inject
  public MainInteractorImpl(DataSource dataSource) {
    this.dataSource = dataSource;
    this.timerUnit = DEFAULT_SIM_TIME;
  }

  @Override
  public Observable<List<LinkedList<Stock>>> startSimulation(final String[] currentStockGroup) {
    return Observable.interval(timerUnit, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
        .map(new Function<Long, Boolean>() {
          @Override
          public Boolean apply(@NonNull Long aLong) throws Exception {
            dataSource.updateStockPrices();
            return true;
          }
        })
        .map(new Function<Boolean, List<LinkedList<Stock>>>() {
          @Override
          public List<LinkedList<Stock>> apply(@NonNull Boolean aBoolean) throws Exception {
            return getStocks1(currentStockGroup);
          }
        });
  }

  @Override
  public void saveStocks() {
    dataSource.storeData();
  }

  @Override
  public Observable<Integer> rewardVideoWatched(long timeNow, int countdown) {
    awardTimerLeft = countdown;
    if (awardTimerLeft != 60) dataSource.userWatchedRewardVideo(timeNow);
    timerUnit = AWARDED_SIM_TIME;
    return getRewardObservable();
  }

  @Override
  public void resetSimTime() {
    timerUnit = DEFAULT_SIM_TIME;
  }

  @Override
  public List<Achievements> getNewUpdateAchievements() {
    return dataSource.checkStockWorthAchievements();
  }

  @Override
  public Single<Double> getScore() {
    return Single.fromCallable(new Callable<Double>() {
      @Override
      public Double call() throws Exception {
        return dataSource.getUserScore();
      }
    });
  }

  private Observable<Integer> getRewardObservable() {

    final int start = 60 - awardTimerLeft;
    final int count = awardTimerLeft;

    Observable<Integer> observable =
        Observable.intervalRange(start, count, 0, timerUnit, TimeUnit.SECONDS)
            .map(new Function<Long, Integer>() {
              @Override
              public Integer apply(@NonNull Long aLong) throws Exception {
                awardTimerLeft = awardTimerLeft - 1;
                return awardTimerLeft;
              }
            });

    return observable;
  }

  private List<LinkedList<Stock>> getAllStocks(String[] currentStockGroup) {
    List<LinkedList<Stock>> list = new ArrayList<>();
    for (String s : currentStockGroup) {
      list.add(dataSource.getStocks(s));
    }
    return list;
  }

  private List<LinkedList<Stock>> getStocks1(String[] stockNames) {
    List<LinkedList<Stock>> lists = new ArrayList<>();
    for (String s : stockNames) {
      lists.add(dataSource.getStocks(s));
    }
    return lists;
  }
}