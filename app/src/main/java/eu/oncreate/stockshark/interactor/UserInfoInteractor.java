package eu.oncreate.stockshark.interactor;

import eu.oncreate.stockshark.data.models.OwnedStockWithCurrentPrice;
import eu.oncreate.stockshark.data.models.User;
import io.reactivex.Completable;
import io.reactivex.Single;
import java.util.List;

public interface UserInfoInteractor extends BaseInteractor {

  Single<User> getUser();

  Single<List<OwnedStockWithCurrentPrice>> getStocksData();

  Completable staffChanged(int newStaffCount, double staffCostPerUnit);
}