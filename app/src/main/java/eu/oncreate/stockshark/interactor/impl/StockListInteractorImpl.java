package eu.oncreate.stockshark.interactor.impl;

import eu.oncreate.stockshark.data.DataSource;
import eu.oncreate.stockshark.data.models.Stock;
import eu.oncreate.stockshark.interactor.StockListInteractor;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import javax.inject.Inject;

public final class StockListInteractorImpl implements StockListInteractor {

  private DataSource dataSource;

  @Inject
  public StockListInteractorImpl(DataSource dataSource) {
    this.dataSource = dataSource;
  }

  @Override
  public Single<List<LinkedList<Stock>>> getInitData(final String[] stockNames) {
    return Single.fromCallable(new Callable<List<LinkedList<Stock>>>() {
      @Override
      public List<LinkedList<Stock>> call() throws Exception {
        return getStocks(stockNames);
      }
    });
  }

  @Override
  public Observable<List<LinkedList<Stock>>> getDataUpdates(final String[] stocksNames) {
    return dataSource.getSubject()
        .flatMap(new Function<List<LinkedList<Stock>>, Observable<List<LinkedList<Stock>>>>() {
          @Override
          public Observable<List<LinkedList<Stock>>> apply(
              @NonNull List<LinkedList<Stock>> linkedLists) throws Exception {
            List<String> stringList = Arrays.asList(stocksNames);
            List<LinkedList<Stock>> filteredList = new ArrayList<>();
            for (int i = 0; i < linkedLists.size(); i++) {
              if (stringList.contains(linkedLists.get(i).getFirst().getName())) {
                filteredList.add(linkedLists.get(i));
              }
            }
            return Observable.just(filteredList);
          }
        });
  }

  private List<LinkedList<Stock>> getStocks(String[] stockNames) {
    List<LinkedList<Stock>> lists = new ArrayList<>();
    for (String s : stockNames) {
      lists.add(dataSource.getStocks(s));
    }
    return lists;
  }
}