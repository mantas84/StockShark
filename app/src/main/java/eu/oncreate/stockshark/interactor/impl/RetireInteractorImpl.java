package eu.oncreate.stockshark.interactor.impl;

import eu.oncreate.stockshark.data.DataSource;
import eu.oncreate.stockshark.interactor.RetireInteractor;
import io.reactivex.Completable;
import java.util.concurrent.Callable;
import javax.inject.Inject;

public final class RetireInteractorImpl implements RetireInteractor {

  private DataSource dataSource;

  @Inject
  public RetireInteractorImpl(DataSource dataSource) {
    this.dataSource = dataSource;
  }

  @Override
  public Completable retire() {
    Completable completable = Completable.fromCallable(new Callable<Object>() {
      @Override
      public Object call() throws Exception {
        dataSource.retire();
        return true;
      }
    });
    return completable;
  }
}