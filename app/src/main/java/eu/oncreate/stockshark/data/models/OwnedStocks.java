package eu.oncreate.stockshark.data.models;

public class OwnedStocks {

  private String name;
  private int stocksCount;
  private double price;

  public OwnedStocks(String name, int stocksCount, double price) {
    this.name = name;
    this.stocksCount = stocksCount;
    this.price = price;
  }

  public String getName() {
    return name;
  }

  public int getStocksCount() {
    return stocksCount;
  }

  public double getPrice() {
    return price;
  }

  public void setStockCount(int stockCount) {
    this.stocksCount = stockCount;
  }
}
