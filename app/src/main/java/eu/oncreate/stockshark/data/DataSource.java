package eu.oncreate.stockshark.data;

import eu.oncreate.stockshark.data.constants.StockConstants;
import eu.oncreate.stockshark.data.database.DatabasePaper;
import eu.oncreate.stockshark.data.database.PaperInterface;
import eu.oncreate.stockshark.data.interfaces.DataSourceI;
import eu.oncreate.stockshark.data.interfaces.StockDaoInterface;
import eu.oncreate.stockshark.data.interfaces.UserDaoI;
import eu.oncreate.stockshark.data.models.Achievements;
import eu.oncreate.stockshark.data.models.Asset;
import eu.oncreate.stockshark.data.models.OwnedStocks;
import eu.oncreate.stockshark.data.models.Stock;
import eu.oncreate.stockshark.data.models.User;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import timber.log.Timber;

public class DataSource implements DataSourceI {

  private StockDaoInterface stockDao;
  private UserDaoI userDao;
  private PaperInterface databasePaper;

  private PublishSubject<List<LinkedList<Stock>>> subject;

  public DataSource(StockDao stockDao, UserDao userDao, DatabasePaper databasePaper) {
    this.stockDao = stockDao;
    this.userDao = userDao;
    this.databasePaper = databasePaper;
    subject = PublishSubject.create();
  }

  @Override
  public void userBuyStocks(String name, int amount) {
    double price = stockDao.getLastStock(name).getPrice();
    double moneySpent = price * amount;
    userDao.boughtStocks(stockDao.getLastStock(name), amount);
    userDao.decreaseMoney(moneySpent);
    //userDao.checkBuyAchievements();
    databasePaper.insertUser(userDao.getUser());
  }

  @Override
  public void userSellStocks(String name, int amount) {
    double currentPrice = stockDao.getLastStock(name).getPrice();
    double moneyGained = currentPrice * amount * (1 - userDao.getUser().getTaxesValues());
    userDao.sellStocks(name, amount, currentPrice);
    userDao.increaseMoney(moneyGained);
    //userDao.checkBuyAchievements();
    databasePaper.insertUser(userDao.getUser());
  }

  @Override
  public void userWatchedRewardVideo(long time) {
    userDao.videoActivatedNow(time);
  }

  @Override
  public void updateStockPrices() {
    stockDao.updateStocks();
    //userDao.checkTotalWorth(getTotalWorth(userDao.getUser()));
    subject.onNext(getAllStocks());
  }

  @Override
  public void storeData() {
    long startTime = System.currentTimeMillis();
    databasePaper.insertUser(userDao.getUser());
    for (String stockName : StockConstants.STOCKS_NAMES) {
      databasePaper.insertStocks(stockDao.getStock(stockName));
    }
    long estimatedTime = System.currentTimeMillis() - startTime;
    Timber.d("-------------         time elapsed %s           -----------", estimatedTime);
  }

  @Override
  public void storeUserData() {
    databasePaper.insertUser(userDao.getUser());
  }

  @Override
  public void restoreData() {
    userDao.setUser(databasePaper.getUser());
    for (String stockName : StockConstants.STOCKS_NAMES) {
      stockDao.setStocks(databasePaper.getStocks(stockName));
    }
  }

  @Override
  public User getUser() {
    return userDao.getUser();
  }

  @Override
  public List<Achievements> checkUserBuyAchievements() {
    return userDao.checkBuyAchievements();
  }

  @Override
  public List<Achievements> checkUserSellAchievements() {
    return userDao.checkSellAchievements();
  }

  @Override
  public List<Achievements> checkStockWorthAchievements() {
    return userDao.checkTotalWorthAchievements(getTotalWorth(userDao.getUser()));
  }

  @Override
  public LinkedList<Stock> getStocks(String name) {
    return stockDao.getStock(name);
  }

  @Override
  public Boolean staffChanged(int newStaffCount, double staffCostPerUnit) {
    int oldStaff = userDao.getUser().getStaffCount();
    int difference = newStaffCount - oldStaff;
    userDao.getUser().setStaffCount(newStaffCount);
    if (difference > 0) {
      userDao.decreaseMoney(difference * staffCostPerUnit);
    }
    databasePaper.insertUser(userDao.getUser());
    return true;
  }

  @Override
  public List<Asset> getUserAssetList() {
    return userDao.getUser().getAssetList();
  }

  @Override
  public void setUserAssetList(List<Asset> assetList) {
    userDao.getUser().setAssetList(assetList);
  }

  @Override
  public double getUserMoney() {
    return userDao.getUser().getMoney();
  }

  @Override
  public void userMoneyChange(double v) {
    if (v > 0) {
      userDao.increaseMoney(v);
    } else {
      userDao.decreaseMoney(Math.abs(v));
    }
  }

  @Override
  public List<Achievements> getUserAchievements() {
    return userDao.getUser().getAchievements();
  }

  @Override
  public List<OwnedStocks> getUserOwnedStocksList() {
    return userDao.getAllOwnedStocks();
  }

  @Override
  public Double getUserScore() {
    Double score = 0.0;
    for (OwnedStocks stock : userDao.getAllOwnedStocks()) {
      score += stock.getPrice() * stock.getStocksCount();
    }
    for (Asset asset : getUserAssetList()) {
      score += asset.getPrice() * asset.getCount();
    }
    score += getUserMoney();

    return score;
  }

  @Override
  public void retire() {
    //userDao.deleteAll();
    //stockDao.deleteAll();
    databasePaper.deleteAll();
  }

  private List<LinkedList<Stock>> getAllStocks() {
    List<LinkedList<Stock>> list = new ArrayList<>();
    for (String stockName : StockConstants.STOCKS_NAMES) {
      list.add(stockDao.getStock(stockName));
    }
    return list;
  }

  private long getTotalWorth(User user) {
    long totalWorth = 0L;
    for (OwnedStocks ownedStock : user.getOwnedStocksList()) {
      totalWorth +=
          ownedStock.getStocksCount() * stockDao.getLastStock(ownedStock.getName()).getPrice();
    }
    return totalWorth;
  }

  public Observable<List<LinkedList<Stock>>> getSubject() {
    return subject;
  }
}
