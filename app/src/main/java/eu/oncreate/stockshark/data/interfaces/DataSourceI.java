package eu.oncreate.stockshark.data.interfaces;

import eu.oncreate.stockshark.data.models.Achievements;
import eu.oncreate.stockshark.data.models.Asset;
import eu.oncreate.stockshark.data.models.OwnedStocks;
import eu.oncreate.stockshark.data.models.Stock;
import eu.oncreate.stockshark.data.models.User;
import java.util.LinkedList;
import java.util.List;

public interface DataSourceI {

  void userBuyStocks(String name, int amount);

  void userSellStocks(String name, int amount);

  void userWatchedRewardVideo(long time);

  void updateStockPrices();

  void storeData();

  void storeUserData();

  void restoreData();

  User getUser();

  List<Achievements> checkUserBuyAchievements();

  List<Achievements> checkUserSellAchievements();

  List<Achievements> checkStockWorthAchievements();

  LinkedList<Stock> getStocks(String name);

  Boolean staffChanged(int newStaffCount, double staffCostPerUnit);

  List<Asset> getUserAssetList();

  void setUserAssetList(List<Asset> assetList);

  double getUserMoney();

  void userMoneyChange(double v);

  List<Achievements> getUserAchievements();

  List<OwnedStocks> getUserOwnedStocksList();

  Double getUserScore();

  void retire();
}
