package eu.oncreate.stockshark.data.models;

public class OwnedStockWithCurrentPrice extends OwnedStocks {

  private double currentPrice;

  public OwnedStockWithCurrentPrice(String name, int stocksCount, double price,
      double currentPrice) {
    super(name, stocksCount, price);
    this.currentPrice = currentPrice;
  }

  public double getCurrentPrice() {
    return currentPrice;
  }
}
