package eu.oncreate.stockshark.data.interfaces;

import eu.oncreate.stockshark.data.models.Stock;
import java.util.LinkedList;

public interface StockDaoInterface {

  LinkedList<Stock> getStock(String name);

  void setStocks(LinkedList<Stock> stocks);

  Stock getLastStock(String name);

  void updateStocks();
}
