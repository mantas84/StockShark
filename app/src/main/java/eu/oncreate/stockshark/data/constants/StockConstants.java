package eu.oncreate.stockshark.data.constants;

public class StockConstants {

  public static final String STOCK_ORANGE = "Orange";
  public static final String STOCK_BOOGLE = "Boogle";
  public static final String STOCK_FACEMAG = "Facemag";
  public static final String STOCK_MACROSOFT = "Macrosoft";
  public static final String STOCK_ABN = "ABN";

  public static final String STOCK_SUNNY = "Sunny";
  public static final String STOCK_SUNSONG = "Sunsong";
  public static final String STOCK_BANASONIK = "Banasonik";
  public static final String STOCK_LUAWEI = "Luawei";
  public static final String STOCK_LOSHIPA = "Loshipa";

  public static final String STOCK_POYOPA = "Poyopa";
  public static final String STOCK_ZOLKSZAGEN = "Zolkszagen";
  public static final String STOCK_MAIN_MOTORING = "Main motoring";
  public static final String STOCK_CIA = "Cia";
  public static final String STOCK_CORD = "Cord";

  public static final String STOCK_DAM_MART = "Dam-mart";
  public static final String STOCK_FLOSTCO = "Flostco";
  public static final String STOCK_PROGER = "Proger";
  public static final String STOCK_HOME_DESPOT = "Home Despot";
  public static final String STOCK_BEST_ACQUISITION = "Best acquisition";

  public static final String STOCK_JASON_FOODS = "Jason Foods";
  public static final String STOCK_CLEPSICO = "ClepsiCo";
  public static final String STOCK_TESTLE = "Testle";
  public static final String STOCK_POPA_COLA = "Popa-Cola";
  public static final String STOCK_VRAFT_REINZ_CO = "Vraft Reinz Co";

  public static final String STOCK_AP_P = "AP&P";
  public static final String STOCK_HORIZON_COMMUNICATIONS = "Horizon Communications";
  public static final String STOCK_ZOID = "Zoid";
  public static final String STOCK_ZODAFONE = "Zodafone";
  public static final String STOCK_BLORANGE = "Blorange";
  public static final String[] STOCKS_NAMES = {
      STOCK_ORANGE, STOCK_BOOGLE, STOCK_FACEMAG, STOCK_MACROSOFT, STOCK_ABN,

      STOCK_SUNNY, STOCK_SUNSONG, STOCK_BANASONIK, STOCK_LUAWEI, STOCK_LOSHIPA,

      STOCK_POYOPA, STOCK_ZOLKSZAGEN, STOCK_MAIN_MOTORING, STOCK_CIA, STOCK_CORD,

      STOCK_DAM_MART, STOCK_FLOSTCO, STOCK_PROGER, STOCK_HOME_DESPOT, STOCK_BEST_ACQUISITION,

      STOCK_JASON_FOODS, STOCK_CLEPSICO, STOCK_TESTLE, STOCK_POPA_COLA, STOCK_VRAFT_REINZ_CO,

      STOCK_AP_P, STOCK_HORIZON_COMMUNICATIONS, STOCK_ZOID, STOCK_ZODAFONE, STOCK_BLORANGE
  };
  public static final String[] STOCKS_NAMES_IT = {
      STOCK_ORANGE, STOCK_BOOGLE, STOCK_FACEMAG, STOCK_MACROSOFT, STOCK_ABN
  };
  public static final String[] STOCKS_NAMES_MANUFACTURERS = {
      STOCK_SUNNY, STOCK_SUNSONG, STOCK_BANASONIK, STOCK_LUAWEI, STOCK_LOSHIPA,
  };
  public static final String[] STOCKS_NAMES_CARS = {
      STOCK_POYOPA, STOCK_ZOLKSZAGEN, STOCK_MAIN_MOTORING, STOCK_CIA, STOCK_CORD,
  };
  public static final String[] STOCKS_NAMES_SHOPS = {
      STOCK_DAM_MART, STOCK_FLOSTCO, STOCK_PROGER, STOCK_HOME_DESPOT, STOCK_BEST_ACQUISITION
  };
  public static final String[] STOCKS_NAMES_FOODS = {
      STOCK_JASON_FOODS, STOCK_CLEPSICO, STOCK_TESTLE, STOCK_POPA_COLA, STOCK_VRAFT_REINZ_CO
  };
  public static final String[] STOCKS_NAMES_COMMUNICATION = {
      STOCK_AP_P, STOCK_HORIZON_COMMUNICATIONS, STOCK_ZOID, STOCK_ZODAFONE, STOCK_BLORANGE
  };
  private static final double STOCK_ORANGE_BASE_PRICE = 50.0d;
  private static final double STOCK_BOOGLE_BASE_PRICE = 314.0d;
  private static final double STOCK_FACEMAG_BASE_PRICE = 271.828d;
  private static final double STOCK_MACROSOFT_BASE_PRICE = 100.0d;
  private static final double STOCK_ABN_BASE_PRICE = 101.0d;
  private static final double STOCK_SUNNY_BASE_PRICE = 10.10d;
  private static final double STOCK_SUNSONG_BASE_PRICE = 105.10d;
  private static final double STOCK_BANASONIK_BASE_PRICE = 5.10d;
  private static final double STOCK_LUAWEI_BASE_PRICE = 15.10d;
  private static final double STOCK_LOSHIPA_BASE_PRICE = 7.10d;
  private static final double STOCK_POYOPA_BASE_PRICE = 50.10d;
  private static final double STOCK_ZOLKSZAGEN_BASE_PRICE = 40.10d;
  private static final double STOCK_MAIN_MOTORING_BASE_PRICE = 30.10d;
  private static final double STOCK_CIA_BASE_PRICE = 5.10d;
  private static final double STOCK_CORD_BASE_PRICE = 27.10d;
  private static final double STOCK_DAM_MART_BASE_PRICE = 33.10d;
  private static final double STOCK_FLOSTCO_BASE_PRICE = 51.10d;
  private static final double STOCK_PROGER_BASE_PRICE = 64.10d;
  private static final double STOCK_HOME_DESPOT_BASE_PRICE = 19.10d;
  private static final double STOCK_BEST_ACQUISITION_BASE_PRICE = 43.10d;
  private static final double STOCK_JASON_FOODS_BASE_PRICE = 19.10d;
  private static final double STOCK_CLEPSICO_BASE_PRICE = 21.10d;
  private static final double STOCK_TESTLE_BASE_PRICE = 55.10d;
  private static final double STOCK_POPA_COLA_BASE_PRICE = 105.10d;
  private static final double STOCK_VRAFT_REINZ_CO_BASE_PRICE = 23.10d;
  private static final double STOCK_AP_P_BASE_PRICE = 42.10d;
  private static final double STOCK_HORIZON_COMMUNICATIONS_BASE_PRICE = 50.10d;
  private static final double STOCK_ZOID_BASE_PRICE = 64.10d;
  private static final double STOCK_ZODAFONE_BASE_PRICE = 48.10d;
  private static final double STOCK_BLORANGE_BASE_PRICE = 5.10d;

  public static double getStockBasePrice(String stockName) {
    double basePrice = 50.0d;
    switch (stockName) {

      case STOCK_ABN:
        basePrice = STOCK_ABN_BASE_PRICE;
        break;

      case STOCK_AP_P:
        basePrice = STOCK_AP_P_BASE_PRICE;
        break;

      case STOCK_BANASONIK:
        basePrice = STOCK_BANASONIK_BASE_PRICE;
        break;

      case STOCK_BEST_ACQUISITION:
        basePrice = STOCK_BEST_ACQUISITION_BASE_PRICE;
        break;

      case STOCK_BLORANGE:
        basePrice = STOCK_BLORANGE_BASE_PRICE;
        break;

      case STOCK_BOOGLE:
        basePrice = STOCK_BOOGLE_BASE_PRICE;
        break;

      case STOCK_CIA:
        basePrice = STOCK_CIA_BASE_PRICE;
        break;

      case STOCK_CLEPSICO:
        basePrice = STOCK_CLEPSICO_BASE_PRICE;
        break;

      case STOCK_CORD:
        basePrice = STOCK_CORD_BASE_PRICE;
        break;

      case STOCK_DAM_MART:
        basePrice = STOCK_DAM_MART_BASE_PRICE;
        break;

      case STOCK_FACEMAG:
        basePrice = STOCK_FACEMAG_BASE_PRICE;
        break;
      case STOCK_FLOSTCO:
        basePrice = STOCK_FLOSTCO_BASE_PRICE;
        break;

      case STOCK_HOME_DESPOT:
        basePrice = STOCK_HOME_DESPOT_BASE_PRICE;
        break;

      case STOCK_HORIZON_COMMUNICATIONS:
        basePrice = STOCK_HORIZON_COMMUNICATIONS_BASE_PRICE;
        break;

      case STOCK_JASON_FOODS:
        basePrice = STOCK_JASON_FOODS_BASE_PRICE;
        break;

      case STOCK_LOSHIPA:
        basePrice = STOCK_LOSHIPA_BASE_PRICE;
        break;

      case STOCK_LUAWEI:
        basePrice = STOCK_LUAWEI_BASE_PRICE;
        break;

      case STOCK_MACROSOFT:
        basePrice = STOCK_MACROSOFT_BASE_PRICE;
        break;

      case STOCK_MAIN_MOTORING:
        basePrice = STOCK_MAIN_MOTORING_BASE_PRICE;
        break;

      case STOCK_ORANGE:
        basePrice = STOCK_ORANGE_BASE_PRICE;
        break;

      case STOCK_POPA_COLA:
        basePrice = STOCK_POPA_COLA_BASE_PRICE;
        break;

      case STOCK_PROGER:
        basePrice = STOCK_PROGER_BASE_PRICE;
        break;

      case STOCK_SUNNY:
        basePrice = STOCK_SUNNY_BASE_PRICE;
        break;

      case STOCK_SUNSONG:
        basePrice = STOCK_SUNSONG_BASE_PRICE;
        break;

      case STOCK_TESTLE:
        basePrice = STOCK_TESTLE_BASE_PRICE;
        break;

      case STOCK_POYOPA:
        basePrice = STOCK_POYOPA_BASE_PRICE;
        break;

      case STOCK_VRAFT_REINZ_CO:
        basePrice = STOCK_VRAFT_REINZ_CO_BASE_PRICE;
        break;

      case STOCK_ZODAFONE:
        basePrice = STOCK_ZODAFONE_BASE_PRICE;
        break;

      case STOCK_ZOID:
        basePrice = STOCK_ZOID_BASE_PRICE;
        break;

      case STOCK_ZOLKSZAGEN:
        basePrice = STOCK_ZOLKSZAGEN_BASE_PRICE;
        break;

      default: // Optional
        break;
    }
    return basePrice;
  }
}

