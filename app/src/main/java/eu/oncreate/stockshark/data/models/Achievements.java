package eu.oncreate.stockshark.data.models;

/**
 * Created by mantas on 9/18/17.
 */

public class Achievements {

  private String typeName;
  private long level;

  private long date;
  private boolean value;

  public Achievements(String typeName, long level, long date, boolean value) {
    this.typeName = typeName;
    this.level = level;
    this.date = date;
    this.value = value;
  }

  public String getTypeName() {
    return typeName;
  }

  public void setTypeName(String typeName) {
    this.typeName = typeName;
  }

  public long getLevel() {
    return level;
  }

  public void setLevel(long level) {
    this.level = level;
  }

  /**
   * @return Date in epoch seconds
   **/
  public long getDate() {
    return date;
  }

  /**
   * @param date Date in epoch seconds
   **/
  public void setDate(long date) {
    this.date = date;
  }

  public boolean getValue() {
    return value;
  }

  public void setValue(boolean value) {
    this.value = value;
  }
}
