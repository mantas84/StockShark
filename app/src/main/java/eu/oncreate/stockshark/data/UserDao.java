package eu.oncreate.stockshark.data;

import eu.oncreate.stockshark.data.constants.AchievementsConstants;
import eu.oncreate.stockshark.data.interfaces.UserDaoI;
import eu.oncreate.stockshark.data.models.Achievements;
import eu.oncreate.stockshark.data.models.OwnedStocks;
import eu.oncreate.stockshark.data.models.Stock;
import eu.oncreate.stockshark.data.models.User;
import java.util.ArrayList;
import java.util.List;
import org.threeten.bp.Instant;

public class UserDao implements UserDaoI {

  private User user;

  public UserDao(User user) {
    this.user = user;
  }

  @Override
  public ArrayList<OwnedStocks> getAllOwnedStocks() {
    return user.getOwnedStocksList();
  }

  @Override
  public ArrayList<OwnedStocks> getOwnedStocks(String name) {
    ArrayList<OwnedStocks> ownedStocks = user.getOwnedStocksList();
    ArrayList<OwnedStocks> foundOwnedStocks = new ArrayList<>();
    for (OwnedStocks item : ownedStocks) {
      if (item.getName().equals(name)) {
        foundOwnedStocks.add(item);
      }
    }
    return foundOwnedStocks;
  }

  @Override
  public int getStocksAmount(String name) {
    int amount = 0;
    ArrayList<OwnedStocks> stocksList = getOwnedStocks(name);
    for (OwnedStocks ownedStocks : stocksList) {
      amount = amount + ownedStocks.getStocksCount();
    }
    return amount;
  }

  @Override
  public void boughtStocks(Stock stock, int amount) {
    ArrayList<OwnedStocks> ownedStocksList = user.getOwnedStocksList();
    ownedStocksList.add(new OwnedStocks(stock.getName(), amount, stock.getPrice()));
    user.setOwnedStocksList(ownedStocksList);
    user.setBuysCount(user.getBuysCount() + amount);
  }

  @Override
  public void sellStocks(String stocksName, int amount, double currentPrice) {
    final int salesCount = amount;
    double totalProfit = 0;
    float tax = user.getTaxesValues();
    ArrayList<OwnedStocks> ownedStocks = user.getOwnedStocksList();
    ArrayList<OwnedStocks> ownedStocks1 = new ArrayList<>(ownedStocks);
    ArrayList<OwnedStocks> foundOwnedStocks = new ArrayList<>();
    for (OwnedStocks item : ownedStocks1) {
      if (item.getName().equals(stocksName)) {
        foundOwnedStocks.add(item);
        ownedStocks.remove(item);
      }
    }

    boolean stocksAvailable = true;

    while ((stocksAvailable) && (amount != 0)) {

      if (foundOwnedStocks.size() < 1) {
        stocksAvailable = false;
      } else {
        if (foundOwnedStocks.get(0).getStocksCount() > amount) {
          totalProfit =
              totalProfit + ((foundOwnedStocks.get(0).getPrice() - currentPrice) * amount * (1
                  - tax));
          foundOwnedStocks.get(0).setStockCount(foundOwnedStocks.get(0).getStocksCount() - amount);
          amount = 0;
        } else {
          totalProfit = totalProfit + ((foundOwnedStocks.get(0).getPrice() - currentPrice)
              * foundOwnedStocks.get(0).getStocksCount()
              * (1 - tax));
          amount = amount - foundOwnedStocks.get(0).getStocksCount();
          foundOwnedStocks.remove(0);
        }
      }
    }

    if (foundOwnedStocks.size() > 0) {
      ownedStocks.addAll(foundOwnedStocks);
    }
    user.setOwnedStocksList(ownedStocks);
    if (user.getHighestProfit() < totalProfit) {
      user.setHighestProfit(totalProfit);
    }
    user.setSalesCount(user.getSalesCount() + salesCount);
    user.setTotalProfit(user.getTotalProfit() + totalProfit);
  }

  @Override
  public void changeName(String name) {
    user.setName(name);
  }

  @Override
  public void increaseMoney(double amount) {
    user.setMoney(user.getMoney() + amount);
  }

  @Override
  public void decreaseMoney(double amount) {
    user.setMoney(user.getMoney() - amount);
  }

  @Override
  public void videoActivatedNow(long timeNow) {
    user.setRewardVideoActivatedAt(timeNow);
  }

  @Override
  public User getUser() {
    return user;
  }

  @Override
  public void setUser(User user) {
    this.user = user;
  }

  @Override
  public List<Achievements> checkBuyAchievements() {
    List<Achievements> currentAchievements = user.getAchievements();
    List<Achievements> newAchievements = new ArrayList<>();

    int ownedCount = user.getOwnedStocksList().size();
    double totalProfit = user.getTotalProfit();

    for (Achievements achievements : currentAchievements) {
      if (achievements.getTypeName()
          .equals(AchievementsConstants.ACHIEVEMENTS_TYPE_BUY_COUNT_NAME)) {
        if (totalProfit >= achievements.getLevel() && (!achievements.getValue())) {
          achievements.setDate(Instant.now().getEpochSecond());
          achievements.setValue(true);
          newAchievements.add(achievements);
        }
      }
      if (achievements.getTypeName().equals(AchievementsConstants.ACHIEVEMENTS_TYPE_COUNT_NAME)) {
        if (ownedCount >= achievements.getLevel() && (!achievements.getValue())) {
          achievements.setDate(Instant.now().getEpochSecond());
          achievements.setValue(true);
          newAchievements.add(achievements);
        }
      }
    }

    return newAchievements;
  }

  @Override
  public List<Achievements> checkSellAchievements() {
    List<Achievements> currentAchievements = user.getAchievements();
    List<Achievements> newAchievements = new ArrayList<>();

    double totalProfit = user.getTotalProfit();
    double highestProfit = user.getHighestProfit();
    long salesCount = user.getSalesCount();

    for (Achievements achievements : currentAchievements) {
      if (achievements.getTypeName()
          .equals(AchievementsConstants.ACHIEVEMENTS_TYPE_PROFIT_TOTAL_NAME)) {
        if (totalProfit >= achievements.getLevel() && (!achievements.getValue())) {
          achievements.setDate(Instant.now().getEpochSecond());
          achievements.setValue(true);
          newAchievements.add(achievements);
        }
      }
      if (achievements.getTypeName().equals(AchievementsConstants.ACHIEVEMENTS_TYPE_PROFIT_NAME)) {
        if (highestProfit >= achievements.getLevel() && (!achievements.getValue())) {
          achievements.setDate(Instant.now().getEpochSecond());
          achievements.setValue(true);
          newAchievements.add(achievements);
        }
      }

      if (achievements.getTypeName()
          .equals(AchievementsConstants.ACHIEVEMENTS_TYPE_SELL_COUNT_NAME)) {
        if (salesCount >= achievements.getLevel() && (!achievements.getValue())) {
          achievements.setDate(Instant.now().getEpochSecond());
          achievements.setValue(true);
          newAchievements.add(achievements);
        }
      }
    }
    return newAchievements;
  }

  @Override
  public List<Achievements> checkTotalWorthAchievements(long totalWorth) {
    user.setAssetWorth(totalWorth);
    List<Achievements> currentAchievements = user.getAchievements();
    List<Achievements> newAchievements = new ArrayList<>();
    for (Achievements achievements : currentAchievements) {
      if (achievements.getTypeName().equals(AchievementsConstants.ACHIEVEMENTS_TYPE_WORTH_NAME)) {
        if (totalWorth >= achievements.getLevel() && (!achievements.getValue())) {
          achievements.setDate(Instant.now().getEpochSecond());
          achievements.setValue(true);
          newAchievements.add(achievements);
        }
      }
    }
    return newAchievements;
  }
}
