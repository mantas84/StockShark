package eu.oncreate.stockshark.data.database;

import eu.oncreate.stockshark.data.models.Stock;
import eu.oncreate.stockshark.data.models.User;
import java.util.LinkedList;

public interface PaperInterface {

  void insertUser(User user);

  User getUser();

  void delete(User user);

  void insertStocks(LinkedList<Stock> stockLinkedList);

  LinkedList<Stock> getStocks(String name);

  void deleteStocks(LinkedList<Stock> stockLinkedList);

  void deleteAllStocks();

  void deleteAll();
}
