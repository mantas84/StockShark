package eu.oncreate.stockshark.data.constants;

import eu.oncreate.stockshark.data.models.Asset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mantas on 9/19/17.
 */

public class AssetConstants {

  public static final String ASSET_LEMONADE_STAND_NAME = "lemonade_stand";
  public static final String ASSET_ICE_CREAM_STAND_NAME = "ice_cream_stand";
  public static final String ASSET_BAKERY_NAME = "bakery";
  public static final String ASSET_COFFEE_SHOP_NAME = "coffee_shop";
  public static final String ASSET_DONUT_SHOP_NAME = "donut_shop";
  public static final String ASSET_BOOK_STORE_NAME = "book_store";
  public static final String ASSET_GAME_SHOP_NAME = "game_shop";
  public static final String ASSET_MUSIC_STORE_NAME = "music_store";
  public static final String ASSET_STEAK_HOUSE_NAME = "steak_house";
  public static final String ASSET_PIZZA_NAME = "pizza";
  public static final String ASSET_BOUTIQUE_STORE_NAME = "boutique_store";

  public static final String ASSET_COULD_NOT_AFFORD_IT = "could_not_afford_it";
  public static final String ASSET_LIMIT_REACHED = "limit_reached";

  private static final String[] ALL_NAMES = {
      ASSET_LEMONADE_STAND_NAME, ASSET_ICE_CREAM_STAND_NAME, ASSET_BAKERY_NAME,
      ASSET_COFFEE_SHOP_NAME, ASSET_DONUT_SHOP_NAME, ASSET_BOOK_STORE_NAME, ASSET_GAME_SHOP_NAME,
      ASSET_MUSIC_STORE_NAME, ASSET_STEAK_HOUSE_NAME, ASSET_PIZZA_NAME, ASSET_BOUTIQUE_STORE_NAME
  };

  public static Asset getAsset(String name) throws Exception {
    String assetName;
    double gain;
    double price;
    int count = 0;
    long interval;
    int limit;
    Asset asset;
    switch (name) {
      case ASSET_LEMONADE_STAND_NAME:
        assetName = ASSET_LEMONADE_STAND_NAME;
        gain = 0.2;
        price = 500;
        interval = 60;
        limit = 100;
        asset = new Asset(assetName, gain, price, count, interval, limit);
        break;
      case ASSET_ICE_CREAM_STAND_NAME:
        assetName = ASSET_ICE_CREAM_STAND_NAME;
        gain = 0.5;
        price = 2000;
        interval = 90;
        limit = 90;
        asset = new Asset(assetName, gain, price, count, interval, limit);
        break;
      case ASSET_BAKERY_NAME:
        assetName = ASSET_BAKERY_NAME;
        gain = 1.0;
        price = 3000;
        interval = 135;
        limit = 80;
        asset = new Asset(assetName, gain, price, count, interval, limit);
        break;
      case ASSET_COFFEE_SHOP_NAME:
        assetName = ASSET_COFFEE_SHOP_NAME;
        gain = 1.5;
        price = 5000;
        interval = 202;
        limit = 80;
        asset = new Asset(assetName, gain, price, count, interval, limit);
        break;
      case ASSET_DONUT_SHOP_NAME:
        assetName = ASSET_DONUT_SHOP_NAME;
        gain = 2.0;
        price = 7500;
        interval = 303;
        limit = 70;
        asset = new Asset(assetName, gain, price, count, interval, limit);
        break;
      case ASSET_BOOK_STORE_NAME:
        assetName = ASSET_BOOK_STORE_NAME;
        gain = 5.0;
        price = 15000;
        interval = 455;
        limit = 50;
        asset = new Asset(assetName, gain, price, count, interval, limit);
        break;
      case ASSET_GAME_SHOP_NAME:
        assetName = ASSET_GAME_SHOP_NAME;
        gain = 10.0;
        price = 25000;
        interval = 60;
        limit = 40;
        asset = new Asset(assetName, gain, price, count, interval, limit);
        break;
      case ASSET_MUSIC_STORE_NAME:
        assetName = ASSET_MUSIC_STORE_NAME;
        gain = 20.0;
        price = 40000;
        interval = 683;
        limit = 30;
        asset = new Asset(assetName, gain, price, count, interval, limit);
        break;
      case ASSET_STEAK_HOUSE_NAME:
        assetName = ASSET_STEAK_HOUSE_NAME;
        gain = 30.0;
        price = 60000;
        interval = 1025;
        limit = 20;
        asset = new Asset(assetName, gain, price, count, interval, limit);
        break;
      case ASSET_PIZZA_NAME:
        assetName = ASSET_PIZZA_NAME;
        gain = 30.0;
        price = 70000;
        interval = 1537;
        limit = 20;
        asset = new Asset(assetName, gain, price, count, interval, limit);
        break;
      case ASSET_BOUTIQUE_STORE_NAME:
        assetName = ASSET_BOUTIQUE_STORE_NAME;
        gain = 75.0;
        price = 100000;
        interval = 2305;
        limit = 10;
        asset = new Asset(assetName, gain, price, count, interval, limit);
        break;
      default:
        throw new Exception("So such thing");
    }

    return asset;
  }

  public static List<Asset> initialAssets() {
    List<Asset> assets = new ArrayList<>();
    for (String assetName : ALL_NAMES) {
      try {
        assets.add(getAsset(assetName));
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return assets;
  }
}
