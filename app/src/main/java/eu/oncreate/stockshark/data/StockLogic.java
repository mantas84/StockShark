package eu.oncreate.stockshark.data;

import java.util.Random;

class StockLogic {

  public static double getNewPrice(double todaysPrice, double yesterdaysPrice, double stockBase) {
    double newPrice;
    double fivePercent = stockBase / 20.0d;
    Random random = new Random();
    double gaussianDouble = Math.abs(random.nextGaussian());
    gaussianDouble = Math.max(gaussianDouble, 0.0d);
    gaussianDouble = Math.min(gaussianDouble, 0.9999999d);

    if (shouldRise(todaysPrice, yesterdaysPrice, stockBase)) {
      newPrice = todaysPrice + gaussianDouble * fivePercent;
    } else {
      newPrice = todaysPrice - gaussianDouble * fivePercent;
    }
    return newPrice;
  }

  /**
   * @param stockBase used to determinate floor(base/3) and ceiling(base*3) price
   */
  static boolean shouldRise(double todaysPrice, double yesterdaysPrice, double stockBase) {
    boolean rise;
    Random random = new Random();
    double gaussianDouble = random.nextGaussian();
    boolean shouldChange = false;
    if (gaussianDouble > 0.70) {
      shouldChange = true;
    }

    if ((todaysPrice > stockBase * 3.0d) || (3.0d * todaysPrice < stockBase)) {
      rise = todaysPrice <= stockBase * 3.0d;
    } else {

      if (todaysPrice >= yesterdaysPrice) {
        rise = !shouldChange;
      } else {
        rise = shouldChange;
      }
    }
    return rise;
  }
}
