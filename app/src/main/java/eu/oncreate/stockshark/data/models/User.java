package eu.oncreate.stockshark.data.models;

import eu.oncreate.stockshark.data.constants.AchievementsConstants;
import eu.oncreate.stockshark.data.constants.AssetConstants;
import java.util.ArrayList;
import java.util.List;

public class User {

  public static final String USER = "user";

  private final float DEFAULT_TAX = 0.1f;

  private String name;
  private double money;
  private long rewardVideoActivatedAt;
  private ArrayList<OwnedStocks> ownedStocksList;
  private List<Asset> assetList;
  private List<Achievements> achievements;

  private double totalProfit;
  private double highestProfit;
  private long salesCount;
  private long buysCount;
  private int staffCount;

  //// TODO: 9/18/17
  //depends on time
  private double stocksWorth;

  private double assetWorth;

  public User( String name) {
    this.name = name;
    rewardVideoActivatedAt = 0;
    ownedStocksList = new ArrayList<>();
    achievements = AchievementsConstants.initialAchievements();
    assetList = AssetConstants.initialAssets();
    totalProfit = 0;
    highestProfit = 0;
    stocksWorth = 0;
    assetWorth = 0;
    salesCount = 0;
    buysCount = 0;
    staffCount = 0;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getMoney() {
    return money;
  }

  public void setMoney(double money) {
    this.money = money;
  }

  public ArrayList<OwnedStocks> getOwnedStocksList() {
    return ownedStocksList;
  }

  public void setOwnedStocksList(ArrayList<OwnedStocks> ownedStocksList) {
    this.ownedStocksList = ownedStocksList;
  }

  public long getRewardVideoActivatedAt() {
    return rewardVideoActivatedAt;
  }

  public void setRewardVideoActivatedAt(long rewardVideoActivatedAt) {
    this.rewardVideoActivatedAt = rewardVideoActivatedAt;
  }

  public List<Achievements> getAchievements() {
    return achievements;
  }

  public void setAchievements(ArrayList<Achievements> achievements) {
    this.achievements = achievements;
  }

  public void setAchievements(List<Achievements> achievements) {
    this.achievements = achievements;
  }

  public double getTotalProfit() {
    return totalProfit;
  }

  public void setTotalProfit(double totalProfit) {
    this.totalProfit = totalProfit;
  }

  public double getHighestProfit() {
    return highestProfit;
  }

  public void setHighestProfit(double highestProfit) {
    this.highestProfit = highestProfit;
  }

  public double getStocksWorth() {
    return stocksWorth;
  }

  public void setStocksWorth(double stocksWorth) {
    this.stocksWorth = stocksWorth;
  }

  public double getAssetWorth() {
    return assetWorth;
  }

  public void setAssetWorth(double assetWorth) {
    this.assetWorth = assetWorth;
  }

  public long getSalesCount() {
    return salesCount;
  }

  public void setSalesCount(long salesCount) {
    this.salesCount = salesCount;
  }

  public long getBuysCount() {
    return buysCount;
  }

  public void setBuysCount(long buysCount) {
    this.buysCount = buysCount;
  }

  public List<Asset> getAssetList() {
    return assetList;
  }

  public void setAssetList(List<Asset> assetList) {
    this.assetList = assetList;
  }

  public int getStaffCount() {
    return staffCount;
  }

  public void setStaffCount(int staffCount) {
    this.staffCount = staffCount;
  }

  public float getTaxesValues() {
    float tax;
    if (staffCount == 0) {
      tax = DEFAULT_TAX;
    } else {
      tax = (float) (DEFAULT_TAX * (Math.pow(0.95f, staffCount)));
    }
    return tax;
  }
}
