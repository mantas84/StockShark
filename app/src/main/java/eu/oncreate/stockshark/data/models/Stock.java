package eu.oncreate.stockshark.data.models;

public class Stock {

  private String name;
  private double price;
  private double basePrice;

  public Stock(String name, double price, double basePrice) {
    this.name = name;
    this.price = price;
    this.basePrice = basePrice;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public double getBasePrice() {
    return basePrice;
  }

  public void setBasePrice(double basePrice) {
    this.basePrice = basePrice;
  }

  @Override
  public String toString() {
    return "Stock{"
        + "name='"
        + name
        + '\''
        + ", price="
        + price
        + ", basePrice="
        + basePrice
        + '}';
  }
}
