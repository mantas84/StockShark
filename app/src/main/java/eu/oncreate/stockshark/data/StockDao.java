package eu.oncreate.stockshark.data;

import eu.oncreate.stockshark.data.constants.StockConstants;
import eu.oncreate.stockshark.data.interfaces.StockDaoInterface;
import eu.oncreate.stockshark.data.models.Stock;
import java.util.LinkedList;

import static eu.oncreate.stockshark.data.StockLogic.getNewPrice;

public class StockDao implements StockDaoInterface {

  private LinkedList<Stock> stock_orange;
  private LinkedList<Stock> stock_boogle;
  private LinkedList<Stock> stock_facemag;
  private LinkedList<Stock> stock_macrosoft;
  private LinkedList<Stock> stock_abn;

  private LinkedList<Stock> stock_sunny;
  private LinkedList<Stock> stock_sunsong;
  private LinkedList<Stock> stock_banasonik;
  private LinkedList<Stock> stock_luawei;
  private LinkedList<Stock> stock_loshipa;

  private LinkedList<Stock> stock_poyopa;
  private LinkedList<Stock> stock_zolkszagen;
  private LinkedList<Stock> stock_main_motoring;
  private LinkedList<Stock> stock_cia;
  private LinkedList<Stock> stock_cord;

  private LinkedList<Stock> stock_dam_mart;
  private LinkedList<Stock> stock_flostco;
  private LinkedList<Stock> stock_proger;
  private LinkedList<Stock> stock_home_despot;
  private LinkedList<Stock> stock_best_acquisition;

  private LinkedList<Stock> stock_jason_foods;
  private LinkedList<Stock> stock_clepsico;
  private LinkedList<Stock> stock_testle;
  private LinkedList<Stock> stock_popa_cola;
  private LinkedList<Stock> stock_vraft_reinz_co;

  private LinkedList<Stock> stock_ap_p;
  private LinkedList<Stock> stock_horizon_communications;
  private LinkedList<Stock> stock_zoid;
  private LinkedList<Stock> stock_zodafone;
  private LinkedList<Stock> stock_blorange;

  public StockDao(LinkedList<Stock> stock_orange, LinkedList<Stock> stock_boogle,
      LinkedList<Stock> stock_facemag, LinkedList<Stock> stock_macrosoft,
      LinkedList<Stock> stock_abn, LinkedList<Stock> stock_sunny, LinkedList<Stock> stock_sunsong,
      LinkedList<Stock> stock_banasonik, LinkedList<Stock> stock_luawei,
      LinkedList<Stock> stock_loshipa, LinkedList<Stock> stock_poyopa,
      LinkedList<Stock> stock_zolkszagen, LinkedList<Stock> stock_main_motoring,
      LinkedList<Stock> stock_cia, LinkedList<Stock> stock_cord, LinkedList<Stock> stock_dam_mart,
      LinkedList<Stock> stock_flostco, LinkedList<Stock> stock_proger,
      LinkedList<Stock> stock_home_despot, LinkedList<Stock> stock_best_acquisition,
      LinkedList<Stock> stock_jason_foods, LinkedList<Stock> stock_clepsico,
      LinkedList<Stock> stock_testle, LinkedList<Stock> stock_popa_cola,
      LinkedList<Stock> stock_vraft_reinz_co, LinkedList<Stock> stock_ap_p,
      LinkedList<Stock> stock_horizon_communications, LinkedList<Stock> stock_zoid,
      LinkedList<Stock> stock_zodafone, LinkedList<Stock> stock_blorange) {
    this.stock_orange = stock_orange;
    this.stock_boogle = stock_boogle;
    this.stock_facemag = stock_facemag;
    this.stock_macrosoft = stock_macrosoft;
    this.stock_sunny = stock_sunny;
    this.stock_sunsong = stock_sunsong;
    this.stock_banasonik = stock_banasonik;
    this.stock_luawei = stock_luawei;
    this.stock_loshipa = stock_loshipa;
    this.stock_poyopa = stock_poyopa;
    this.stock_abn = stock_abn;
    this.stock_zolkszagen = stock_zolkszagen;
    this.stock_main_motoring = stock_main_motoring;
    this.stock_cia = stock_cia;
    this.stock_cord = stock_cord;
    this.stock_dam_mart = stock_dam_mart;
    this.stock_home_despot = stock_home_despot;
    this.stock_flostco = stock_flostco;
    this.stock_proger = stock_proger;
    this.stock_best_acquisition = stock_best_acquisition;
    this.stock_jason_foods = stock_jason_foods;
    this.stock_clepsico = stock_clepsico;
    this.stock_testle = stock_testle;
    this.stock_popa_cola = stock_popa_cola;
    this.stock_vraft_reinz_co = stock_vraft_reinz_co;
    this.stock_ap_p = stock_ap_p;
    this.stock_horizon_communications = stock_horizon_communications;
    this.stock_zoid = stock_zoid;
    this.stock_zodafone = stock_zodafone;
    this.stock_blorange = stock_blorange;
  }

  @Override
  public LinkedList<Stock> getStock(String name) {
    LinkedList<Stock> stocks;

    switch (name) {

      case StockConstants.STOCK_ABN:
        stocks = stock_abn;
        break;

      case StockConstants.STOCK_AP_P:
        stocks = stock_ap_p;
        break;

      case StockConstants.STOCK_BANASONIK:
        stocks = stock_banasonik;
        break;

      case StockConstants.STOCK_BEST_ACQUISITION:
        stocks = stock_best_acquisition;
        break;

      case StockConstants.STOCK_BLORANGE:
        stocks = stock_blorange;
        break;

      case StockConstants.STOCK_BOOGLE:
        stocks = stock_boogle;
        break;

      case StockConstants.STOCK_CIA:
        stocks = stock_cia;
        break;

      case StockConstants.STOCK_CLEPSICO:
        stocks = stock_clepsico;
        break;

      case StockConstants.STOCK_CORD:
        stocks = stock_cord;
        break;

      case StockConstants.STOCK_DAM_MART:
        stocks = stock_dam_mart;
        break;

      case StockConstants.STOCK_FACEMAG:
        stocks = stock_facemag;
        break;
      case StockConstants.STOCK_FLOSTCO:
        stocks = stock_flostco;
        break;

      case StockConstants.STOCK_HOME_DESPOT:
        stocks = stock_home_despot;
        break;

      case StockConstants.STOCK_HORIZON_COMMUNICATIONS:
        stocks = stock_horizon_communications;
        break;

      case StockConstants.STOCK_JASON_FOODS:
        stocks = stock_jason_foods;
        break;

      case StockConstants.STOCK_LOSHIPA:
        stocks = stock_loshipa;
        break;

      case StockConstants.STOCK_LUAWEI:
        stocks = stock_luawei;
        break;

      case StockConstants.STOCK_MACROSOFT:
        stocks = stock_macrosoft;
        break;

      case StockConstants.STOCK_MAIN_MOTORING:
        stocks = stock_main_motoring;
        break;

      case StockConstants.STOCK_ORANGE:
        stocks = stock_orange;
        break;

      case StockConstants.STOCK_POPA_COLA:
        stocks = stock_popa_cola;
        break;

      case StockConstants.STOCK_PROGER:
        stocks = stock_proger;
        break;

      case StockConstants.STOCK_SUNNY:
        stocks = stock_sunny;
        break;

      case StockConstants.STOCK_SUNSONG:
        stocks = stock_sunsong;
        break;

      case StockConstants.STOCK_TESTLE:
        stocks = stock_testle;
        break;

      case StockConstants.STOCK_POYOPA:
        stocks = stock_poyopa;
        break;

      case StockConstants.STOCK_VRAFT_REINZ_CO:
        stocks = stock_vraft_reinz_co;
        break;

      case StockConstants.STOCK_ZODAFONE:
        stocks = stock_zodafone;
        break;

      case StockConstants.STOCK_ZOID:
        stocks = stock_zoid;
        break;

      case StockConstants.STOCK_ZOLKSZAGEN:
        stocks = stock_zolkszagen;
        break;

      // return emptyList
      default: // Optional
        stocks = new LinkedList<>();
        break;
    }
    return stocks;
  }

  @Override
  public void setStocks(LinkedList<Stock> stocks) {
    switch (stocks.getFirst().getName()) {

      case StockConstants.STOCK_ABN:
        stock_abn = stocks;
        break;

      case StockConstants.STOCK_AP_P:
        stock_ap_p = stocks;
        break;

      case StockConstants.STOCK_BANASONIK:
        stock_banasonik = stocks;
        break;

      case StockConstants.STOCK_BEST_ACQUISITION:
        stock_best_acquisition = stocks;
        break;

      case StockConstants.STOCK_BLORANGE:
        stock_blorange = stocks;
        break;

      case StockConstants.STOCK_BOOGLE:
        stock_boogle = stocks;
        break;

      case StockConstants.STOCK_CIA:
        stock_cia = stocks;
        break;

      case StockConstants.STOCK_CLEPSICO:
        stock_clepsico = stocks;
        break;

      case StockConstants.STOCK_CORD:
        stock_cord = stocks;
        break;

      case StockConstants.STOCK_DAM_MART:
        stock_dam_mart = stocks;
        break;

      case StockConstants.STOCK_FACEMAG:
        stock_facemag = stocks;
        break;
      case StockConstants.STOCK_FLOSTCO:
        stock_flostco = stocks;
        break;

      case StockConstants.STOCK_HOME_DESPOT:
        stock_home_despot = stocks;
        break;

      case StockConstants.STOCK_HORIZON_COMMUNICATIONS:
        stock_horizon_communications = stocks;
        break;

      case StockConstants.STOCK_JASON_FOODS:
        stock_jason_foods = stocks;
        break;

      case StockConstants.STOCK_LOSHIPA:
        stock_loshipa = stocks;
        break;

      case StockConstants.STOCK_LUAWEI:
        stock_luawei = stocks;
        break;

      case StockConstants.STOCK_MACROSOFT:
        stock_macrosoft = stocks;
        break;

      case StockConstants.STOCK_MAIN_MOTORING:
        stock_main_motoring = stocks;
        break;

      case StockConstants.STOCK_ORANGE:
        stock_orange = stocks;
        break;

      case StockConstants.STOCK_POPA_COLA:
        stock_popa_cola = stocks;
        break;

      case StockConstants.STOCK_PROGER:
        stock_proger = stocks;
        break;

      case StockConstants.STOCK_SUNNY:
        stock_sunny = stocks;
        break;

      case StockConstants.STOCK_SUNSONG:
        stock_sunsong = stocks;
        break;

      case StockConstants.STOCK_TESTLE:
        stock_testle = stocks;
        break;

      case StockConstants.STOCK_POYOPA:
        stock_poyopa = stocks;
        break;

      case StockConstants.STOCK_VRAFT_REINZ_CO:
        stock_vraft_reinz_co = stocks;
        break;

      case StockConstants.STOCK_ZODAFONE:
        stock_zodafone = stocks;
        break;

      case StockConstants.STOCK_ZOID:
        stock_zoid = stocks;
        break;

      case StockConstants.STOCK_ZOLKSZAGEN:
        stock_zolkszagen = stocks;
        break;

      default: // Optional
        break;
    }
  }

  @Override
  public Stock getLastStock(String name) {
    Stock stocks;

    switch (name) {

      case StockConstants.STOCK_ABN:
        stocks = stock_abn.getLast();
        break;

      case StockConstants.STOCK_AP_P:
        stocks = stock_ap_p.getLast();
        break;

      case StockConstants.STOCK_BANASONIK:
        stocks = stock_banasonik.getLast();
        break;

      case StockConstants.STOCK_BEST_ACQUISITION:
        stocks = stock_best_acquisition.getLast();
        break;

      case StockConstants.STOCK_BLORANGE:
        stocks = stock_blorange.getLast();
        break;

      case StockConstants.STOCK_BOOGLE:
        stocks = stock_boogle.getLast();
        break;

      case StockConstants.STOCK_CIA:
        stocks = stock_cia.getLast();
        break;

      case StockConstants.STOCK_CLEPSICO:
        stocks = stock_clepsico.getLast();
        break;

      case StockConstants.STOCK_CORD:
        stocks = stock_cord.getLast();
        break;

      case StockConstants.STOCK_DAM_MART:
        stocks = stock_dam_mart.getLast();
        break;

      case StockConstants.STOCK_FACEMAG:
        stocks = stock_facemag.getLast();
        break;
      case StockConstants.STOCK_FLOSTCO:
        stocks = stock_flostco.getLast();
        break;

      case StockConstants.STOCK_HOME_DESPOT:
        stocks = stock_home_despot.getLast();
        break;

      case StockConstants.STOCK_HORIZON_COMMUNICATIONS:
        stocks = stock_horizon_communications.getLast();
        break;

      case StockConstants.STOCK_JASON_FOODS:
        stocks = stock_jason_foods.getLast();
        break;

      case StockConstants.STOCK_LOSHIPA:
        stocks = stock_loshipa.getFirst();
        break;

      case StockConstants.STOCK_LUAWEI:
        stocks = stock_luawei.getLast();
        break;

      case StockConstants.STOCK_MACROSOFT:
        stocks = stock_macrosoft.getLast();
        break;

      case StockConstants.STOCK_MAIN_MOTORING:
        stocks = stock_main_motoring.getLast();
        break;

      case StockConstants.STOCK_ORANGE:
        stocks = stock_orange.getLast();
        break;

      case StockConstants.STOCK_POPA_COLA:
        stocks = stock_popa_cola.getLast();
        break;

      case StockConstants.STOCK_PROGER:
        stocks = stock_proger.getLast();
        break;

      case StockConstants.STOCK_SUNNY:
        stocks = stock_sunny.getLast();
        break;

      case StockConstants.STOCK_SUNSONG:
        stocks = stock_sunsong.getLast();
        break;

      case StockConstants.STOCK_TESTLE:
        stocks = stock_testle.getLast();
        break;

      case StockConstants.STOCK_POYOPA:
        stocks = stock_poyopa.getLast();
        break;

      case StockConstants.STOCK_VRAFT_REINZ_CO:
        stocks = stock_vraft_reinz_co.getLast();
        break;

      case StockConstants.STOCK_ZODAFONE:
        stocks = stock_zodafone.getLast();
        break;

      case StockConstants.STOCK_ZOID:
        stocks = stock_zoid.getLast();
        break;

      case StockConstants.STOCK_ZOLKSZAGEN:
        stocks = stock_zolkszagen.getLast();
        break;

      // return null
      default: // Optional
        stocks = null;
        break;
    }
    return stocks;
  }

  @Override
  public void updateStocks() {
    int lastSize = 0;
    for (String name : StockConstants.STOCKS_NAMES) {
      update(name);
      lastSize = getStock(name).size();
    }
    //Timber.d("update done %s", lastSize);
  }

  private void update(String name) {
    LinkedList<Stock> stocks = getStock(name);
    while (stocks.size() >= 99) {
      stocks.removeFirst();
    }
    if (stocks.size() >= 2) {
      Stock stockLast = stocks.getLast();
      Stock stock = new Stock(stockLast.getName(),
          getNewPrice(stockLast.getPrice(), stocks.get(stocks.size() - 1).getPrice(),
              stockLast.getBasePrice()), stockLast.getBasePrice());
      stocks.addLast(stock);
    }

    setStocks(stocks);
  }
}
