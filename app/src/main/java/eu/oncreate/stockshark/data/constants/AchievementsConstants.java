package eu.oncreate.stockshark.data.constants;

import eu.oncreate.stockshark.data.models.Achievements;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mantas on 9/20/17.
 */

public class AchievementsConstants {

  public static final String ACHIEVEMENTS_TYPE_PROFIT_NAME = "achievement_profit";
  public static final String ACHIEVEMENTS_TYPE_PROFIT_TOTAL_NAME = "achievement_profit_total";
  public static final String ACHIEVEMENTS_TYPE_COUNT_NAME = "achievement_count";
  public static final String ACHIEVEMENTS_TYPE_WORTH_NAME = "achievement_worth";
  public static final String ACHIEVEMENTS_TYPE_SELL_COUNT_NAME = "achievement_sell_count";
  public static final String ACHIEVEMENTS_TYPE_BUY_COUNT_NAME = "achievement_buy_count";

  private static final long[] PROFIT_TOTAL_LEVELS = {
      1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000, 10000000000L,
      100000000000L, 1000000000000L, 10000000000000L, 100000000000000L, 1000000000000000L
  };
  private static final long[] PROFIT_COUNT_LEVELS = {
      1, 10, 50, 100, 200, 500, 1000, 1500, 2000, 3000, 4000, 5000, 7500, 10000, 15000, 20000,
      25000, 50000, 75000, 100000, 150000, 200000, 250000, 500000, 750000, 1000000, 10000000,
      100000000, 1000000000, 10000000000L, 100000000000L, 1000000000000L, 10000000000000L,
      100000000000000L, 1000000000000000L
  };

  private static String[] ALL_NAMES = {
      ACHIEVEMENTS_TYPE_PROFIT_NAME, ACHIEVEMENTS_TYPE_PROFIT_TOTAL_NAME,
      ACHIEVEMENTS_TYPE_COUNT_NAME, ACHIEVEMENTS_TYPE_WORTH_NAME, ACHIEVEMENTS_TYPE_SELL_COUNT_NAME,
      ACHIEVEMENTS_TYPE_BUY_COUNT_NAME
  };

  public static long[] getLevels(String name) {
    long[] longs;
    switch (name) {
      case ACHIEVEMENTS_TYPE_PROFIT_TOTAL_NAME:
        longs = PROFIT_TOTAL_LEVELS;
        break;
      case ACHIEVEMENTS_TYPE_COUNT_NAME:
        longs = PROFIT_COUNT_LEVELS;
        break;
      case ACHIEVEMENTS_TYPE_SELL_COUNT_NAME:
        longs = PROFIT_COUNT_LEVELS;
        break;
      case ACHIEVEMENTS_TYPE_BUY_COUNT_NAME:
        longs = PROFIT_COUNT_LEVELS;
        break;
      default:
        longs = PROFIT_COUNT_LEVELS;
        break;
    }
    return longs;
  }

  public static List<Achievements> initialAchievements() {
    List<Achievements> achievementsList = new ArrayList<>();
    for (String TYPENAME : ALL_NAMES) {
      for (long level : getLevels(TYPENAME)) {
        achievementsList.add(new Achievements(TYPENAME, level, 0, false));
      }
    }
    return achievementsList;
  }
}
