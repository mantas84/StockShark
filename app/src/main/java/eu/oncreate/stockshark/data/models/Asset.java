package eu.oncreate.stockshark.data.models;

/**
 * Created by mantas on 9/19/17.
 */

public class Asset {

  private String name;
  private double gain;
  private double price;
  private int count;
  private long interval;
  private long lastGained;
  private int countLimit;

  public Asset(String name, double gain, double price, int count, long interval, int countLimit) {
    this.name = name;
    this.gain = gain;
    this.price = price;
    this.count = count;
    this.interval = interval;
    this.countLimit = countLimit;
    lastGained = 0;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getGain() {
    return gain;
  }

  public void setGain(double gain) {
    this.gain = gain;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public int getCount() {
    return count;
  }

  public void setCount(int count) {
    this.count = count;
  }

  public long getInterval() {
    return interval;
  }

  public void setInterval(long interval) {
    this.interval = interval;
  }

  public long getLastGained() {
    return lastGained;
  }

  public void setLastGained(long lastGained) {
    this.lastGained = lastGained;
  }

  public int getCountLimit() {
    return countLimit;
  }

  public void setCountLimit(int countLimit) {
    this.countLimit = countLimit;
  }
}
