package eu.oncreate.stockshark.data.interfaces;

import eu.oncreate.stockshark.data.models.Achievements;
import eu.oncreate.stockshark.data.models.OwnedStocks;
import eu.oncreate.stockshark.data.models.Stock;
import eu.oncreate.stockshark.data.models.User;
import java.util.ArrayList;
import java.util.List;

public interface UserDaoI {
  ArrayList<OwnedStocks> getAllOwnedStocks();

  ArrayList<OwnedStocks> getOwnedStocks(String name);

  int getStocksAmount(String name);

  void boughtStocks(Stock stock, int amount);

  void sellStocks(String stocksName, int amount, double currentPrice);

  void changeName(String name);

  void increaseMoney(double amount);

  void decreaseMoney(double amount);

  void videoActivatedNow(long timeNow);

  User getUser();

  void setUser(User user);

  List<Achievements> checkBuyAchievements();

  List<Achievements> checkSellAchievements();

  List<Achievements> checkTotalWorthAchievements(long totalWorth);
}
