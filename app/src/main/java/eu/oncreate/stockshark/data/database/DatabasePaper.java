package eu.oncreate.stockshark.data.database;

import eu.oncreate.stockshark.data.constants.StockConstants;
import eu.oncreate.stockshark.data.models.Stock;
import eu.oncreate.stockshark.data.models.User;
import io.paperdb.Paper;
import java.util.LinkedList;

public class DatabasePaper implements PaperInterface {

  @Override
  public void insertUser(User user) {
    Paper.book().write(User.USER, user);
  }

  @Override
  public User getUser() {
    return Paper.book().read(User.USER);
  }

  @Override
  public void delete(User user) {
    Paper.book().delete(User.USER);
  }

  @Override
  public void insertStocks(LinkedList<Stock> stockLinkedList) {
    Paper.book().write(stockLinkedList.getFirst().getName(), stockLinkedList);
  }

  @Override
  public LinkedList<Stock> getStocks(String name) {
    return Paper.book().read(name);
  }

  @Override
  public void deleteStocks(LinkedList<Stock> stockLinkedList) {
    Paper.book().delete(stockLinkedList.getFirst().getName());
  }

  @Override
  public void deleteAllStocks() {
    for (String name : StockConstants.STOCKS_NAMES) {
      Paper.book().delete(name);
    }
  }

  @Override
  public void deleteAll() {
    Paper.book().destroy();
  }
}
