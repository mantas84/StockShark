package eu.oncreate.stockshark.helpers;

import android.graphics.Color;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import eu.oncreate.stockshark.data.models.Stock;
import java.util.LinkedList;

public class MpandroidGraphHelper {

  public static void setUpGraph(LineChart chart, LinkedList<Stock> stockLinkedList, int pointsCount,
      int valueTextColor, int setColor, int circleColor) {
    chart.setMaxVisibleValueCount(10);
    chart.setBackgroundColor(Color.TRANSPARENT);
    LineData data1 = new LineData();
    ILineDataSet set1 = createSet(setColor, circleColor);
    //
    data1.addDataSet(set1);
    chart.setData(data1);

    LineData data = chart.getData();
    int i = 0;
    if (stockLinkedList.size() >= pointsCount) {
      i = stockLinkedList.size() - pointsCount;
    }

    for (int j = i; j < stockLinkedList.size(); j++) {
      ILineDataSet set = data.getDataSetByIndex(0);
      float priceF = (float) stockLinkedList.get(j).getPrice();
      data.addEntry(new Entry(set.getEntryCount(), priceF), 0);
      data.notifyDataChanged();
    }
    chart.notifyDataSetChanged();
    chart.setVisibleXRangeMaximum(10);
    chart.setMaxVisibleValueCount(10);
    chart.moveViewToX(data.getEntryCount());

    data.setValueTextColor(valueTextColor);
    chart.setHighlightPerDragEnabled(false);
    chart.setHighlightPerTapEnabled(false);
    XAxis xAxis = chart.getXAxis();
    YAxis yAxisLeft = chart.getAxisLeft();
    YAxis yAxisRight = chart.getAxisRight();
    xAxis.setEnabled(false);
    yAxisLeft.setEnabled(false);
    yAxisRight.setEnabled(false);
    Legend legend = chart.getLegend();
    legend.setEnabled(false);
    chart.getDescription().setEnabled(false);
    chart.setScaleEnabled(false);
    chart.setData(data);
    //chart.setViewPortOffsets(0f, 0f, 0f, 0f);
    chart.invalidate();
  }

  private static LineDataSet createSet(int setColor, int circleColor) {
    LineDataSet set = new LineDataSet(null, null);
    set.setAxisDependency(YAxis.AxisDependency.LEFT);
    set.setColor(setColor);
    set.setCircleColor(circleColor);
    set.setLineWidth(8f);
    set.setCircleRadius(8f);
    set.setFillAlpha(65);
    set.setDrawValues(false);
    return set;
  }

  public static void updateChart(LineChart chart, int limit, int currentPointIndex,
      float currentPointValue) {
    LineData data = chart.getData();
    ILineDataSet set = data.getDataSetByIndex(0);
    if (set.getEntryCount() >= limit) {
      Entry e = set.getEntryForXValue(currentPointIndex - limit, Float.NaN);
      data.removeEntry(e, 0);
    }
    data.addEntry(new Entry(currentPointIndex, currentPointValue), 0);
    chart.notifyDataSetChanged();
    chart.setVisibleXRangeMaximum(10);
    chart.setMaxVisibleValueCount(10);
    chart.moveViewToX(data.getEntryCount());
    chart.setData(data);
  }
}
