package eu.oncreate.stockshark.helpers;

import android.content.Context;
import eu.oncreate.stockshark.R;
import eu.oncreate.stockshark.data.constants.AchievementsConstants;
import eu.oncreate.stockshark.data.constants.AssetConstants;

/**
 * Created by mantas on 9/21/17.
 */

public class StringToResStringHelper {

  public static String getAchievementString(Context context, String typeName, long level) {
    String string = "";
    if (AchievementsConstants.ACHIEVEMENTS_TYPE_PROFIT_NAME.equals(typeName)) {
      if (level > Integer.MAX_VALUE) {
        string = context.getResources().getString(R.string.achievement_profit, level);
      } else {
        int levelInt = (int) level;
        string = context.getResources()
            .getQuantityString(R.plurals.achievement_profit, levelInt, levelInt);
      }
    }
    if (AchievementsConstants.ACHIEVEMENTS_TYPE_PROFIT_TOTAL_NAME.equals(typeName)) {
      if (level > Integer.MAX_VALUE) {
        string = context.getResources().getString(R.string.achievement_profit_total, level);
      } else {
        int levelInt = (int) level;
        string = context.getResources()
            .getQuantityString(R.plurals.achievement_profit_total, levelInt, levelInt);
      }
    }
    if (AchievementsConstants.ACHIEVEMENTS_TYPE_COUNT_NAME.equals(typeName)) {
      if (level > Integer.MAX_VALUE) {
        string = context.getResources().getString(R.string.achievement_count, level);
      } else {
        int levelInt = (int) level;
        string = context.getResources()
            .getQuantityString(R.plurals.achievement_count, levelInt, levelInt);
      }
    }
    if (AchievementsConstants.ACHIEVEMENTS_TYPE_WORTH_NAME.equals(typeName)) {
      if (level > Integer.MAX_VALUE) {
        string = context.getResources().getString(R.string.achievement_worth, level);
      } else {
        int levelInt = (int) level;
        string = context.getResources()
            .getQuantityString(R.plurals.achievement_worth, levelInt, levelInt);
      }
    }
    if (AchievementsConstants.ACHIEVEMENTS_TYPE_SELL_COUNT_NAME.equals(typeName)) {
      if (level > Integer.MAX_VALUE) {
        string = context.getResources().getString(R.string.achievement_sell_count, level);
      } else {
        int levelInt = (int) level;
        string = context.getResources()
            .getQuantityString(R.plurals.achievement_sell_count, levelInt, levelInt);
      }
    }
    if (AchievementsConstants.ACHIEVEMENTS_TYPE_BUY_COUNT_NAME.equals(typeName)) {
      if (level > Integer.MAX_VALUE) {
        string = context.getResources().getString(R.string.achievement_buy_count, level);
      } else {
        int levelInt = (int) level;
        string = context.getResources()
            .getQuantityString(R.plurals.achievement_buy_count, levelInt, levelInt);
      }
    }
    return string;
  }

  public static String getAssetString(Context context, String name) {
    String string = "";
    if (AssetConstants.ASSET_BAKERY_NAME.equals(name)) {
      string = context.getResources().getString(R.string.asset_bakery);
    }
    if (AssetConstants.ASSET_LEMONADE_STAND_NAME.equals(name)) {
      string = context.getResources().getString(R.string.asset_lemonade_stand);
    }
    if (AssetConstants.ASSET_ICE_CREAM_STAND_NAME.equals(name)) {
      string = context.getResources().getString(R.string.asset_ice_cream_stand);
    }
    if (AssetConstants.ASSET_COFFEE_SHOP_NAME.equals(name)) {
      string = context.getResources().getString(R.string.asset_coffee_shop);
    }
    if (AssetConstants.ASSET_DONUT_SHOP_NAME.equals(name)) {
      string = context.getResources().getString(R.string.asset_donut_shop);
    }
    if (AssetConstants.ASSET_BOOK_STORE_NAME.equals(name)) {
      string = context.getResources().getString(R.string.asset_book_store);
    }
    if (AssetConstants.ASSET_GAME_SHOP_NAME.equals(name)) {
      string = context.getResources().getString(R.string.asset_game_shop);
    }
    if (AssetConstants.ASSET_MUSIC_STORE_NAME.equals(name)) {
      string = context.getResources().getString(R.string.asset_music_store);
    }
    if (AssetConstants.ASSET_STEAK_HOUSE_NAME.equals(name)) {
      string = context.getResources().getString(R.string.asset_steak_house);
    }
    if (AssetConstants.ASSET_PIZZA_NAME.equals(name)) {
      string = context.getResources().getString(R.string.asset_pizza);
    }
    if (AssetConstants.ASSET_BOUTIQUE_STORE_NAME.equals(name)) {
      string = context.getResources().getString(R.string.asset_boutique_store);
    }

    return string;
  }

  public static String getAssetCantBuyReason(Context context, String reason) {
    String reasonString;
    switch (reason) {
      case AssetConstants.ASSET_COULD_NOT_AFFORD_IT:
        reasonString = context.getResources().getString(R.string.asset_could_not_afford_it);
        break;
      case AssetConstants.ASSET_LIMIT_REACHED:
        reasonString = context.getResources().getString(R.string.asset_limit_reached);
        break;
      default:
        reasonString = context.getResources().getString(R.string.asset_something_went_wrong);
        break;
    }
    return reasonString;
  }
}
