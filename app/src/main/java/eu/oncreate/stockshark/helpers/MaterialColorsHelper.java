package eu.oncreate.stockshark.helpers;

import android.content.Context;
import eu.oncreate.stockshark.R;

public class MaterialColorsHelper {

  public static int[] getColors(Context context, int position) {
    int colorIndex = position % 7;
    int[] returnColors = new int[4];
    switch (colorIndex) {
      case 0:
        returnColors[0] = context.getResources().getColor(R.color.material_indigo_500);
        returnColors[1] = context.getResources().getColor(R.color.material_indigo_700);
        returnColors[2] = context.getResources().getColor(R.color.material_indigo_300);
        returnColors[3] = context.getResources().getColor(R.color.material_pink_accent_200);
        break;
      case 1:
        returnColors[0] = context.getResources().getColor(R.color.material_pink_500);
        returnColors[1] = context.getResources().getColor(R.color.material_pink_700);
        returnColors[2] = context.getResources().getColor(R.color.material_pink_300);
        returnColors[3] = context.getResources().getColor(R.color.material_yellow_accent_200);
        break;
      case 4:
        returnColors[0] = context.getResources().getColor(R.color.material_pink_500);
        returnColors[1] = context.getResources().getColor(R.color.material_pink_700);
        returnColors[2] = context.getResources().getColor(R.color.material_pink_300);
        returnColors[3] = context.getResources().getColor(R.color.material_cyan_accent_200);
        break;
      case 3:
        returnColors[0] = context.getResources().getColor(R.color.material_cyan_500);
        returnColors[1] = context.getResources().getColor(R.color.material_cyan_700);
        returnColors[2] = context.getResources().getColor(R.color.material_cyan_300);
        returnColors[3] = context.getResources().getColor(R.color.material_yellow_accent_200);
        break;
      case 2:
        returnColors[0] = context.getResources().getColor(R.color.material_green_500);
        returnColors[1] = context.getResources().getColor(R.color.material_green_700);
        returnColors[2] = context.getResources().getColor(R.color.material_green_300);
        returnColors[3] = context.getResources().getColor(R.color.material_deep_orange_accent_200);
        break;
      case 5:
        returnColors[0] = context.getResources().getColor(R.color.material_red_500);
        returnColors[1] = context.getResources().getColor(R.color.material_red_700);
        returnColors[2] = context.getResources().getColor(R.color.material_red_300);
        returnColors[3] = context.getResources().getColor(R.color.material_yellow_accent_200);
        break;
      case 6:
        returnColors[0] = context.getResources().getColor(R.color.material_teal_500);
        returnColors[1] = context.getResources().getColor(R.color.material_teal_700);
        returnColors[2] = context.getResources().getColor(R.color.material_teal_300);
        returnColors[3] = context.getResources().getColor(R.color.material_cyan_accent_200);
        break;
      default:
        returnColors[0] = context.getResources().getColor(R.color.material_indigo_500);
        returnColors[1] = context.getResources().getColor(R.color.material_indigo_700);
        returnColors[2] = context.getResources().getColor(R.color.material_indigo_300);
        returnColors[3] = context.getResources().getColor(R.color.material_pink_accent_200);
        break;
    }
    return returnColors;
  }
}
