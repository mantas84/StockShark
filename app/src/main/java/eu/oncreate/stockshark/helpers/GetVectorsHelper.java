package eu.oncreate.stockshark.helpers;

import android.content.Context;
import android.graphics.drawable.Drawable;
import eu.oncreate.stockshark.R;
import eu.oncreate.stockshark.data.constants.AssetConstants;

/**
 * Created by mantas on 9/26/17.
 */

public class GetVectorsHelper {

  public static Drawable getDrawable(Context context, String name) {
    Drawable drawable = null;
    if (AssetConstants.ASSET_BAKERY_NAME.equals(name)) {
      drawable = context.getResources().getDrawable(R.drawable.img_bakery, context.getTheme());
    }
    if (AssetConstants.ASSET_LEMONADE_STAND_NAME.equals(name)) {
      drawable =
          context.getResources().getDrawable(R.drawable.img_lemonadestand, context.getTheme());
    }
    if (AssetConstants.ASSET_ICE_CREAM_STAND_NAME.equals(name)) {
      drawable =
          context.getResources().getDrawable(R.drawable.img_icecreamstand, context.getTheme());
    }
    if (AssetConstants.ASSET_COFFEE_SHOP_NAME.equals(name)) {
      drawable = context.getResources().getDrawable(R.drawable.img_coffeeshop, context.getTheme());
    }
    if (AssetConstants.ASSET_DONUT_SHOP_NAME.equals(name)) {
      drawable = context.getResources().getDrawable(R.drawable.img_donutsshop, context.getTheme());
    }
    if (AssetConstants.ASSET_BOOK_STORE_NAME.equals(name)) {
      drawable = context.getResources().getDrawable(R.drawable.img_bookstore, context.getTheme());
    }
    if (AssetConstants.ASSET_GAME_SHOP_NAME.equals(name)) {
      drawable = context.getResources().getDrawable(R.drawable.img_gameshop, context.getTheme());
    }
    if (AssetConstants.ASSET_MUSIC_STORE_NAME.equals(name)) {
      drawable = context.getResources().getDrawable(R.drawable.img_musicshop, context.getTheme());
    }
    if (AssetConstants.ASSET_STEAK_HOUSE_NAME.equals(name)) {
      drawable = context.getResources().getDrawable(R.drawable.img_steak, context.getTheme());
    }
    if (AssetConstants.ASSET_PIZZA_NAME.equals(name)) {
      drawable = context.getResources().getDrawable(R.drawable.img_pizza, context.getTheme());
    }
    if (AssetConstants.ASSET_BOUTIQUE_STORE_NAME.equals(name)) {
      drawable = context.getResources().getDrawable(R.drawable.img_boutique, context.getTheme());
    }

    return drawable;
  }

  public static int getDrawableInt(String name) {
    int drawableInt = 0;
    if (AssetConstants.ASSET_BAKERY_NAME.equals(name)) {
      drawableInt = R.drawable.img_bakery;
    }
    if (AssetConstants.ASSET_LEMONADE_STAND_NAME.equals(name)) {
      drawableInt = R.drawable.img_lemonadestand;
    }
    if (AssetConstants.ASSET_ICE_CREAM_STAND_NAME.equals(name)) {
      drawableInt = R.drawable.img_icecreamstand;
    }
    if (AssetConstants.ASSET_COFFEE_SHOP_NAME.equals(name)) {
      drawableInt = R.drawable.img_coffeeshop;
    }
    if (AssetConstants.ASSET_DONUT_SHOP_NAME.equals(name)) {
      drawableInt = R.drawable.img_donutsshop;
    }
    if (AssetConstants.ASSET_BOOK_STORE_NAME.equals(name)) {
      drawableInt = R.drawable.img_bookstore;
    }
    if (AssetConstants.ASSET_GAME_SHOP_NAME.equals(name)) {
      drawableInt = R.drawable.img_gameshop;
    }
    if (AssetConstants.ASSET_MUSIC_STORE_NAME.equals(name)) {
      drawableInt = R.drawable.img_musicshop;
    }
    if (AssetConstants.ASSET_STEAK_HOUSE_NAME.equals(name)) {
      drawableInt = R.drawable.img_steak;
    }
    if (AssetConstants.ASSET_PIZZA_NAME.equals(name)) {
      drawableInt = R.drawable.img_pizza;
    }
    if (AssetConstants.ASSET_BOUTIQUE_STORE_NAME.equals(name)) {
      drawableInt = R.drawable.img_boutique;
    }

    return drawableInt;
  }
}
