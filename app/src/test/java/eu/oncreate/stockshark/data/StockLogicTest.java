package eu.oncreate.stockshark.data;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class StockLogicTest {

  @Before
  public void setUp() throws Exception {

  }

  @After
  public void tearDown() throws Exception {

  }

  @Test
  public void getNewPrice() throws Exception {
    final int iterationCount = 100000;
    double todaysPrice = 100.0d;
    double yesterdaysPrice = 100.1d;
    double stockBase = 100.0d;
    int riseCount = 0;
    int dropCount = 0;
    double sumCount = 0;

    for (int i = 0; i < iterationCount; i++) {
      double todaysPriceNew = StockLogic.getNewPrice(todaysPrice, yesterdaysPrice, stockBase);
      sumCount += todaysPriceNew;
      if (todaysPriceNew > todaysPrice) {
        riseCount += 1;
      } else {
        dropCount += 1;
      }
      yesterdaysPrice = todaysPrice;
      todaysPrice = todaysPriceNew;
      Assert.assertTrue(
          "price too high, today's price " + todaysPrice + ", yesterdays " + yesterdaysPrice,
          (todaysPrice <= (3.6d * stockBase)));
      Assert.assertTrue(
          "price too low, today's price " + todaysPrice + ", yesterdays " + yesterdaysPrice,
          3.6d * todaysPrice >= stockBase);
      Assert.assertTrue("price change too big, (yesterdaysPrice - todaysPrice) " + Math.abs(
          yesterdaysPrice - todaysPrice) + ", baseDif " + (stockBase / 20.0d),
          Math.abs(yesterdaysPrice - todaysPrice) <= (stockBase / 20.0d));
    }
    System.out.println("Average Price " + (sumCount / iterationCount));
    System.out.println("Price rise count " + riseCount);
    System.out.println("Price drop count " + dropCount);
  }

  @Test
  public void shouldRise() throws Exception {
    double todaysPrice = 100.0d;
    double yesterdaysPrice = 101.0d;
    double stockBase = 100.0d;
    int riseCount = 0;
    int dropCount = 0;
    boolean shouldRise;
    for (int i = 0; i < 10000; i++) {
      shouldRise = StockLogic.shouldRise(todaysPrice, yesterdaysPrice, stockBase);
      if (shouldRise) {
        riseCount += 1;
      } else {
        dropCount += 1;
      }
    }
    Assert.assertTrue(riseCount > 1000);
    Assert.assertTrue(dropCount > 1000);
  }
}