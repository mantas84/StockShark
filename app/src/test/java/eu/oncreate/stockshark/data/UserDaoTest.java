package eu.oncreate.stockshark.data;

import eu.oncreate.stockshark.data.models.OwnedStocks;
import eu.oncreate.stockshark.data.models.Stock;
import eu.oncreate.stockshark.data.models.User;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UserDaoTest {

  User user;
  OwnedStocks ownedStocks;
  ArrayList<OwnedStocks> ownedStocksList;

  UserDao userDao;

  @Before
  public void setUp() throws Exception {

    user = new User("John Doe");
    user.setMoney(1000.0d);
    ownedStocks = new OwnedStocks("StockName", 100, 101.0d);
    ownedStocksList = new ArrayList<>();
    ownedStocksList.add(ownedStocks);
    user.setOwnedStocksList(ownedStocksList);

    userDao = new UserDao(user);
  }

  @After
  public void tearDown() throws Exception {
    user = null;
    ownedStocks = null;
    ownedStocksList = null;
  }

  @Test
  public void getAllOwnedStocks() throws Exception {
    ArrayList<OwnedStocks> ownedStocks1 = userDao.getAllOwnedStocks();
    Assert.assertNotNull(ownedStocks1);
    Assert.assertTrue(ownedStocks1.size() == 1);
    Assert.assertEquals(ownedStocksList.size(), ownedStocks1.size());
    Assert.assertEquals(ownedStocksList.get(0), ownedStocks1.get(0));
    Assert.assertEquals(ownedStocksList.get(0).getName(), ownedStocks1.get(0).getName());
    Assert.assertEquals(ownedStocksList.get(0).getPrice(), ownedStocks1.get(0).getPrice());
    Assert.assertEquals(ownedStocksList.get(0).getStocksCount(),
        ownedStocks1.get(0).getStocksCount());
  }

  @Test
  public void getOwnedStocks() throws Exception {
    ArrayList<OwnedStocks> ownedStocks1 = userDao.getOwnedStocks(ownedStocks.getName());
    Assert.assertNotNull(ownedStocks1);
    Assert.assertTrue(ownedStocks1.size() == 1);
    Assert.assertEquals(ownedStocksList.size(), ownedStocks1.size());
    Assert.assertEquals(ownedStocksList.get(0), ownedStocks1.get(0));
    Assert.assertEquals(ownedStocksList.get(0).getName(), ownedStocks1.get(0).getName());
    Assert.assertEquals(ownedStocksList.get(0).getPrice(), ownedStocks1.get(0).getPrice());
    Assert.assertEquals(ownedStocksList.get(0).getStocksCount(),
        ownedStocks1.get(0).getStocksCount());
  }

  @Test
  public void getStocksAmount() throws Exception {
    int amount = -1;
    amount = userDao.getStocksAmount(ownedStocks.getName());
    Assert.assertNotEquals(-1, amount);
    Assert.assertEquals(ownedStocks.getStocksCount(), amount);
  }

  @Test
  public void boughtStocks() throws Exception {
    Stock stock = new Stock("stockName", 102.0d, 99.0d);
    userDao.boughtStocks(stock, 100);

    ArrayList<OwnedStocks> owned = userDao.getOwnedStocks(stock.getName());
    Assert.assertEquals(1, owned.size());
    Assert.assertEquals(stock.getName(), owned.get(0).getName());
    Assert.assertEquals(stock.getPrice(), owned.get(0).getPrice());
    Assert.assertEquals(100, owned.get(0).getStocksCount());
  }

  @Test
  public void sellStocks() throws Exception {
    Stock stock = new Stock("stockName1", 102.0d, 99.0d);
    userDao.boughtStocks(stock, 100);

    double currentPrice = stock.getPrice();
    userDao.sellStocks(stock.getName(), 51, currentPrice);

    ArrayList<OwnedStocks> owned = userDao.getOwnedStocks(stock.getName());
    Assert.assertEquals(1, owned.size());
    Assert.assertEquals(stock.getName(), owned.get(0).getName());
    Assert.assertEquals(stock.getPrice(), owned.get(0).getPrice());
    Assert.assertEquals(49, owned.get(0).getStocksCount());
  }

  @Test
  public void changeName() throws Exception {
    String newName = "John Smith";
    userDao.changeName(newName);
    String name = userDao.getUser().getName();
    Assert.assertNotNull(name);
    Assert.assertEquals(newName, name);
  }

  @Test
  public void increaseMoney() throws Exception {
    final double DELTA = 1e-15;
    double moneyAtStart = userDao.getUser().getMoney();
    userDao.increaseMoney(100);
    double moneyAtEnd = userDao.getUser().getMoney();
    Assert.assertEquals(moneyAtStart + 100, moneyAtEnd, DELTA);
  }

  @Test
  public void decreaseMoney() throws Exception {
    final double DELTA = 1e-15;
    double moneyAtStart = userDao.getUser().getMoney();
    userDao.decreaseMoney(100);
    double moneyAtEnd = userDao.getUser().getMoney();
    Assert.assertEquals(moneyAtStart - 100, moneyAtEnd, DELTA);
  }

  @Test
  public void getUser() throws Exception {
    userDao = new UserDao(user);
    User user1 = userDao.getUser();
    Assert.assertEquals(user, user1);
  }

  @Test
  public void setUser() throws Exception {
    userDao.setUser(user);
    User user1 = userDao.getUser();
    Assert.assertEquals(user, user1);
  }
}